#include "pde3d.hpp"
#include "config_code.h"
#include "global_config.h"

namespace fd3 {

// =============================================================================
// PDE
// =============================================================================

PDE::PDE(Mesh *mesh, const std::string &pdeid) : PDE(*mesh, pdeid) {}

PDE::PDE(Mesh &mesh, const std::string &pdeid) : //PDE(&mesh, id) {}
    fd::PDECommon  (pdeid),
    mesh_          (&mesh)
{
  dimension_ = 3;
  if (!mesh_->finalized())
    throw _S"PDE constructor: mesh definition is not complete.";
}

PDE::~PDE() {}

void PDE::set_boundary_discretization_method(int bdm)
{
  fd::PDECommon::set_boundary_discretization_method(bdm);
}

#if 0
void PDE::require_stage_finished(bool stage_finished, const std::string& msg)
{
  if (!stage_finished)
}

void PDE::require_stage_not_finished(const bool& stage)
{
  const bool *p = static_cast<const bool *>(&stage);
  std::string msg;
  if (p == &decl_finished_) msg = "";
  if (stage_finished) throw msg;
}

// -----------------------------------------------------------------------------
// 	BCs
// -----------------------------------------------------------------------------

void PDE::select_bc_for_vertex(const fd::Field& u, int vertex, int sid)
{
  const int field_index = get_component_index(u);
  if (field_index == -1)
  {
    const std::string global_field_name = fd::Field::find_global_id(u.id);
    if (global_field_name.empty())
    	throw _S"Internal error 0014. An unregistered field (#" +
    		fd::to_string(u.id) + ") was encountered. Please report this error.";
    THROW("The field " + global_field_name + " does not belong to the PDE " +
    	_S(components() > 1 ? "system " : "") + name + ".");
  }
  select_bc_for_vertex(field_index, vertex, sid);
}

static inline bool s_check_sid(int sid) noexcept
{ return sid == fd2::Mesh::L || sid == fd2::Mesh::R ||
         sid == fd2::Mesh::B || sid == fd2::Mesh::T; }

bool PDE::check_vertex(int vertex, int sid) const noexcept
{
  for (const auto& r: default_bc_selections_)
    if (r.vertex == vertex)
      return sid == r.sides.primary || sid == r.sides.secondary;
  return false;
}

void PDE::select_bc_for_vertex(int field_index, int vertex, int sid)
{
  if (get_bc_selection_index(field_index, vertex) != -1)
    throw "A boundary condition was previously selected for vertex "
          + fd::to_string(vertex) + ", field "
          + field_[field_index].format_name() + ".";
  THROW_IF(vertex < 0 || vertex > 3, "Bad vertex.");
  THROW_IF(!s_check_sid(sid), "Bad side id: " + fd::to_string(sid) + ".");
  THROW_IF(!check_vertex(vertex, sid), "Bad side (" +
    fd::to_string(sid) + ") for vertex " + fd::to_string(vertex) + ".");
  bc_selections_.push_back({field_index, vertex, sid});
}

int PDE::get_bc_selection_index(int field_index, int vertex) const
{
  for (int i = 0; i < (int) bc_selections_.size(); ++i)
    if (bc_selections_[i].c == field_index && bc_selections_[i].vertex == vertex)
      return i;
  return -1;
}

std::vector<std::array<PDE::VertexSides, 6>> PDE::get_bc_selection_table() const
{
  std::vector<std::array<VertexSides, 6>> table;
  for (int i = 0; i < components(); ++i)
  {
    std::array<VertexSides, 6> vertex_bcs;
    for (const auto& r: default_bc_selections_)
    {
      const int sel = get_bc_selection_index(i, r.vertex);
      const int sid = (sel == -1) ? 0 : bc_selections_[sel].sid;
      vertex_bcs[r.vertex] = (sel == -1 || sid == r.sides.primary) ?
        r.sides : VertexSides{sid, r.sides.primary};
    }
    table.push_back(vertex_bcs);
  }
  return table;
}
#endif // 0

} // namespace fd3
