#include "pde1d.hpp"
#include "config_code.h"
#include "global_config.h"

namespace fd1 {

// =============================================================================
// PDE
// =============================================================================

PDE::PDE(Mesh *mesh, const std::string& pdeid) :
  fd::PDECommon  (pdeid),
  mesh_          (mesh )
{
  dimension_ = 1;
  if (!mesh_->finalized())
    throw _S"PDE constructor: mesh definition is not complete.";
}

PDE::PDE(Mesh& mesh, const std::string& id) : PDE(&mesh, id) {}

PDE::~PDE() {}

std::vector<int> PDE::get_bids_on_side(int sid) const
{
  if (sid == mesh_->get_sid(0)) return std::vector<int>{0};
  if (sid == mesh_->get_sid(1)) return std::vector<int>{1};
  return std::vector<int>{};
}

} // namespace fd1
