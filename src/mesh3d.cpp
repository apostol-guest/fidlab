#include <cstdlib>
// #include <cmath>
#include <iostream>
#include <iomanip>

#include "mesh3d.hpp"
#include "geometryex.hpp"
#include "range.hpp"
#include "global_config.h"

using std::string;
using std::vector;
using std::array;

using fd::Boundary;
using fd::Axis;
using fd::Rectangle;
using fd::plane_coordinate_axis;

#define RESOL(i) fd::global_config().space_resolution[i]
#define RESAREA  fd::global_config().area_resolution

#if 0
        y^       z
         | h+---/---------+g
         | /|  /          |
         |/ | /           |
        d+--|/         +c |
         |  +-------------+f
         | /e          | /
         |/            |/
         +-------------+---------> x
        a               b

  bfgc {0} --> R
  cghd {1} --> T
  aehd {2} --> L
  abfe {3} --> B
  efgh {4} --> U
  abcd {5} --> D

// FIG. The indicated numbers are the sid's (ids of boundary sides).
#endif

/**
 *  Get boundary type from face id.
 */
static int BTYPE[6]{Boundary::X, Boundary::Y, Boundary::X, Boundary::Y,
                    Boundary::Z, Boundary::Z};
/** Face names. */
static const char *FID[] {
    "R", "T", "L", "B", "U", "D"};
/** Edge names. */
static const char *EID[] {
    "BL", "BR", "TR", "TL", "UB", "UR", "UT", "UL", "DB", "DR", "DT", "DL"};
/** Vertex names. */
static const char *VID[] {
    "DBL", "DBR", "DTR", "DTL", "UBL", "UBR", "UTR", "UTL"};
/**
 *  Returns boundary side identifier.
 * 
 *  \param bdtype The type of boundary (Boundary::X, Y, Z)
 *  \param extrem The extremity of the boundary's interval; 0: start, 1: end.
 *  \return The side identifier, Mesh::R, Mesh::T etc.
 */
static int sGetSid(int bdtype, int extrem)
{
  static const int SID[3][2] = {{fd3::Mesh::L, fd3::Mesh::R},
                                {fd3::Mesh::B, fd3::Mesh::T},
                                {fd3::Mesh::D, fd3::Mesh::U}};
  ASSERT((extrem == 0 || extrem == 1) && (
    bdtype == Boundary::X || bdtype == Boundary::Y || bdtype == Boundary::Z));
  return SID[bdtype][extrem];
}

/** Convert boundary type to axis without bounds checking */
static inline Axis toaxis(int bdtyp) noexcept {return static_cast<Axis>(bdtyp);}
// { static const Axis AXIS[3]{Axis::X, Axis::Y, Axis::Z}; return AXIS[bdtyp]; }

static inline bool lt(double p, double q, Axis i)
{ return RESOL(i) < q-p; }

static inline bool eq(double p, double q, Axis i)
{ return std::fabs(p-q) < RESOL(i); }

static inline bool eq(double x, double y, Axis n, int w)
{ return std::fabs(x-y) < RESOL(fd::plane_coordinate_axis(n, w)); }

static double area_resol(const Rectangle& r, size_t N, double hx, double hy)
{ return sqrt(N)*(hx*(r.d - r.c) + hy*(r.b - r.a)); }

namespace fd3 {

//============================================================================//
//::::::::::::::::::::::::::::::::::  Mesh  :::::::::::::::::::::::::::::::::://
//============================================================================//

Mesh::Mesh(std::initializer_list<double> l, int Nx, int Ny, int Nz) :
    nx(Nx), ny(Ny), nz(Nz),
    a(0), b(1), c(0), d(1), e(0), f(1)
{
  auto it = l.begin();
  if (l.size() == 3)
  {
    b = *it++;
    d = *it++;
    f = *it++;
  }
  else if (l.size() == 6)
  {
    a = *it++;
    b = *it++;
    c = *it++;
    d = *it++;
    e = *it++;
    f = *it++;
  }
  else if (l.size() != 0)
  {
    THROW("incorrect number of arguments.");
  }
  
  boundaries_.push_back(&global_boundary_);
  hx = (b - a)/double(nx - 1);
  hy = (d - c)/double(ny - 1);
  hz = (f - e)/double(nz - 1);
  finalized_ = true;
}

#if 0
double& Mesh::operator[](int i) noexcept
{
  static std::function<double&(Mesh&)> __f_[6]{
      [](Mesh &self) -> double& {return self.a;},
      [](Mesh &self) -> double& {return self.b;},
      [](Mesh &self) -> double& {return self.c;},
      [](Mesh &self) -> double& {return self.d;},
      [](Mesh &self) -> double& {return self.e;},
      [](Mesh &self) -> double& {return self.f;}
  };
  return __f_[i](*this);
}
const double& Mesh::operator[](int i) const noexcept
{
  static std::function<const double&(const Mesh&)> __f_[6]{
      [](const Mesh &self) -> const double& {return self.a;},
      [](const Mesh &self) -> const double& {return self.b;},
      [](const Mesh &self) -> const double& {return self.c;},
      [](const Mesh &self) -> const double& {return self.d;},
      [](const Mesh &self) -> const double& {return self.e;},
      [](const Mesh &self) -> const double& {return self.f;}
  };
  return __f_[i](*this);
}
double Mesh::spacing(Axis i) const noexcept
{
  static std::function<double(const Mesh&)> __f_[3]{
      [](const Mesh &self) -> double {return self.hx;},
      [](const Mesh &self) -> double {return self.hy;},
      [](const Mesh &self) -> double {return self.hz;}
  };
  return __f_[i](*this);
}
#endif // 0

static double Mesh::* COORD[6] {
    &Mesh::a, &Mesh::b, &Mesh::c, &Mesh::d, &Mesh::e, &Mesh::f
};
static double Mesh::* SPACING[3] {&Mesh::hx, &Mesh::hy, &Mesh::hz};
static const int I[3][4]{{2,3,4,5}, {0,1,4,5}, {0,1,2,3}};
static const int EXTREMITY[6]{1, 1, 0, 0, 1, 0};
static const double Mesh::* UX[3]{&Mesh::b, &Mesh::d, &Mesh::f};

double& Mesh::operator[](int i) noexcept
{ return this->*COORD[i]; }

const double& Mesh::operator[](int i) const noexcept
{ return this->*COORD[i]; }

double Mesh::spacing(Axis i) const noexcept
{ return this->*SPACING[i]; }

Rectangle Mesh::face_rectangle(Axis n) const noexcept
{ return {coord(I[n][0]), coord(I[n][1]), coord(I[n][2]), coord(I[n][3])}; }

/*static*/
int Mesh::face_extremity(int fid) noexcept
{ return EXTREMITY[fid]; }

int Mesh::get_extremity(const Boundary *p) const
{
  ASSERT(p != nullptr && p->type != Boundary::G);
  return int(eq(this->*UX[p->type], p->pos(), toaxis(p->type)));
}

vector<vector<int>> Mesh::get_bids_by_side() const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  vector<vector<int>> result(6);
  for (const int i : fd::Range<int>(num_boundaries()))
    result[get_sid(i)].push_back(i);
  return result;
}

Mesh &Mesh::set_boundary(Boundary &bc)
{
  if (has_global_boundary())
  {
    boundaries_.pop_back(); // temporarily remove global boundary
    boundaries_.push_back(&bc);
    boundaries_.push_back(&global_boundary_);
    check_boundaries((int)boundaries_.size() - 1);
  }
  else
  {
    THROW_IF(finalized(), "Bad use of method, mesh is finalized.");
    boundaries_.push_back(&bc);
  }
  return *this;
}

void Mesh::set_boundaries(std::initializer_list<Boundary::Ref> l)
{
  if (has_global_boundary())
  {
    boundaries_.pop_back(); // temporarily remove global boundary
    const int nb = (int)boundaries_.size();
    for (auto& ref : l) boundaries_.push_back(&ref.self);
    boundaries_.push_back(&global_boundary_); // restore global boundary
    check_boundaries(nb);
  }
  else
  {
    for (auto& ref : l) set_boundary(ref.self);
    finalize();
  }
}

void Mesh::finalize()
{
  THROW_IF(has_global_boundary(), "Incorrect use of method.");

  for (auto bd : boundaries_)
    if (bd == nullptr)
    {
      THROW("Null boundary.");
    }
    else if (!fd::find({Boundary::X, Boundary::Y, Boundary::Z}, bd->type))
    {
      THROW("Incorrect boundary type.");
    }
  set_domain();
  hx = (b - a)/double(nx - 1);
  hy = (d - c)/double(ny - 1);
  hz = (f - e)/double(nz - 1);
  finalized_ = true;
}

void Mesh::check_boundaries(int begin_from) const
{
  static const int FACE[6] {L, R, B, T, D, U};
  int fid = -1;
  Axis n;
  for (const int bid : fd::Range<int>(begin_from, num_boundaries()))
  {
    Boundary *p = boundaries_[bid];
    if (p->type == Boundary::G)
      throw _S"Global boundaries are automatically inserted. Use the "
              "proper mesh constructor to define a global boundary.";
    for (const int i : fd::Range<int>{6})
    {
      n = static_cast<Axis>(i/2);
      if (n == toaxis(p->type) && eq(p->pos(), coord(i), n))
      {
        fid = FACE[i];
        break;
      }
    }
    if (fid == -1)
      throw "The position of boundary "+fd::to_string(bid)+" is incorrect.";
    const auto &r = static_cast<Boundary3d*>(p)->rectangle();
    if (!face_rectangle(n).contains(r, n))
      throw "Boundary "+fd::to_string(bid)+" does not fit in face "+face(fid)+".";
  }
}

void Mesh::set_domain()
{
  constexpr auto X = Axis::X, Y = Axis::Y, Z = Axis::Z;
  static const string std_dom_error{"Incorrect specification of boundaries."};

  auto set_extremities = [this](int t) -> bool {
    std::vector<Boundary *> fb; // filtered boundaries
    for (const auto bd : boundaries_)
      if (bd->type == t) fb.push_back(bd);
    if (fb.size() < 2) return false;
    const Axis i = toaxis(t);
    auto el = std::minmax_element(fb.begin(), fb.end(),
        [i](Boundary *p, Boundary *q) {return lt(p->pos(), q->pos(), i);});
    // double &A = [this,t]()->double&{double *pa[3]{&a,&c,&e};return *(pa[t]);}();
    // double &B = [this,t]()->double&{double *pb[3]{&b,&d,&f};return *(pb[t]);}();
    double &A = (*this)[2*t];
    double &B = (*this)[2*t+1];
    A = (*el.first)->pos();
    B = (*el.second)->pos();
    for (const auto p : fb)
      if (!eq(A, p->pos(), i) && !eq(B, p->pos(), i)) return false;
    return true;
  };

  // Set extremities checking alignment of boundaries
  if (!set_extremities(X) || !set_extremities(Y) || !set_extremities(Z))
    throw std_dom_error;

  const Rectangle side_rect[3] {{c,d,e,f}, {a,b,e,f}, {a,b,c,d}};

  // Check if boundaries are confined within side boundary limits
  for (const auto bd : boundaries_)
    if (!side_rect[bd->type].contains(get_rectangle(bd), toaxis(bd->type)))
      throw std_dom_error;

  // Classify boundaries and compute areas
  array<array<vector<Boundary *>,2>,3> classified;
  for (const auto bd : boundaries_)
    classified[bd->type][get_extremity(bd)].push_back(bd);

  // Check overlapping
  for (int j = 0; j < 3; ++j)
  {
    const Axis n = toaxis(j);
    const Axis axis[2] {plane_coordinate_axis(n,0), plane_coordinate_axis(n,1)};
    for (int i = 0; i < 2; ++i)
    {
      vector<Rectangle::Ref> rect_refs;
      vector<fd::RectangleX> rects;
      for (const auto& bd : classified[j][i])
      {
        const Rectangle& r = get_rectangle(bd);
        rect_refs.push_back(r);
        rects.push_back(*(const fd::RectangleX *)&r);
      }
      if (!Rectangle::rect_hull(rect_refs, n).coincides(side_rect[j], n))
        throw "Boundaries extend beyond limits for face "+fd::to_string(2*j+1)+'.';
      if (((const fd::RectangleX*)&side_rect[j])->subtract(rects,n).size() != 0)
        throw "Boundaries do not cover face "+fd::to_string(2*j+1)+'.';
      if (disjoint_boundaries)
      {
        if (!Rectangle::pairwise_disjoint(rect_refs, n))
          throw "Boundaries are not disjoint on face "+fd::to_string(2*j+1)+'.';
        const auto N = classified[j][i].size();
        const double RESOL_AREA = (RESAREA < 0) ?
            area_resol(side_rect[j], N, RESOL(axis[0]), RESOL(axis[1])) :
            RESAREA;
        double area = 0;
        for (const auto &ref: rect_refs) area += ref.get().area(n);
        if (fabs(area - side_rect[j].area(n)) > RESOL_AREA)
          fd::log("Area test failed on face "+fd::to_string(2*j+1)+'.');
      }
    }
  }
}

vector<int> Mesh::get_bids_on_side(int sid) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  vector<int> result;
  for (int i = 0; i < num_boundaries(); ++i)
    if (in_side(i, sid)) result.push_back(i);
  return result;
}

int Mesh::get_sid(Boundary *p) const
{
  ASSERT(p->type != Boundary::G);
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  return sGetSid(p->type, get_extremity(p));
}

int Mesh::get_sid(int bid) const
{ return get_sid(boundaries_[bid]); }

bool Mesh::in_side(int bid, int fid) const
{
  const auto &p = boundaries_[bid];
  ASSERT(p->type != Boundary::G);
  return BTYPE[fid] == p->type && get_extremity(p) == face_extremity(fid);
}

int Mesh::extremity(int bid) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  THROW_IF(bid < 0 || bid >= num_boundaries(), "bid out of bounds.");
  return get_extremity(boundaries_[bid]);
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::::  PDE Utilities  :::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

/*static*/ const char *Mesh::face(int fid) {return FID[fid];}
/*static*/ const char *Mesh::edge(int eid) {return EID[eid];}
/*static*/ const char *Mesh::vertex(int vid) {return VID[vid];}

/*static*/ int Mesh::face_btype(int fid) noexcept {return BTYPE[fid];}

const Rectangle& Mesh::get_rectangle(const Boundary *p) const
{
  THROW_IF(p == nullptr, "Null boundary.");
  THROW_IF(p->type == Boundary::G, "Global boundaries have no rectangle.");
  return static_cast<const Boundary3d*>(p)->rectangle();
}

const Rectangle& Mesh::get_rectangle(int bid) const
{
  THROW_IF(bid < 0 || bid >= num_boundaries(), "bid out of bounds.");
  return get_rectangle(boundaries_[bid]);
}

vector<int> Mesh::get_domain_edges(int bid) const
{
  static int EDGEID[6][4]{
  //   a==a' b==b' c==c' d==d'
  //     |    |    |     |
  //     V    V    V     V
        {BR,  TR,  DR,  UR}, // <-- sid == R
        {TL,  TR,  DT,  UT}, // <-- sid == T
        {BL,  TL,  DL,  UL}, // <-- sid == L
        {BL,  BR,  DB,  UB}, // <-- sid == B
        {UL,  UR,  UB,  UT}, // <-- sid == U
        {DL,  DR,  DB,  DT}, // <-- sid == D
  };

  ASSERT_MSG(bid > -1 && bid < num_boundaries(), "bid out of bounds.");

  const auto p = boundaries_[bid];
  const int sid = get_sid(p);
  const Axis n = toaxis(p->type);
  const Rectangle domain_face = face_rectangle(n);
  const Rectangle &rect = get_rectangle(p);

  vector<int> edges;
  if (eq(rect.a, domain_face.a, n, 0)) edges.push_back(EDGEID[sid][0]);
  if (eq(rect.b, domain_face.b, n, 0)) edges.push_back(EDGEID[sid][1]);
  if (eq(rect.c, domain_face.c, n, 1)) edges.push_back(EDGEID[sid][2]);
  if (eq(rect.d, domain_face.d, n, 1)) edges.push_back(EDGEID[sid][3]);
  return edges;
}

vector<int> Mesh::get_domain_vertices(int bid) const
{
  // The rectangles are considered projected onto their coordinate planes,
  // for example, xBoundary rectangles are considered to be in the yz plane.
  //
  //                           BL   BR   TR   TL 
  //                           |    |    |    |  
  //                           V    V    V    V  
  static const int VID[6][4]{{DBR, DTR, UTR, UBR},  // <-- R
                             {DTL, DTR, UTR, UTL},  // <-- T
                             {DBL, DTL, UTL, UBL},  // <-- L
                             {DBL, DBR, UBR, UBL},  // <-- B
                             {UBL, UBR, UTR, UTL},  // <-- U
                             {DBL, DBR, DTR, DTL}}; // <-- D

  ASSERT_MSG(bid > -1 && bid < num_boundaries(), "bid out of bounds.");

  const auto p = boundaries_[bid];
  const int sid = get_sid(p);
  const Axis n = toaxis(p->type);
  const auto face_vertices = face_rectangle(n).get_vertices();
  const auto rect = get_rectangle(p);

  vector<int> domain_vertices;
  for (int i : {BL, BR, TR, TL})
    if (rect.contains(face_vertices[i], n))
      domain_vertices.push_back(VID[sid][i]);
  return domain_vertices;
}

static
std::tuple<const int*,Axis> sGetIntervalExtremityAxis(const Axis n, int fid2)
{
  constexpr Axis X = Axis::X, Y = Axis::Y, Z = Axis::Z;
  constexpr int R = Mesh::R, L = Mesh::L, T = Mesh::T, B = Mesh::B;

  static const int SID[3][6]{
  /*X*/ {-1,  R, -1,  L,  T,  B},
  /*Y*/ { R, -1,  L, -1,  T,  B},
  /*Z*/ { R,  T,  L,  B, -1, -1}
  };
  static const int EXTR[4][2]{
  /*R*/ {2,3},
  /*T*/ {0,1},
  /*L*/ {2,3},
  /*B*/ {0,1}
  };
  static const Axis SIDE_AXIS[3][6]{
  /*X*/ {X,  Z,  X,  Z,  Y,  Y}, // X is error
  /*Y*/ {Z,  Y,  Z,  Y,  X,  X}, // Y is error
  /*Z*/ {Y,  X,  Y,  X,  Z,  Z}  // Z is error
  };

  if (fid2 == -1) return std::make_tuple(nullptr, Z);
//  ASSERT(SIDE_AXIS[n][fid2] != n);

  const int sid = SID[n][fid2];
  if (sid == -1) return std::make_tuple(nullptr, Z);
  return std::make_tuple(EXTR[sid], SIDE_AXIS[n][fid2]);
}

std::tuple<fd::Interval,Axis> Mesh::get_boundary_side(int bid, int fid2) const
{
  ASSERT(bid < num_boundaries());

  const auto p = boundaries_[bid];
  const auto n = toaxis(p->type);
  const int *x;
  Axis side_axis;

  std::tie(x, side_axis) = sGetIntervalExtremityAxis(n, fid2);
  if (x == nullptr) return std::make_tuple(fd::Interval{0, -1}, n);

  const auto &r = get_rectangle(p);
  return std::make_tuple(fd::Interval{r(x[0]), r(x[1])}, side_axis);
}

std::tuple<fd::Interval,Axis> Mesh::get_face_side(int fid, int fid2) const
{
  const auto n = toaxis(BTYPE[fid]);
  const int *x;
  Axis side_axis;

  std::tie(x, side_axis) = sGetIntervalExtremityAxis(n, fid2);
  if(x == nullptr) return std::make_tuple(fd::Interval{0, -1}, n);

  const auto r = face_rectangle(n);
  return std::make_tuple(fd::Interval{r(x[0]), r(x[1])}, side_axis);
}

std::tuple<fd::Interval,Axis> Mesh::edge_interval(int eid) const
{
  static const Axis EDGE_AXIS[] {
  /* BL */ Axis::Z,
  /* BR */ Axis::Z,
  /* TR */ Axis::Z,
  /* TL */ Axis::Z,
  /* UB */ Axis::X,
  /* UR */ Axis::Y,
  /* UT */ Axis::X,
  /* UL */ Axis::Y,
  /* DB */ Axis::X,
  /* DR */ Axis::Y,
  /* DT */ Axis::X,
  /* DL */ Axis::Y,
  };

  static const int INTERVAL_EXTREMITY[3][2] {
  /* X */ {0,1},
  /* Y */ {2,3},
  /* Z */ {4,5},
  };

  const auto edge_axis = EDGE_AXIS[eid];
  const auto x = INTERVAL_EXTREMITY[edge_axis];
  const auto &m = *this;
  return std::make_tuple(fd::Interval{m[x[0]], m[x[1]]}, edge_axis);
}

static const int EIDS[12][2] {
  /*BL*/ {Mesh::B, Mesh::L},
  /*BR*/ {Mesh::B, Mesh::R},
  /*TR*/ {Mesh::T, Mesh::R},
  /*TL*/ {Mesh::T, Mesh::L},
  /*UB*/ {Mesh::U, Mesh::B},
  /*UR*/ {Mesh::U, Mesh::R},
  /*UT*/ {Mesh::U, Mesh::T},
  /*UL*/ {Mesh::U, Mesh::L},
  /*DB*/ {Mesh::D, Mesh::B},
  /*DR*/ {Mesh::D, Mesh::R},
  /*DT*/ {Mesh::D, Mesh::T},
  /*DL*/ {Mesh::D, Mesh::L}
};

int Mesh::get_meeting_face(int bid, int eid) const
{
  ASSERT(bid < num_boundaries());

  const auto &r = EIDS[eid];
  const int fid = get_sid(bid);
  return fid == r[0] ? r[1] : (fid == r[1] ? r[0] : -1);
}

std::tuple<int,Axis,fd::Interval> Mesh::get_meeting_bid(int bid, int eid) const
{
  ASSERT(bid < num_boundaries());

  const auto &r = EIDS[eid];
  const int fid = get_sid(bid);
  const int fid2 = fid == r[0] ? r[1] : (fid == r[1] ? r[0] : -1);

  THROW_IF(fid2 == -1, "Internal error 0017. Please report this error.");

  Axis n;
  fd::Interval side;
  std::tie(side, n) = get_boundary_side(bid, fid2);

  for (int i = bid + 1; i < num_boundaries(); ++i)
  {
    if (get_sid(i) != fid2) continue;

    Axis curn;
    fd::Interval curside;
    std::tie(curside, curn) = get_boundary_side(i, fid);

    ASSERT(curn == n);

    if (curside.contains(side, n))
      return std::tuple<int,Axis,fd::Interval>{i,n,curside};
  }

  return std::tuple<int,Axis,fd::Interval>{-1,n,fd::Interval{0,-1}};
}

#if 0
int Mesh::bid_from_point(const fd::Point &pt, const vector<int> &face) const
{
  for (int bid : face)
  {
    Boundary3d *p = static_cast<Boundary3d*>(boundaries_[bid]);
    if (p->rectangle().contains_at(p->pos(), pt, toaxis(p->type))) return bid;
  }
  return -1;
}

int Mesh::node(double x, Axis i) const
{
  const double hh = spacing(i);
  const double x0 = coord(2*i);
#if USE_EXTREMELY_CONSERVATIVE_ROUNDING
  int nd = std::round((x - x0)/hh);
  return std::fabs(x0 + nd*hh - x) < RESOL(i) ? nd : (
      std::fabs(x0 + (nd+1)*hh - x) < RESOL(i) ? nd+1 : nd-1);
#else // !USE_EXTREMELY_CONSERVATIVE_ROUNDING
  return std::round((x - x0)/hh);
#endif // USE_EXTREMELY_CONSERVATIVE_ROUNDING
}

array<int,3> Mesh::node(const fd::Point &pt) const
{
  constexpr Axis X = Axis::X, Y = Axis::Y, Z = Axis::Z;
  return {node(pt(X), X), node(pt(Y), Y), node(pt(Z), Z)};
}
#endif // 0

//============================================================================//
//:::::::::::::::::::::::::::::::  CubicMesh  :::::::::::::::::::::::::::::::://
//============================================================================//

CubicMesh::CubicMesh(int nx, int ny, int nz,
    double a, double b, double c, double d, double e, double f) :
  Mesh(nx, ny, nz)
{
  THROW_IF(!lt(a, b, Axis::X) || !lt(c, d, Axis::Y) || !lt(e, f, Axis::Z),
           "incorrect specification of RectangularMesh.");
  xb_[0] = xBoundary(a, {c, d, e, f});
  xb_[1] = xBoundary(b, {c, d, e, f});
  yb_[0] = yBoundary(c, {a, b, e, f});
  yb_[1] = yBoundary(d, {a, b, e, f});
  zb_[0] = zBoundary(e, {a, b, c, d});
  zb_[1] = zBoundary(f, {a, b, c, d});
  set_boundaries({xb_[0], xb_[1], yb_[0], yb_[1], zb_[0], zb_[1]});
}

} // namespace fd3
