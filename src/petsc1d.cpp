#include <cstdlib>
#include <cmath>
#include <ctime>
#include "pde1d.hpp"

using std::string;

namespace fd1 {

// =============================================================================
// PDE
// =============================================================================

// -----------------------------------------------------------------------------
// Code Generator
// -----------------------------------------------------------------------------

void PDE::generate_code_function_body_petsc_nonlinear(int what)
{
  if (what != GENERATE_FUNCTION_CODE && what != GENERATE_JACOBIAN_CODE)
    THROW("Incorrect use of method.");

  // ---------------------------------------------------------------------------
  // Generate function body
  // ---------------------------------------------------------------------------

  const int nc = components();
  const string rv(what == GENERATE_FUNCTION_CODE ? "F" : "J");
  const string rt(what == GENERATE_FUNCTION_CODE ?
	R"(// Instruct compiler to handle F[eq] as if it were a double
	struct {
		double value;
		inline operator double() const noexcept {return value;}
		inline double &operator[](int) noexcept {return value;}
		inline void operator=(double x) noexcept {value=x;}
	})" : "SparseVector");

  { const string decl_c(nc == 1 ? "" : "const int c = ");
    code_ << R"(
{
	)"<<rt<<" "<<rv<<R"(;
	int i;
	)"<<decl_c<<R"(field_index_to_mesh_coordinates(eq, &i);)"; } // End of block

  const bool need_extended_boundary = (
      boundary_discretization_method_ == SYM_DIFF2) && has_neumann_BCs();

  // ---------------------------------------------------------------------------
  auto generate_code_at = [this, what, nc](int sid, int ntabs, int flags)
  {
    auto format_element = [](const string&,
        const string&, const string&, const string&) {return string("eq");};

    const int bid = (sid == Mesh::R ? 1 : 0);
    if (nc > 1)
    {
      ntabs += 2;
      code_ << R"(
		switch (c)
		{)";
    }
    for (int c = 0; c < nc; ++c)
    {
      if (nc > 1) code_ << R"(
			case )"<<c<<":";
      if (boundary_discretization_method_ == SYM_DIFF2)
        generate_code_SYM_DIFF2_for(c, bid, what, ntabs, flags, format_element);
      else
        generate_code_for(c, bid, what, ntabs, format_element);
      if (nc > 1 && c < nc-1) code_ << R"(
				break;)";
    }
    if (nc > 1) code_ << R"(
		})";
  }; //-------------------------------------------------------------------------

  code_ << std::endl;

  if (!generate_code_function_body_replace_eqs(what, 0, "c", "eq", false))
    code_ << R"(
	)";

  if (need_extended_boundary) // -------- need_extended_boundary ---------------
  {
    // Code for extended boundary and virtual corners -- active for SYM_DIFF2
    code_ << R"(if (i == -1)
	{)";
    generate_code_at(Mesh::L, 0, ORDINARY_BC_CODE);
    code_ << R"(
	})";
    code_ << R"(
	else if (i == nx)
	{)";
    generate_code_at(Mesh::R, 0, ORDINARY_BC_CODE);
    code_ << R"(
	}
	else )";
  } // ---------------------- end need_extended_boundary -----------------------

  code_ << R"(if (i == 0)
	{)";
  generate_code_at(Mesh::L, 0, EXTRA_BC_CODE);
  code_ << R"(
	}
	else if (i == nx-1)
	{)";
  generate_code_at(Mesh::R, 0, EXTRA_BC_CODE);
  code_ << R"(
	}
	else
	{)";

  const string tabs(nc > 1 ? 2 : 0, '\t');
  if (nc > 1) code_ << R"(
		switch (c)
		{)";
  for (int c = 0; c < nc; ++c)
  {
    if (nc > 1) code_ << R"(
			case )"<<c<<":";
    if (what == GENERATE_FUNCTION_CODE) code_ << R"(
		)" << tabs << "F[eq] = " << equation_codes_[c].text_y << ";";
    else if (what == GENERATE_JACOBIAN_CODE) code_ << R"(
		)" << tabs << "J[eq] = " << equation_codes_[c].text_j << ";";
    if (nc > 1 && c < nc-1) code_ << R"(
				break;)";
  }
  if (nc > 1) code_ << R"(
		})";

  code_ << R"(
	})";

  code_ << R"(
	return )"<<rv<<(what == GENERATE_FUNCTION_CODE ? ".value" : "[eq]")<<R"(;
}
)";
}

} // namespace fd1
