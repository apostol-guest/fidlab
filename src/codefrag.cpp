#include "codefrag.h"

using std::string;

namespace fd {

static constexpr int NBS = sizeof(CodeFragment::text_bx_y)/
                           sizeof(CodeFragment::text_bx_y[0]);
static constexpr int NVS = sizeof(CodeFragment::text_vx_y)/
                           sizeof(CodeFragment::text_vx_y[0]);

/*static*/ int CodeFragment::sizeof_bx() noexcept {return NBS;}
/*static*/ int CodeFragment::sizeof_vx() noexcept {return NVS;}

CodeFragment::CodeFragment(const string& code_y, const string& code_j,
                           Format h_y, Format h_j) :
    bid      (-1),
    terminal (true),
    text_y   (code_y),
    text_j   (code_j),
    hint_y   (h_y),
    hint_j   (h_j)
{
  for (int i = 0; i < NBS; ++i)
  {
    text_bx_y[i] = text_bx_j[i] = "";
    hint_bx_y[i] = hint_bx_j[i] = NO_BRACKETS;
  }
  for (int i = 0; i < NVS; ++i)
  {
    text_vx_y[i] = text_vx_j[i] = "";
    hint_vx_y[i] = hint_vx_j[i] = NO_BRACKETS;
  }
}

bool CodeFragment::no_bx_code() const
{
  bool b = text_bx_y[0].empty();
  for (int i = 1; i < NBS; ++i) b = b && text_bx_y[i].empty();
  return b;
}

bool CodeFragment::no_vx_code() const
{
  bool b = text_vx_y[0].empty();
  for (int i = 1; i < NVS; ++i) b = b && text_vx_y[i].empty();
  return b;
}

} // namespace fd
