#include "global_config.h"
#include "basedefs.h"
#include "config_code.h"

using std::string;

namespace fd {

static GlobalConfig s_global_config {
    DEF_MAXLEN_INLINE_IMPL,
    // Space resolutions
    {DEF_SPACE_X_ACCURACY, DEF_SPACE_Y_ACCURACY, DEF_SPACE_Z_ACCURACY},
    // Area resolution
    -(DEF_SPACE_X_ACCURACY+DEF_SPACE_Y_ACCURACY+DEF_SPACE_Z_ACCURACY),
    DEF_MAX_BACKUP_FILES,
#ifdef __DEBUG__
    fd::EXCEPTION,
    fd::EXCEPTION,
#else
    fd::NOTE,
    fd::NOTE,
#endif // __DEBUG__
    // Numerical differentiation accuracy
    1.e-4,
    // Numerical differentiation zero
    1.e-30
};

void GlobalConfig::format_function_implementation(string& imp) const
{
  string tmp = fd::strip_space(imp);
  if (tmp.empty())
  {
    imp = tmp;
    return;
  }
  if (tmp[0] == '{') return;
  if (tmp.length() > 2 && (tmp.find("/*") == 0 || tmp.find("//") == 0)) return;
  imp = tmp;
  if ((int)imp.length() <= max_length_inline_impl)
    imp = " {return " + imp + ";}";
  else
    imp = "\n\t{\n\t\treturn " + imp + ";\n\t}";
}

GlobalConfig& global_config() noexcept {return s_global_config;}

} // namespace fd
