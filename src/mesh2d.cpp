#include <cstdlib>
#include <cmath>
#include <iostream>
#include "mesh2d.hpp"
#include "global_config.h"

using std::string;
using std::vector;
using std::array;
using namespace fd2;

using fd::Boundary;

#if 0
                1
         +-------------+
         |             |
       2 |             | 0
         |             |
         +-------------+
                3
// FIG. The indicated numbers are the sid's (ids of boundary sides).
#endif

/**
 *  Returns boundary side identifier.
 *
 *  \param bdtype The type of boundary (Boundary::X, Y)
 *  \param extrem The extremity of the boundary's interval; 0: start, 1: end.
 *  \return The side identifier, Mesh::R, Mesh::T etc.
 */
static int sGetSid(int bdtype, int extrem)
{
  static const int SID[2][2] = {{Mesh::L, Mesh::R},
                                {Mesh::B, Mesh::T}};
  ASSERT((extrem == 0 || extrem == 1) &&
         (bdtype == Boundary::X || bdtype == Boundary::Y));
  return SID[bdtype][extrem];
}

static inline double dist(double p, double q) {return std::fabs(p-q);}

static inline bool xlt(double p, double q)
{ return p < q - fd::global_config().space_resolution[1]; }
static inline bool ylt(double p, double q)
{ return p < q - fd::global_config().space_resolution[0]; }
static inline bool xgt(double p, double q)
{ return p > q + fd::global_config().space_resolution[1]; }
static inline bool ygt(double p, double q)
{ return p > q + fd::global_config().space_resolution[0]; }
static inline bool xneq(double p, double q)
{ return dist(p,q) > fd::global_config().space_resolution[1]; }
static inline bool yneq(double p, double q)
{ return dist(p,q) > fd::global_config().space_resolution[0]; }

static inline double xpos(const Boundary *bd)
{ return static_cast<const xBoundary*>(bd)->pos(); }
static inline double ypos(const Boundary *bd)
{ return static_cast<const yBoundary*>(bd)->pos(); }
static inline double y0(const Boundary *bd)
{ auto p = static_cast<const xBoundary*>(bd); return std::min(p->y0(),p->y1()); }
static inline double y1(const Boundary *bd)
{ auto p = static_cast<const xBoundary*>(bd); return std::max(p->y0(),p->y1()); }
static inline double x0(const Boundary *bd)
{ auto p = static_cast<const yBoundary*>(bd); return std::min(p->x0(),p->x1()); }
static inline double x1(const Boundary *bd)
{ auto p = static_cast<const yBoundary*>(bd); return std::max(p->x0(),p->x1()); }

// =============================================================================
// Mesh
// =============================================================================

Mesh &Mesh::set_boundary(Boundary &bc)
{
  THROW_IF(finalized(), "Bad use of method, mesh is finalized.");
  boundaries_.push_back(&bc);
  return *this;
}

void Mesh::set_boundaries(std::initializer_list<Boundary::Ref> l)
{
  for (auto& ref : l) set_boundary(ref.self);
  finalize();
}

void Mesh::finalize()
{
  for (auto bd : boundaries_)
    if (bd == nullptr)
      throw _S"Null boundary.";
    else if (bd->type != Boundary::X && bd->type != Boundary::Y)
      throw _S"Incorrect boundary type.";
  THROW_IF(!order_boundaries(), "Incorrect specification of boundaries.");
  hx = (b - a)/double(nx - 1);
  hy = (d - c)/double(ny - 1);
  finalized_ = true;
}

bool Mesh::order_boundaries()
{
  auto set_x_extremities = [this]() -> bool {
    std::vector<Boundary *> fb;
    for (const auto bd : boundaries_)
      if (bd->type == Boundary::X) fb.push_back(bd);
    if (fb.size() < 2) return false;
    auto e = std::minmax_element(fb.begin(), fb.end(),
        [](Boundary *p, Boundary *q) {return ylt(xpos(p), xpos(q));});
    a = xpos(*e.first);
    b = xpos(*e.second);
    for (const auto bd : fb)
      if (yneq(a, xpos(bd)) && yneq(b, xpos(bd))) return false;
    return true;
  };

  auto set_y_extremities = [this]() -> bool {
    std::vector<Boundary *> fb;
    for (const auto bd : boundaries_)
      if (bd->type == Boundary::Y) fb.push_back(bd);
    if (fb.size() < 2) return false;
    auto e = std::minmax_element(fb.begin(), fb.end(),
        [](Boundary *p, Boundary *q) {return xlt(ypos(p), ypos(q));});
    c = ypos(*e.first);
    d = ypos(*e.second);
    for (const auto bd : fb)
      if (xneq(c, ypos(bd)) && xneq(d, ypos(bd))) return false;
    return true;
  };

  // Set extremities checking alignment of boundaries
  if (!set_x_extremities() || !set_y_extremities()) return false;

  // Check if boundaries are confined within extremities
  for (const auto bd : boundaries_)
  {
    switch (bd->type)
    {
      case Boundary::X: if (xlt(y0(bd), c) || xgt(y1(bd), d)) return false; break;
      case Boundary::Y: if (ylt(x0(bd), a) || ygt(x1(bd), b)) return false;
    }
  }

  // Classify boundaries and compute lengths
  std::vector<Boundary *> xb[2], yb[2];
  double xlen[2]{0,0}, ylen[2]{0,0};
  for (const auto bd : boundaries_)
  {
    const int e = get_extremity(bd);
    if (bd->type == Boundary::X)
    {
      xb[e].push_back(bd);
      xlen[e] += y1(bd) - y0(bd);
    }
    else
    {
      yb[e].push_back(bd);
      ylen[e] += x1(bd) - x0(bd);
    }
  }

  // Check lengths
  if (xneq(xlen[0], d-c) || xneq(xlen[1], d-c)) return false;
  if (yneq(ylen[0], b-a) || yneq(ylen[1], b-a)) return false;

  // Sort boundaries
  for (int i = 0; i < 2; ++i)
  {
    std::sort(xb[i].begin(), xb[i].end(), [i](Boundary *p, Boundary *q) {
      const double d = y0(p) - y0(q); return (i == 0 ? -d : d) < 0;});
    std::sort(yb[i].begin(), yb[i].end(), [i](Boundary *p, Boundary *q) {
      const double d = x0(p) - x0(q); return (i == 0 ? d : -d) < 0;});
  }
  boundaries_.assign(xb[1].begin(), xb[1].end());
  boundaries_.insert(boundaries_.cend(), yb[1].begin(), yb[1].end());
  boundaries_.insert(boundaries_.cend(), xb[0].begin(), xb[0].end());
  boundaries_.insert(boundaries_.cend(), yb[0].begin(), yb[0].end());

  return true;
}

int Mesh::get_extremity(const Boundary *p) const
{ return p->type == Boundary::X ? int(yneq(a,xpos(p))) : int(xneq(c,ypos(p))); }

array<double,2> Mesh::get_boundary_extremities(int bid) const
{
  Boundary *p = boundaries_[bid];
  return (p->type == Boundary::X) ? array<double,2>{y0(p),y1(p)}
                                  : array<double,2>{x0(p),x1(p)};
}

vector<vector<int>> Mesh::get_bids_by_side() const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  vector<vector<int>> result;
  vector<int> cur;
  for (int i = 0, type = Boundary::X; i < (int)boundaries_.size(); ++i)
  {
    Boundary *p = boundaries_[i];
    if (p->type != type)
    {
      type = p->type;
      result.push_back(cur);
      cur.clear();
    }
    cur.push_back(i);
  }
  result.push_back(cur);
  return result;
}

vector<int> Mesh::get_bids_on_side(int sid) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  int i = 0, type = Boundary::X;
  ASSERT(type == boundaries_[0]->type);
  if (get_sid(boundaries_[0]) != sid)
    for (i = 1; i < (int)boundaries_.size(); ++i)
    {
	  Boundary *p = boundaries_[i];
	  if (p->type != type)
      {
        type = p->type;
        if (get_sid(p) == sid) break;
      }
    }
  vector<int> result;
  while (i < (int)boundaries_.size() && boundaries_[i]->type == type)
    result.push_back(i++);
  return result;
}

int Mesh::get_sid(Boundary *p) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  return sGetSid(int(p->type == Boundary::Y), get_extremity(p));
}

int Mesh::get_sid(int bid) const
{ return get_sid(boundaries_[bid]); }

int Mesh::extremity(int bid) const
{
  THROW_IF(!finalized_, "Bad use of method, mesh is not finalized.");
  THROW_IF(bid < 0 || bid >= (int)boundaries_.size(), "bid out of bounds.");
  return get_extremity(boundaries_[bid]);
}

// =============================================================================
// RectangularMesh
// =============================================================================
RectangularMesh::RectangularMesh(int nx, int ny,
    double a, double b, double c, double d) : Mesh(nx, ny)
{
  THROW_IF(!ylt(a, b) || !xlt(c, d), "incorrect specification of RectangularMesh.");
  xb_[0] = xBoundary(a, c, d);
  xb_[1] = xBoundary(b, c, d);
  yb_[0] = yBoundary(c, a, b);
  yb_[1] = yBoundary(d, a, b);
  set_boundaries({xb_[0], xb_[1], yb_[0], yb_[1]});
}
