#include <iostream>
#include "pde.hpp"
#include "config_code.h"

using std::string;

namespace fd {

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::::::::  Code  :::::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::generate_code_gsl(int what)
{
  // ---------------------------------------------------------------------------
  // Generate implementation
  // ---------------------------------------------------------------------------

  string funcname, outvars, invars;
  switch (what)
  {
    case GENERATE_CLASS_CODE:
      generate_code_class_gsl();
      return;
    case GENERATE_FUNCTION_CODE:
      funcname = "func";
      outvars = "vector<double>& F, ";
      invars = "const vector<double>& u";
      break;
    case GENERATE_JACOBIAN_CODE:
      funcname = "jac" ;
      outvars = "vector<double>& J, ";
      invars = "const vector<double>& u, const vector<double>& du";
      break;
    case GENERATE_BOTH:
      funcname = "ffj" ;
      outvars = "vector<double>& F, vector<double>& J, ";
      invars = "const vector<double>& u, const vector<double>& du";
      break;
    default: THROW("bad parameter");
  }

  // Write function code
  code_ << R"(
void )"<<name<<"::"<<funcname<<"("<<outvars<<invars<<")";
  generate_code_function_body(what);
}

void PDECommon::generate_code_class_gsl()
{
  const int nc = components();

  string ie_imp = has_initial_estimate() ?
"double "+format_space_function_declaration(name+"::initial_estimate", nc > 1)+
      init_estimate_imp_ : "";

  string fn_imp;
  if (use_field_names && nc > 1)
  {
    string field_names;
  	const string sep = (nc <= 3) ? ", " : ",\n\t\t";
  	for (int i = 0; i < nc; ++i)
  	{
  	  if (i == 0) field_names += (nc <= 3 ? "" : "\n\t\t");
  	  else field_names += sep;
  	  field_names += "\""+field_[i].name+"\"";
  	}
  	if (nc > 3) field_names += "\n\t";

    fn_imp =
"const std::string& "+name+R"(::field_name(int c) const
{
	static const std::string s_field_names[]{)"+field_names+R"(};
	if (c < 0 || c > )"+fd::to_string(nc-1)+R"()
		throw std::string(__PRETTY_FUNCTION__)+": field index out of bounds.";
	return s_field_names[c];
})";
  }

  if (!fn_imp.empty() && !ie_imp.empty()) ie_imp += "\n\n";
  if (!ie_imp.empty() || !fn_imp.empty()) code_ << R"(
)"<<ie_imp<<fn_imp<<R"(
)";

  generate_code_class_extra();
}

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::::::::  Main  :::::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::generate_main_gsl()
{
  if (linear())
  {
    code_ << "using namespace gsl;\n";
    generate_main_gsl_linear();
  }
  else
  {
    code_ << "#include " << (system_installed_gslpp ? "<gsl++/gslroot.hpp>\n\n" 
                                                    : "\"gslroot.hpp\"\n\n")
          << "using namespace gsl;";
  	generate_main_gsl_nonlinear();
  }
}

void PDECommon::generate_main_gsl_linear()
{
  if (matrix_method_ == GMRES)
  {
    code_ << R"(
static double norm_inf(const vector<double>& v)
{
	double m = 1.e300;
	for (int i = 0; i < v.size(); ++i)
		if (fabs(v[i]) < m) m = fabs(v[i]);
	return m;
}

int main()
{
	)" << format_constructor_comment() << R"(
	)"<<name<<R"( pde;
	const int N = pde.dim();
	sp::matrix<double> A(N, N);
	vector<double> b(N), x(N);)";
    code_ << R"(

	// Build matrix
	{
		sp::matrix<double> B(N, N);
		x.set_basis(0);
		for (int i = 0; i < N; ++i)
		{
			if (i != 0) x.swap_elements(i-1,i);
			pde.jac(b, /* unused */x, x);
			double m = norm_inf(b)*1.e-16;
			for (int j = 0; j < N; ++j)
				if (b[j] > m) B(j, i) = b[j];
		}
		A = B.ccs();
	}
	// Build RHS
	x.set_zero();
	pde.func(b, x);
	b *= -1;)";
  }
  else // !GMRES
  {
    code_ << R"(
int main()
{
	)" << format_constructor_comment() << R"(
	)"<<name<<R"( pde;
	const int N = pde.dim();
	matrix<double> A(N, N);
	vector<double> b(N), x(N);)";
    code_ << R"(

	// Build matrix
	x.set_basis(0);
	for (int i = 0; i < N; ++i)
	{
		if (i != 0) x.swap_elements(i-1,i);
		pde.jac(b, /* unused */x, x);
		A.set_col(i, b);
	}
	// Build RHS
	x.set_zero();
	pde.func(b, x);
	b *= -1;)";
  }
  switch (matrix_method_)
  {
    case LU: code_ << R"(

	// Solve the system
	{
		int s;
		permutation p(N);
		LU::decomp(A, p, &s);
		LU::solve(A, p, b, x);
	})";
      break;
    case QR: code_ << R"(

	// Solve the system
	{
		vector<double> tau(N);
		QR::decomp(A, tau);
		QR::solve(A, tau, b, x);
	})";
      break;
    case QRP: code_ << R"(

	// Solve the system
	{
		int s;
		permutation p(N);
		vector<double> tau(N), norm(N);
		QRP::decomp(A, tau, p, &s, norm);
		QRP::solve(A, tau, p, b, x);
	})";
      break;
    case HOUSEHOLDER: code_ << R"(

	// Solve the system
	householder::solve(A, b, x);)";
      break;
    case GMRES: code_ << R"(

	// Solve the system
	sp::itersolve<double> gmres(N);
  int status = gmres.solve(A, b, x, -1., [&gmres](vector<double>& u, int iter) {
		static const int max_iter = 1000;
		// Remove comment to display progress (requires header <iostream>).
//    std::cout << "Iteration "<<iter<<" residual = " << gmres.normr() << "\n";
    return iter < max_iter;
  });
		// Remove comments to display convergence.
//  if (status == GSL_SUCCESS) std::cout << "GMRES converged.\n";
//  else std::cout << "GMRES did not converge\n";)";
      break;
    default: throw _S"Unknown GSL linear solver.";
  }
  code_ << R"(
)";

  generate_solution_output();

  code_ << R"(

	return 0;
}

)";
}

void PDECommon::generate_main_gsl_nonlinear()
{
  // HYBRIDS,  HYBRID,  DNEWTON, BROYDEN --> multiroot::fsolver
  // HYBRIDSJ, HYBRIDJ, NEWTON,  GNEWTON --> multiroot::fdfsolver
  const bool uses_jacobian = nonlinear_solver_ == HYBRIDSJ ||
      nonlinear_solver_ == HYBRIDJ ||
      nonlinear_solver_ == NEWTON  ||
      nonlinear_solver_ == GNEWTON;
  const string solver_class = _S(uses_jacobian ? "fd" : "") + "fsolver";
  const string solution_method = [this]() -> string {
    switch (nonlinear_solver_) {
      case HYBRIDS:  return _S"HYBRIDS";
      case HYBRID:   return _S"HYBRID";
      case DNEWTON:  return _S"DNEWTON";
      case BROYDEN:  return _S"BROYDEN";
      case HYBRIDSJ: return _S"HYBRIDSJ";
      case HYBRIDJ:  return _S"HYBRIDJ";
      case NEWTON:   return _S"NEWTON";
      case GNEWTON:  return _S"GNEWTON";
      default: throw _S"Unknown nonlinear solution method.";
    }
    return _S"";
  }();
  const int nc = components();
  const string locvar[4]{
  	"",
  	"const int &i = ia[0];",
  	"const int &i = ia[0], &j = ia[1];",
  	"const int &i = ia[0], &j = ia[1], &k = ia[2];"
  };

  code_ << R"(
using namespace gsl::multiroot;
using gsl::vector;
)";
  if (uses_jacobian)
  code_ << R"(using gsl::matrix;
)";

  code_ << R"(
//! Solution progress displayer.
/*! \c how_many negative means "all".
 *  \c iter negative means "do not display iteration number". */
void display_current(const )"<<solver_class<<R"(& solver, int iter, int how_many)
{
	vector<double>::reference x = solver.root();
	vector<double>::reference y = solver.f();
	vector<double>::reference d = solver.dx();
	if (how_many < 0) how_many = x.size();

	if (iter >= 0)
		std::cout << "iteration: " << iter << '\n';
	if (how_many > 0) std::cout << "x = [";
	for (int i = 0; i < how_many; ++i)
	{
		std::cout << x[i];
		if (i == how_many - 1) std::cout << "]\n";
		else std::cout << " ";
	}

	auto norm2 = [](const vector<double>::reference& v) -> double {
		const int n = v.size();
		double sum = 0.;
		for (int i = 0; i < n; ++i) sum += v[i]*v[i];
		return sqrt(sum/double(n));
	};

	std::cout << "|f(x)|_2 = " << norm2(y) << " |dx|_2 = " << norm2(d) << '\n';
}

int main()
{
	)" << format_constructor_comment() << R"(
	)"<<name<<R"( pde;
	const int N = pde.dim();
	)"<<solver_class<<" solver("<<solver_class<<"::"<<solution_method<<","<<R"(N);
  // Initial estimate:
	vector<double> x_init(N);
	)"<<(has_initial_estimate() ?
	R"(for (int el = 0; el < N; ++el)
	{
		int ia[)"+fd::to_string(dimension())+R"(];
		)"+_S(nc > 1 ? "int c = " : "")+
		R"(pde.field_index_to_mesh_coordinates(el, ia);
		)"+locvar[dimension()]+R"(
		x_init[el] = pde.initial_estimate()"+format_coord()+R"();
	}
)" :
	R"(// Default initialization zeroes x_init. Change as appropriate.
	x_init.set_zero();
)");
  if (uses_jacobian) code_ << R"(
	// Function to be solved and its Jacobian.
	auto f = [&pde](const vector<double>& x, vector<double>& y, matrix<double>& J)
	{
		if (!y.is_null()) pde.func(y,x);
		if (!J.is_null())
		{
			const int N = (int) x.size();
			vector<double> r(N), b(N);
			r.set_basis(0);
			for (int i = 0; i < N; ++i)
			{
				if (i != 0) r.swap_elements(i-1,i);
				pde.jac(b, x, r);
				J.set_col(i, b);
			}
    }
    return GSL_SUCCESS;
  };
)";

  if (uses_jacobian) code_ << R"(
  solver.set(x_init, f);
)";
  else code_ << R"(
  solver.set(x_init, [&pde](const vector<double>& x, vector<double>& y) {
    pde.func(y,x);
    return GSL_SUCCESS;
  });
)";

  code_ << R"(
	// Modify as required.
	int max_iter = 30;
	const double req_accur = 1.e-7;
	// Lambda's return value indicates continuation.
  int status = solver.solve(max_iter,[&solver,req_accur](int& status,int iter) {
		// Default progress display.
		display_current(solver, iter, 0);
		// Check if solver is stuck.
    if (status != 0) return false;
		// Check if required accuracy has been attained.
		status = fsolver::test_residual(solver.f(), req_accur);
    return status == GSL_CONTINUE;
  });

  std::cout << "Status: " << gsl_strerror(status) << '\n';)";
  code_ << R"(
)";

  code_ << R"(
	// Preparation for solution output.
  vector<double>::reference x = solver.root();
)";
  generate_solution_output();
  code_ << R"(

	return 0;
}

)";
}

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::::::::  Header  ::::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

void PDECommon::generate_header_funcs_gsl()
{
  const string nsp = libs_[library_]; // namespace
  code_h_ << R"(
	// Functions generating problem discretization.
	void ffj ()"<<nsp<<"::vector<double>& F, "<<nsp<<R"(::vector<double>& J,
						const )"<<nsp<<"::vector<double>& u, const "<<nsp<<R"(::vector<double>& du);
	void func()"<<nsp<<"::vector<double>& F, const "<<nsp<<R"(::vector<double>& u);
	void jac ()"<<nsp<<R"(::vector<double>& J,
						const )"<<nsp<<"::vector<double>& u, const "<<nsp<<R"(::vector<double>& du);)";
}

string PDECommon::format_header_compare_solution_iter_gsl() const
{
  const int dim = dimension();
  const string f = (components() == 1) ? "" : "field_number";
  string s = R"(
		for (int i = 0; i < nx; ++i))";
  if (dim > 1) s += R"(
			for (int j = 0; j < ny; ++j))";
  if (dim > 2) s += R"(
				for (int k = 0; k < nz; ++k))";
  const string ntabs(1+dim, '\t');
  return s + R"(
)"+ntabs+R"({
)"+ntabs+R"(	const double d = fabs(v[)"+
                  format_node_function_call("e","i","j","k",f)+"] - "+
                  format_function_call("exact_solution","i","j","k",f)+R"();
)"+ntabs+R"(	n1 += d;
)"+ntabs+R"(	n2 += d*d;
)"+ntabs+R"(	if (d > nsup) nsup = d;
)"+ntabs+R"(})";
}

void PDECommon::generate_header_compare_solution_gsl()
{
  const int nc = components();
  const string nsp = libs_[library_]; // namespace
  const string f  = (nc == 1) ? "" : "field_number,";
  const string fi = (nc == 1) ? "" : "int field_number,";
  const string ref = linear() ? "" : "::reference";
  const string nargs = (dimension() > 1 ? "(" : "") + format_num_nodes() +
                       (dimension() > 1 ? ")" : "");
  code_h_ << R"(

	// Comparison with exact solution
	double dnorm1()"<<fi<<"const "<<nsp<<R"(::vector<double>)"<<ref<<R"(& v)
	{ double norms[3]; dnorms()"<<f<<R"(v, norms); return norms[1]; }
	double dnorm2()"<<fi<<"const "<<nsp<<R"(::vector<double>)"<<ref<<R"(& v)
	{ double norms[3]; dnorms()"<<f<<R"(v, norms); return norms[2]; }
	double dnormsup()"<<fi<<"const "<<nsp<<R"(::vector<double>)"<<ref<<R"(& v)
	{ double norms[3]; dnorms()"<<f<<R"(v, norms); return norms[0]; }
	void dnorms()"<<fi<<"const "<<nsp<<R"(::vector<double>)"<<ref<<R"(& v, double norms[3])
	{
		double n2 = 0, n1 = 0, nsup = 0;)"<<
    format_header_compare_solution_iter_gsl()<<R"(
		norms[0] = nsup;
		norms[1] = n1/)"<<nargs<<R"(;
		norms[2] = sqrt(n2/)"<<nargs<<R"();
	})";
}

} // namespace fd
