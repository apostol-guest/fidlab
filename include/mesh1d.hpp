#ifndef MESH1D_HPP_INCLUDED
#define MESH1D_HPP_INCLUDED

#include <vector>
#include "mesh.hpp"

// =============================================================================
// Mesh
// =============================================================================

namespace fd1
{
/**
 *  \brief One-dimensional mesh representation.
 */
class Mesh : public fd::MeshCommon
{
  using Boundary = fd::Boundary;
  using BoundaryCondition = fd::BoundaryCondition;

protected:

  /**
   *  \brief Interval extremities.
   *
   *  Not to be confused with boundary identifiers.
   */
  enum {
    LEFT = 0, //!< Left extremity
    RIGHT = 1 //!< Right extremity
  };

public:

  int nx;      //!< Number of mesh knots
  double a, b; //!< Solution interval
  double hx;   //!< Mesh spacing

  /** Mesh constructor. */
  Mesh(int N) : nx(N) {};
  /** Add boundary to mesh. */
  Mesh &set_boundary(Boundary &b);
  /** Add boundary to mesh. */
  inline Mesh &add_boundary(Boundary &b) {return set_boundary(b);}
  /** Add boundaries to mesh. */
  void set_boundaries(std::initializer_list<Boundary::Ref> l);

  /** Finalize \c Mesh.
   *
   *  The Mesh is automatically finalized after calling \a set_boundaries.
   *  Usually, you should not have to call this method directly.
   *  \sa set_boundaries.
   */
  void finalize();

  /** Get extremity of boundary (0 or 1).
   *
   *  For 1-d meshes extremity coincides with the bid.
   *  \sa extremity(int)
   */
  int extremity(Boundary *p) const;
  /** Get extremity of boundary (0 or 1).
   *
   *  \sa extremity(Boundary *)
   */
  inline int extremity(int bid) const {return bid;}
  /** Get the side id (\c sid) of the boundary.
   *
   *  A side id (\c sid) is one Mesh::R, B etc.
   */
  int get_sid(Boundary *p) const;
  /** Get the side id (\c sid) of the boundary.
   *
   *  \sa get_bid(Boundary *).
   */
  int get_sid(int bid) const;

protected:

  bool order_boundaries();

private:

  bool check_boundaries() const;
};

} // namespace fd1

#endif // MESH1D_HPP_INCLUDED
