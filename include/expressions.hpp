#ifndef EXPRESSIONS_H_INCLUDED
#define EXPRESSIONS_H_INCLUDED

#include <utility>
#include "discretizer.hpp"
#include "field.hpp"
#include "operations.hpp"

#ifndef DECL_EXPRITEM_DUP

#define DECL_EXPRITEM_DUP() \
  virtual ExpressionItem *duplicate() const
#define IMPLEMENT_EXPRITEM_DUP(t) \
ExpressionItem *t::duplicate() const \
{ t *p = new t(nullptr); *p = *this; return p; }

#endif // DECL_EXPRITEM_DUP

namespace fd
{

class Expression;
class ExpressionItem;
class Function;
class Constant;

struct FixupInfo
{
  const std::vector<int>& target;
  const std::vector<int>& result;
  FixupInfo(const std::vector<int>& tgt, const std::vector<int>& res) :
    target(tgt), result(res) {}
};

typedef std::pair<const ExpressionItem *, int> UnresolvedRef;

struct ExpressionRef
{
  Expression *ptr;
  ExpressionRef(Expression *expr) : ptr(expr) {}
};

//============================================================================//
//:::::::::::::      ExpressionItem BasicOperator Constant       ::::::::::::://
//:::::::::::::  Function FieldFunction CutoffFunction Operator  ::::::::::::://
//============================================================================//

class ExpressionItem
{
protected:

  //! Expression item discretizer.
  /*! Owned by the \c ExpressionItem */
  Discretizer *discretizer_;

  //! Basic constructor.
  ExpressionItem(Discretizer *d) : discretizer_(d) {}

public:

  using CodeFragment = fd::CodeFragment;

  enum {BASIC_OPERATOR = Discretizer::BASIC_OPERATOR,
        FUNCTION = Discretizer::FUNCTION,
        CONST_FIELD = Discretizer::CONST_FIELD,
        FIELD_FUNCTION = Discretizer::FIELD_FUNCTION,
        CUTOFF_FUNCTION = Discretizer::CUTOFF_FUNCTION,
        DIFF_EXPR};

  DECL_EXPRITEM_DUP();

  //! Destructor.
  /*! Deletes the discretizer. */
  virtual ~ExpressionItem() {if (discretizer_ != nullptr) delete discretizer_;}

  inline const char *name   () const {return discretizer_->name();}
  inline int total_order    () const {return discretizer_->total_order();}
  inline IntTriple order    () const {return discretizer_->order();}
  inline int type           () const {return discretizer_->type();}
  inline Discretizer *discretizer() const {return discretizer_;}

  virtual bool is_expression() const {return false;}
  virtual bool linear       () const {return true;}
  virtual int  degree_of_linearity() const {return 0;}

  ExpressionItem& operator=(const ExpressionItem &r);

  virtual void fixup_refs(const FixupInfo&, std::vector<UnresolvedRef>&) {}

  virtual CodeFragment generate_code(const DiscretizationData& dd) const
  {
    DEBUG_LOG(" stensil-of-%s ", name());
    return discretizer_->discretize(dd);
  }

  virtual CodeFragment generate_code_boundary(
              const BoundaryDiscretizationData& bdd) const
  {
    DEBUG_LOG(" stensil-of-%s ", name());
    return discretizer_->discretize_on_boundary(bdd);
  }

};

// 1. BasicOperator
// -----------------------------------------------------------------------------
class BasicOperator : public ExpressionItem
{
  //!	Field component id.
  int component_;

public:

  //! Constructor for basic operators.
  /*!	Does not duplicate \c discretizer. The \c discretizer must
      have been obtained by \c operator \c new. */
  BasicOperator(Discretizer *discretizer);
  //! Constructor for basic operators.
  /*!	Duplicates \c discretizer. */
  BasicOperator(const Discretizer& discretizer);

//  virtual ~BasicOperator() {} // not necessary!

  DECL_EXPRITEM_DUP();

  //! Std copy operator.
  BasicOperator& operator=(const BasicOperator &r);

  virtual int  degree_of_linearity() const {return 1;}
  virtual bool is_expression() const {return component_ > -1;} // TODO: check, -1 must be replaced by 0!

  //! Parse expressions X[u] where u is a field component specified by its id.
  Expression& operator[](int component) const;
  //! Parse expressions X[u] where u is a field component.
  Expression& operator[](const Field& u) const;
  //! Parse expressions X[f] where f is a scalar.
  Expression& operator[](const Function &r);

  virtual void fixup_refs(const FixupInfo&, std::vector<UnresolvedRef>&);

  virtual CodeFragment generate_code(const DiscretizationData& dd) const;
  virtual CodeFragment generate_code_boundary(
              const BoundaryDiscretizationData& bdd) const;

};

// 2. Function
// -----------------------------------------------------------------------------
class Function : public ExpressionItem
{
//  friend class BasicOperator;

public:

  struct Ref
  {
    Function& self;
    explicit Ref(Function& r) : self(r) {}
  };

  //!	Function implementation.
  std::string implementation;

  //! Constructor for scalar functions.
  explicit Function(const std::string &name) :
      ExpressionItem(new FunctionDiscretizer(name)) {}

//  virtual ~Function(); // not necessary

  DECL_EXPRITEM_DUP();

  //! Std copy operator.
  Function& operator=(const Function &r);

//	explicit Function(const Function &f);

//	operator Expression() const;

  //! Convert a Function object into a Function::Ref.
  /*! Function::Ref's are encapsulated Function&'s. They are used to transfer
      Function&'s into containers, notably initializer_list<Function::Ref>.
      We can do away with this method, provided that we make Ref's
      constructor Ref::Ref(Function&) non-explicit. */
  operator Ref() {return Ref(*this);}

  const std::string& fname() const {return this_discretizer()->fname;}
#ifdef __DEBUG__
  const std::string debug_name() const;
#endif // __DEBUG__

  //! Set implementation possibly after proper formatting.
  /*! Calls \c GlobalConfig::format_function_implementation() to perform
      the formatting. */
  void set_implementation(const std::string& spec);

  // ---------------------------------------------------------------------------
  // Function -- Parse binary expressions
  // ---------------------------------------------------------------------------
  Expression& operator+(double a) const;
  Expression& operator+(const Field& b) const;
  Expression& operator+(const Function& b) const;
  Expression& operator+(const Expression& b) const;
  Expression& operator-(double a) const;
  Expression& operator-(const Field& b) const;
  Expression& operator-(const Function& b) const;
  Expression& operator-(const Expression& b) const;
  Expression& operator*(double a) const;
  Expression& operator*(const Field& b) const;
  Expression& operator*(const Function& b) const;
  Expression& operator*(const Expression& b) const;
  Expression& operator/(double a) const;
  Expression& operator/(const Field& b) const;
  Expression& operator/(const Function& b) const;
  Expression& operator/(const Expression& b) const;

  // ---------------------------------------------------------------------------
  // Function -- Parse unary expressions
  // ---------------------------------------------------------------------------
  Expression& operator-() const;
  Expression& operator+() const;

protected:

  Expression& generic_operator(const char op) const;

  Expression& generic_operator(double a, const char op) const;
  Expression& generic_operator(const Field& b, const char op) const;
  Expression& generic_operator(const ExpressionItem& b, const char op) const;
  Expression& generic_operator(const Expression& b, const char op) const;

private:

  inline FunctionDiscretizer *this_discretizer() const
  { return (FunctionDiscretizer *) discretizer(); }

  //! Private constructor used by \c duplicate().
  Function(Discretizer *d) : ExpressionItem(d) {}
};

Expression& operator+(double a, const Function& f);
Expression& operator-(double a, const Function& f);
Expression& operator*(double a, const Function& f);
Expression& operator/(double a, const Function& f);

// 3. Constant
// -----------------------------------------------------------------------------
//! Constant class. Encapsulation of double.
/*! Constant is for internal use only. */
class Constant : public ExpressionItem
{
  //! Private constructor used by \c duplicate().
  explicit Constant(Discretizer *d) : ExpressionItem(d) {}

public:

  //! Constructor for constant fields.
  explicit Constant(double c) :
      ExpressionItem(new ConstantDiscretizer(c)) {}

  inline double value() const {return this_discretizer()->value();}

  DECL_EXPRITEM_DUP();

  // Inherits base class copy operator.

protected:

  inline ConstantDiscretizer *this_discretizer() const
  { return (ConstantDiscretizer *) discretizer(); }
};

// 4. Field function
// -----------------------------------------------------------------------------
class FieldFunction : public ExpressionItem
{

  static const std::string s_std_field_names[3];
  bool linear_;

protected:

  //! Component ids of supplied field arguments.
  /*! This list is used \em only when \c HAVE_STANDALONE_FIELDS is active.
      It is populated when the item F(u1, u2, ...) is encountered in
      an expression. Space variables are not specified in expressions. */
  std::vector<int> fcall_args_;

  //! Supplied expression arguments.
  /*! This list is populated when the item F(expr1, expr2, ...) is encountered
      in an expression. Space variables are not specified in expressions. */
  std::vector<Expression *> fcall_expr_args_;

  FieldFunction(Discretizer *d) : ExpressionItem(d) {}
  //! Protected constructor.
  /*! Used internally to copy an instance of a user created FieldFunction.
      Instances created by this method can be directly manipulated. */
  explicit FieldFunction(const FieldFunction &ff); // TODO: remove!

public:

  struct Ref
  {
    FieldFunction& self;
    explicit Ref(FieldFunction& r) : self(r) {}
  };

  //! Implementation of field function and its partial derivatives wrt field
  //! arguments.
  /*! If the \c size of \c implementation is less than the number of field
      arguments, only the headers of the missing partial derivatives are
      output. This also applies when an implementation's value is empty. When
      an implementation's value is "default," a default implementation is
      automatically generated, which performs a numerical differentiation.
      You can change the behavior of the supplied differentiation method by
      changing the value of the PDE member \c <funcname>_diff_accuracy, where
      \c <funcname> is the field function's name. A negative value indicates
      the default numerical differentiation method, which automatically
      selects the differentiation accuracy according to the value of the
      members \c <funcname>_diff_accuracy and \c <funcname>_zero_accuracy. */
  std::vector<std::string> implementation;

  //! Standard constructor for field functions.
  FieldFunction(const std::string &name, int argc, bool spacevars = false,
                                                   bool is_linear = false);

//	virtual ~FieldFunction(); // not necessary

  // Inherited copy operator should be OK.
  DECL_EXPRITEM_DUP();

  //! Convert a FieldFunction object into a FieldFunction::Ref.
  /*! FieldFunction::Ref's are encapsulated FieldFunction&'s. They are used
      to transfer FieldFunction&'s into containers, notably
      initializer_list<FieldFunction::Ref>.
      We can do away with this method, provided that we make Ref's
      constructor Ref::Ref(FieldFunction&) non-explicit. */
  operator Ref() {return Ref(*this);}

  //! Set implementation possibly after proper formatting.
  /*! Calls \c GlobalConfig::format_function_implementation() to perform
      the formatting. */
  void set_implementation(const std::vector<std::string>& imp);

  //! Compute degree of linearity.
  /*! Here is how dol (degree of linearity) is determined:
      Each simple item is assigned a dol as follows:
      0 consts & funcs, 1 linear items, 2 nonlinear.
      Addition and subtraction of simple items give the dol of the item
      with the greatest dol. Multiplication gives the sum of the dols.
      Division gives the sum of the first item's dol with the second
      multiplied by 2. */
  virtual int  degree_of_linearity() const;
  virtual bool linear() const {return degree_of_linearity() < 2;}

  bool has_space_args() const {return this_discretizer()->has_space_args();}
  int argc() const {return this_discretizer()->argc();}
  const std::string& fname() const {return this_discretizer()->fname;}
  std::string field_name(int n) const {return field_name(n, argc());}
  std::string get_func_name(int n) const;
  std::string get_accuracy_var(int n, int which = 1) const;

  virtual void fixup_refs(const FixupInfo&, std::vector<UnresolvedRef>&);

  // ---------------------------------------------------------------------------
  // FieldFunction -- Code generation
  // ---------------------------------------------------------------------------
  virtual CodeFragment generate_code(const DiscretizationData& dd) const;
  virtual CodeFragment generate_code_boundary(
              const BoundaryDiscretizationData& bdd) const;
  std::string code_implementation(int n, int dim) const;

#if HAVE_STANDALONE_FIELDS
  // ---------------------------------------------------------------------------
  // FieldFunction -- Create expressions
  // ---------------------------------------------------------------------------
  //! Usage: f(u, v) or f(x, y, u, v) when the function admits space arguments.
  Expression& operator()(std::initializer_list<const Field> l) const;
  Expression& operator()(const Field& u) const;
  Expression& operator()(const Field& u, const Field& v) const;
  Expression& operator()(const Field& u, const Field& v, const Field& w) const;
  Expression& operator()(const Field&, const Field&, const Field&,
                         const Field&) const;
  Expression& operator()(const Field&, const Field&, const Field&, const Field&,
                         const Field&) const;
  Expression& operator()(const Field&, const Field&, const Field&, const Field&,
                         const Field&, const Field&) const;
  Expression& operator()(const Field&, const Field&, const Field&, const Field&,
                         const Field&, const Field&, const Field&) const;
  Expression& operator()(const Field&, const Field&, const Field&, const Field&,
                         const Field&, const Field&, const Field&,
                         const Field&) const;
#endif // HAVE_STANDALONE_FIELDS

  //! Usage: f(Dx[u] + 2*v, Dxx[v] + Dy[u], Id[u])
  //! The expressions are implicitly converted to \c ExpressionRef's
  //! which are encapslated \c Expression pointers.
  Expression& operator()(std::initializer_list<const ExpressionRef> args) const;
  Expression& operator()(Expression&) const;
  Expression& operator()(Expression&, Expression&) const;
  Expression& operator()(Expression&, Expression&, Expression&) const;
  Expression& operator()(Expression&, Expression&, Expression&,
                         Expression&) const;
  Expression& operator()(Expression&, Expression&, Expression&, Expression&,
                         Expression&) const;
  Expression& operator()(Expression&, Expression&, Expression&, Expression&,
                         Expression&, Expression&) const;
  Expression& operator()(Expression&, Expression&, Expression&, Expression&,
                         Expression&, Expression&, Expression&) const;
  Expression& operator()(Expression&, Expression&, Expression&, Expression&,
                         Expression&, Expression&, Expression&,
                         Expression&) const;

protected:

  inline FieldFunctionDiscretizer *this_discretizer() const
  { return (FieldFunctionDiscretizer *) discretizer(); }

  std::string get_func_name(int n, const std::string& basename) const;
  std::string code_function_call(const std::string& f, int which,
      const std::string& varname, int argc, bool spacevars, int dim) const;
  static std::string field_name(int n, int argc);

  // ---------------------------------------------------------------------------
  // FieldFunction -- Generic expression creation
  // ---------------------------------------------------------------------------
  Expression& generic_expression(std::initializer_list<const Field>) const;
  Expression& generic_expression(std::initializer_list<Expression*>) const;
};

// 5. Cutoff function
// -----------------------------------------------------------------------------
class CutoffFunction : public ExpressionItem
{

protected:

  //! Component ids of supplied field arguments.
  /*! This is used \em only when \c HAVE_STANDALONE_FIELDS is active.
      It is populated when the item F(u) is encountered in an expression. */
  int fcall_arg_{-1};

  //! Supplied expression argument.
  /*! This list is populated when the item F(expr) is encountered
      in an expression. */
  Expression *fcall_expr_arg_{nullptr};

  CutoffFunction(Discretizer *d) : ExpressionItem(d) {}

public:

  struct Ref
  {
    CutoffFunction &self;
    explicit Ref(CutoffFunction &r) : self(r) {}
  };

  //! Implementation of the cutoff function.
  std::string implementation;

  //! Standard constructor for field functions.
  CutoffFunction(const std::string &name);

//	virtual ~CutoffFunction(); // not necessary

  // Default copy operator should be OK.
  DECL_EXPRITEM_DUP();

  //! Convert a CutoffFunction object into a CutoffFunction::Ref.
  /*! CutoffFunction::Ref's are encapsulated CutoffFunction&'s. They are used
      to transfer CutoffFunction&'s into containers, notably
      initializer_list<CutoffFunction::Ref>.
      We can do away with this method, provided that we make Ref's
      constructor Ref::Ref(CutoffFunction&) non-explicit. */
  operator Ref() {return Ref(*this);}

  //! Set implementation possibly after proper formatting.
  /*! Calls \c GlobalConfig::format_function_implementation() to perform
      the formatting. */
  void set_implementation(const std::string& impl);

  //! Compute degree of linearity.
  /*! \sa FieldFunction::degree_of_linearity() */
  virtual int  degree_of_linearity() const;
  virtual bool linear() const {return degree_of_linearity() < 2;}

  const std::string& fname() const {return this_discretizer()->fname;}

  virtual void fixup_refs(const FixupInfo&, std::vector<UnresolvedRef>&);

  // ---------------------------------------------------------------------------
  // CutoffFunction -- Code generation
  // ---------------------------------------------------------------------------
  virtual CodeFragment generate_code(const DiscretizationData& dd) const;
  virtual CodeFragment generate_code_boundary(
              const BoundaryDiscretizationData& bdd) const;
  std::string code_implementation(int dim) const;

  // ---------------------------------------------------------------------------
  // CutoffFunction -- Create expressions
  // ---------------------------------------------------------------------------
#if HAVE_STANDALONE_FIELDS
  //! Usage: f(u)
  Expression& operator()(const Field& u) const;
#endif // HAVE_STANDALONE_FIELDS

  //! Usage: f(Dx[u] + 2*v)
  //! The expression is implicitly converted to \c ExpressionRef
  Expression& operator()(Expression&) const;

protected:

  inline CutoffDiscretizer *this_discretizer() const
  { return (CutoffDiscretizer *) discretizer(); }
};

// 6. Operator
// -----------------------------------------------------------------------------
//!	User defined operator class.
/*! Example:
    Operator Laplace, Poison;
    Laplace(u, v) = Dxx[u] + Dyy[v]; // or
    Laplace({u, v}) = Dxx[u] + Dyy[v];
    Poisson(u, v) = Laplace[{u, v}] + u*Dy[v];
    pde.set_operator(Laplace);
    pde.set_operators({Laplace, Poisson}); // (?)
    pde(1) = Laplace[{u, v}] + u*Dy[v];
    pde(1) = Laplace[{u, v}] + u*Dy[v]; */
class Operator : public ExpressionItem
{
private:

  bool linear_;

protected:

  Expression *expression_;

  //! Component ids of supplied field arguments.
  /*!	This list is populated when in an expression the item L(u, v) is
      encountered. Non-argument fields and space variables are not
      specified in expressions. */
  std::vector<int> args_;

public:

  //! Optional operator name.
  std::string fname;

protected:

  struct Ref
  {
    Operator& self;
    Ref(Operator& o) : self(o) {}
    Operator& operator=(/*const */Expression&);
  };

  //! Protected constructor used by \c duplicate().
  Operator(Discretizer *d) : ExpressionItem(d),
    linear_(false), expression_(nullptr), fname("") {}

public:

  //! User (standard) constructor for operators.
  /*!	The user creates instances of \c Operators as local variables
      (i.e. on the stack) using this constructor. */
  Operator(const std::string& name = std::string("")) :
    ExpressionItem(new OperatorDiscretizer),
    linear_(false),
    expression_(nullptr),
    fname(name) {}

#if 0
  //! Standard constructor for operators.
  /*!	The user creates instances of \c Operators as local variables
    (i.e. on the stack) using this constructor. If more than 4
    parameters are needed, the constructor with initializer list
    is used. After its construction and definition an \c Operator
    is selected into a PDE prior to its use in an expression. */
  Operator() : Operator({}) {}
  Operator(const Field& u) : Operator({u}) {}
  Operator(const Field& u, const Field& v) : Operator{u, v} {}
  Operator(const Field& u, const Field& v, const Field& w) : Operator{u, v, w} {}
  Operator(const Field& u, const Field& v, const Field& w, const Field& z) :
      Operator{u, v, w, z} {}
#endif // 0

  // We need to extend the basic destructor in order to destroy expression_
  virtual ~Operator();

  virtual bool linear () const {return linear_;}
  virtual int  degree_of_linearity() const {return linear_ ? 1 : 2;}

  int argc() const {return (int) args_.size();}

  // No override of duplicate() method. Base class method throws exception.

  // Copy operator for diagnostic purposes.
  Operator& operator=(const Operator& oper);

  //! Assignment operator for diagnostic purposes.
  Operator& operator=(const Expression& expr);

  // ---------------------------------------------------------------------------
  // Operator -- Code generation
  // ---------------------------------------------------------------------------
  // Since \c Operator is never an expression item, these three methods
  // should never be called. They are here for diagnostic purposes. -->
  virtual void fixup_refs(const FixupInfo&, std::vector<UnresolvedRef>&);
  virtual CodeFragment generate_code(const DiscretizationData& dd) const;
  virtual CodeFragment generate_code_boundary(
							const BoundaryDiscretizationData& bdd) const;
  // <-- End of methods for diagnostic purposes.

  // ---------------------------------------------------------------------------
  // Operator -- Definition (expression assignment)
  // ---------------------------------------------------------------------------
  Ref operator()(std::initializer_list<const Field> l);
  Ref operator()(const Field& u)
  { return (*this)({u}); }
  Ref operator()(const Field& u, const Field& v)
  { return (*this)({u, v}); }
  Ref operator()(const Field& u, const Field& v, const Field& w)
  {	return (*this)({u, v, w}); }
  Ref operator()(const Field& u, const Field& v, const Field& w, const Field& z)
  { return (*this)({u, v, w, z}); }

  // ---------------------------------------------------------------------------
  // Operator -- Create expressions
  // ---------------------------------------------------------------------------
  Expression& operator[](std::initializer_list<const Field> args) const;
  Expression& operator[](const Field& u) const
  { return (*this)[{u}]; }

protected:

  inline OperatorDiscretizer *this_discretizer() const
  { return (OperatorDiscretizer *) discretizer(); }

  std::string get_func_name(int n, const std::string& basename) const;
  std::string code_implementation(int n) const;
  std::string code_function_call(const std::string& f, int which,
      const std::string& varname, int argc, bool spacevars) const;
  static std::string field_name(int n, int argc);

  // ---------------------------------------------------------------------------
  // Operator -- Expression assignment
  // ---------------------------------------------------------------------------
  Operator& set_differential_expression(Expression& expr);

}; // Operator

// =============================================================================
// Expression
// =============================================================================

// Tree nodes to parse expressions
// T1, T2 are other expressions or ExpressionItem's
// (including Constants and ScalarFunctions)

class Expression
{
  //! Expression node with left and right operands (subexpressions).
  Expression *left_;
  Expression *right_;
  //! Indicates that this expression is an \c ExpressionItem encapsulation.
  ExpressionItem *term_;
  char operation_;
  int  type_;
  bool linear_;

  typedef BasicOperator DE;
  typedef fd::CodeFragment CodeFragment;

public:

  Expression(const Expression *t1, const Expression *t2, const char op) :
    left_(const_cast<Expression *>(t1)),
    right_(const_cast<Expression *>(t2)),
    term_(nullptr),
    operation_(op)
  { type_ = get_type(); linear_ = get_linearity(); }

  Expression(const ExpressionItem *term) :
      left_(nullptr),
      right_(nullptr),
      term_(const_cast<ExpressionItem *>(term)),
      operation_(0),
      type_(term->type()),
      linear_(term->linear())
  {
    ASSERT(!(type_ == DE::BASIC_OPERATOR) || term_->is_expression());
    ASSERT(type_ != DE::DIFF_EXPR);
    if (type_ == DE::BASIC_OPERATOR) type_ = DE::DIFF_EXPR;
    if (type_ != DE::FUNCTION && type_ != DE::CONST_FIELD &&
        type_ != DE::FIELD_FUNCTION && type_ != DE::CUTOFF_FUNCTION &&
        type_ != DE::DIFF_EXPR)
      throw _S"Term " + term_->name() + " cannot be converted to expression.";
  }

  ~Expression();

  // Expression& operator=(const Expression& expr);
  Expression *duplicate() const
  { return new Expression(*this); }

  //! Used to pass arguments as initialization_list's.
  operator ExpressionRef() {return ExpressionRef(this);}

//	inline bool is_expression	() const {return true;}
  inline bool simple() const {return term_ != nullptr;}
  inline int simple_type() const {return term_ == nullptr ? -1 : term_->type();}

  CodeFragment apply_operation(const CodeFragment& left,
                               const CodeFragment& right) const
  {
    switch (operation_)
    {
      case '+': return PlusOp  (left, right).apply();
      case '-': return MinusOp (left, right).apply();
      case '*': return MultOp  (left, right).apply();
      case '/': return DivOp   (left, right).apply();
      case 0: 	throw _S"Null operation.";
    }
    throw "Unknown operation: " + std::string(1, operation_);
    return CodeFragment();
  }

  void fixup_refs(const FixupInfo&, std::vector<UnresolvedRef>&);

  CodeFragment generate_code(const DiscretizationData& dd) const;
  CodeFragment generate_code_boundary(const BoundaryDiscretizationData&) const;

  inline int  type() const {return type_;}
  inline bool linear() const {return linear_;}
  //! Compute degree of linearity.
  /*! Here is how dol (degree of linearity) is determined.
      Each simple item is assigned a dol as follows:
      0 consts & space funcs, 1 linear items, 2 nonlinear.
      Addition and subtraction of expressions give the dol of the item
      with the greatest dol. Multiplication gives the sum of the dols.
      Division gives the sum of the first item's dol with the second
      multiplied by 2. */
  int degree_of_linearity() const;

  fd::IntPair order() const
  {
    fd::IntPair l = left_->order();
    fd::IntPair r = right_->order();
    if (l[0] < r[0]) l[0] = r[0];
    if (l[1] < r[1]) l[1] = r[1];
    return l;
  }

  /*inline */
  int total_order() const
  {
    fd::IntPair a = order();
    return a[0] < a[1] ? a[1] : a[0];
  }

#ifdef __DEBUG__
  std::string name() const;
#endif

  // ---------------------------------------------------------------------------
  // Expression -- Parse binary expressions
  // ---------------------------------------------------------------------------
  Expression& operator+(double a) const;
  Expression& operator+(const Field& b) const;
  Expression& operator+(const Function& b) const;
  Expression& operator+(const Expression& b) const;
  Expression& operator-(double a) const;
  Expression& operator-(const Field& b) const;
  Expression& operator-(const Function& b) const;
  Expression& operator-(const Expression& b) const;
  Expression& operator*(double a) const;
  Expression& operator*(const Field& b) const;
  Expression& operator*(const Function& b) const;
  Expression& operator*(const Expression& b) const;
  Expression& operator/(double a) const;
  Expression& operator/(const Field& b) const;
  Expression& operator/(const Function& b) const;
  Expression& operator/(const Expression& b) const;

  // ---------------------------------------------------------------------------
  // Expression -- Parse unary expressions
  // ---------------------------------------------------------------------------
  Expression& operator-() const;
  Expression& operator+() const;

protected:

  Expression& generic_operator(double a, const char op) const;
  Expression& generic_operator(const Field& b, const char op) const;
  Expression& generic_operator(const ExpressionItem& b, const char op) const;
  Expression& generic_operator(const Expression& b, const char op) const;

  //! Copy constructor used internally by duplicate();
  Expression(const Expression& r);

private:

  int get_type() const;
  bool get_linearity() const;
};

// The accepting (final) node is implemented by
// PDE::set_differential_expression or PDE::operator=

Expression& operator+(double a, const Expression &expr);
Expression& operator-(double a, const Expression &expr);
Expression& operator*(double a, const Expression &expr);
Expression& operator/(double a, const Expression &expr);

extern BasicOperator Dx, Dy, Dz, Dxx, Dyy, Dzz, Dxy, Dxz, Dyz, Id;

} // namespace fd

#endif // EXPRESSIONS_H_INCLUDED
