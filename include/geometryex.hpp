#ifndef GEOMETRYEX_HPP_INCLUDED
#define GEOMETRYEX_HPP_INCLUDED

#include "geometry.hpp"

//============================================================================//
//::::::::::::::::::::::::  Geometry extension classes  :::::::::::::::::::::://
//============================================================================//

namespace fd
{

struct Line : Interval
{
  enum LineRestriction {LINE, RAY_POS, RAY_NEG, LINE_SEGMENT};

  //! Line axis
  Axis axis;
  //! Projection along its axis
  Point2d pos;
  //! Check if line is a single point.
  bool empty() const noexcept {return Interval::empty(axis);}

  Point origin() const;
  //! Check if line contains point.
  bool contains(const Point &p) const noexcept;
  //! Check if line, ray or line segment contains point.
  bool contains(const Point &p, LineRestriction t) const noexcept;
  //! Check if line intersects rectangle.
  /*! It is assumed that line and rectangle are in the same coordinate plane.
   *! Consequently, the axis normal to the rectangle must be different from
   *! the line's axis; i.e. if line's axis is X, the rectangle's axis can be
   *! Y or Z.
   *! \param r The rectangle to be checked.
   *! \param n The axis normal to the plane of the rectangle.
   */
  bool intersects(const Rectangle &r, Axis n) const;
  //! Check if specified part of line intersects rectangle.
  /*! \sa intersects(const Rectangle &r, Axis n)
   */
  bool intersects(const Rectangle &r, Axis n, LineRestriction t) const;
};

struct RectangleX : Rectangle
{
  //! Remove intersection and break resulting shape into rectangles.
  /*! \param n The rectangle is normal to axis \c n.*/
  std::vector<RectangleX> subtract(const RectangleX &r, Axis n) const;
  //! Subtract all rectangles of the supplied list from this rectangle.
  /*! \param n The rectangle is normal to axis \c n.*/
  std::vector<RectangleX> subtract(const std::vector<RectangleX>&,Axis n) const;
  //! Subtract this rectangle from each rectangle in the supplied list.
  /*! \param n The rectangle is normal to axis \c n.*/
  std::vector<RectangleX> rsub(const std::vector<RectangleX>&, Axis n) const;
  //! Get the lines of the rectangle's sides.
  /*! The rectangle lies in the coordinate plane normal to axis \c n.*/
  std::vector<Line> get_side_lines(Axis n) const;
  //! Select lines intersecting rectangle.
  /*! The rectangle lies in the coordinate plane normal to axis \c n.*/
  std::vector<Line> select_intersecting(const std::vector<Line>&, Axis n) const;
  //! Partition rectangle according to set of horizontal and vertical lines.
  std::vector<RectangleX> partition(const std::vector<Line> &lines, Axis) const;
  //! Partition rectangle according to set of points.
  std::vector<RectangleX> partition(const std::vector<Point2d>&, Axis n) const;
  //! Merge with rectangle if rectangles can be merged into a larger rectangle.
  bool merge_adjacent(const Rectangle &r, Axis n);
  //! Merge all pairs of rectangles, if they can be merged.
  static void merge(std::vector<RectangleX> &l, Axis n);
};

} // namespace fd

std::ostream& operator<<(std::ostream &os, const fd::Line &l);
std::istream& operator>>(std::istream &is, fd::Line &l);

#endif // GEOMETRYEX_HPP_INCLUDED
