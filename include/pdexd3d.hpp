#ifndef PDEXD3D_HPP_INCLUDED
#define PDEXD3D_HPP_INCLUDED

#include "pde3d.hpp"

namespace extdom {
  namespace d3 {

  using Mesh = fd3::Mesh;
  using xBoundary = fd3::xBoundary;
  using yBoundary = fd3::yBoundary;
  using zBoundary = fd3::zBoundary;

  //==========================================================================//
  //:::::::::::::::::::::::::::::::::  PDE  :::::::::::::::::::::::::::::::::://
  //==========================================================================//

  /** PDE on extended domain.
   *
   * Solution of a PDE on a domain with a virtual extended boundary so that
   * normal derivatives on the boundary can be computed by the usual 2-point
   * differentiation formula (SYM_DIFF2 discretization). Assuming that the
   * domain of the PDE is [a, b] x [c, d] x [e, f], the extended domain is:
   * [a - hx, b + hx] x [c - hy, d + hy] x [e - hz, f + hz]
   *
   * Instantiation of this class is required for the SYM_DIFF2 discretization
   * method.
   */
  class PDE : public fd3::PDE
  {

  public:

    /** Constructor */
    PDE(Mesh &mesh, const std::string& id = std::string("PDE")):
        fd3::PDE(mesh, id)
    {
      THROW_IF(mesh.has_global_boundary(), "Extended domain meshes cannot use "
                                           "global boundaries.");
      boundary_discretization_method_ = SYM_DIFF2;
    }
    /** DEPRECATED */
    PDE(Mesh *mesh, const std::string& id = std::string("PDE")):
        PDE(*mesh, id) {}

  protected:

    /** Edge information associated with a \c Field.
     *
     * \c virtual_extremity[2] contains the virtual extremities of the faces
     * in node coordinates as reported by \c Field structure.
     */
    struct EdgeInfo
    {
      /** Array containing the SID of each face */
      const int *fids;
      /** Axis parallel to edge */
      int edge_axis;
      /** Boundary type or normal axis of each face */
      int face_type[2];
      /** Domain extremity of each face, 0 or 1 */
      int face_extremity[2];
      /** Extremity (node coordinate), extended or not, of each face */
      int node_extremity[2];
      /** Presence of extended boundary */
      bool extended_boundary[2];
    };

    /** Vertex information associated with a \c Field.
     *
     * \c virtual_extremity[2] contains the virtual extremities of the faces
     * in node coordinates as reported by \c Field structure.
     */
    struct VertexInfo
    {
      /** Array containing the SID of each face */
      const int *fids;
      /** Boundary type or normal axis of each face */
      int  face_type[3];
      /** Domain extremity of each face, 0 or 1 */
      int  face_extremity[3];
      /** Extremity (node coordinate), extended or not, of each face */
      int  node_extremity[3];
      /** Presence of extended boundary */
      bool extended_boundary[3];
    };

    /**
     * Generate the final discretized system of equations to be solved.
     *
     * \sa fd3::PDE::generate_code_function_body.
     */
    // virtual void generate_code_function_body(int what) override;
    /**
     * Generate code for the body of the functions.
     *
     * Nonlinear PETSc specific code generator.
     */
    virtual void generate_code_function_body_petsc_nonlinear(int what) override;
    /**
     * Generate code for faces
     *
     * This method is called from\sa generate_code_function_body.
     */
    virtual void generate_code_face(int what, int bid, int ntabs) override;
    /**
     * Generate code for edges
     *
     * This method is called from\sa generate_code_function_body.
     */
    virtual
    void generate_code_edge(int what, int eid, int bid, int fid2, int ntabs)
                                                                    override;
    /**
     * Generate code for vertices
     *
     * This method is called from\sa generate_code_function_body.
     */
    virtual
    void generate_code_vertex(int what, int vid, int bid, int ntabs) override;
    /**
     * Generate code for virtual points.
     *
     * Called internally by \sa generate_code_function_body.
     * \param what \sa generate_code_function_body
     */
    virtual void generate_code_virtual_points(int what) override;
    /** Get edge information associated with a \c Field. */
    EdgeInfo get_edge_info(int eid, int c) const;
    /** Get vertex information associated with a \c Field. */
    VertexInfo get_vertex_info(int vid, int c) const;
    /** Check if edge's face has extended boundary
     *
     * \param ei \c EdgeInfo of a field.
     * \param side The side of the edge.
     *
     *  \sa has_extended_boundary(int, int, int)
     */
    bool has_extended_boundary(const PDE::EdgeInfo& ei, int side) const;
    /** Check if edge's face has extended boundary
     *
     * \param eid The ID of the edge.
     * \param c The index of the field.
     * \param side The side of the edge.
     *
     * This method reports if a side has an extended boundary according to
     * the data of the associated \c Field structure. It uses internally
     * \sa get_edge_info.
     */
    bool has_extended_boundary(int eid, int c, int side) const;
    void generate_code_dummy_BC(int what, int ntabs) noexcept;
    /** Generate code for extended edge.
     *
     * An extended edge results from the hypothetical removal of an extended
     * domain's face. For example, the nodes
     * (nx,0,k), where 0 <= k <= nz-1,
     * are the BR entended edge.
     * Virtual edges are the extended domain's edges without their extended
     * vertices. For example, the nodes
     * (nx,-1,k), where 0 <= k <= nz-1,
     * are the BR virtual edge.
     *
     * \param face_selector 0: ordinary (primary); 1: secondary.
     * \param idx used to access the array fields of \c edge_info_field in the
     *            correct order.
     */
    void generate_code_for_extended_edge(const int eid,
                                        const int face_selector,
                                        const int bid,
                                        const int bid2,
                                        EdgeInfo  edge_info_field[],
                                        const int (&idx)[2],
                                        const int what,
                                        const int ntabs);
    /**
     * Finalize BCs before code generation.
     *
     * Special version for the \c extdom solution method.
     * Simply calls \sa PDECommon::finalize_BCs_extdom().
     * It is used by \sa generate_code().
     */
    virtual void finalize_BCs() override {finalize_BCs_extdom();}
  };

} // namespace d3
} // namespace extdom

#endif // PDEXD3D_HPP_INCLUDED
