#ifndef BC3D_HPP_INCLUDED
#define BC3D_HPP_INCLUDED

#include <cmath>
#include "geometry.hpp"
#include "bc.hpp"

// =============================================================================
// Boundary -- 3d classes
// =============================================================================

namespace fd3
{

/**
 * \brief Base class for flat 3d boundaries.
 *
 * Boundary3d does not set the boundary's type.
 * This is done in derived classes, \c xBoundary etc.
 */
class Boundary3d : public fd::Boundary
{
protected:
  /**
   * \brief Rectangle in the yz, xz or xy plane.
   *
   * xBoundary: yz-plane, 1st coordinate y, 2nd z
   * yBoundary: xz-plane, 1st coordinate x, 2nd z
   * zBoundary: xy-plane, 1st coordinate x, 2nd y
   */
  fd::Rectangle rect_;

  Boundary3d() : fd::Boundary(1), rect_{0,1,0,1} {}
  Boundary3d(double x, const fd::Rectangle &r) : fd::Boundary(x), rect_(r) {}

public:
  inline const fd::Rectangle& rectangle() const noexcept {return rect_;}
  virtual int dimension() const noexcept override {return 3;}
  virtual void print() const override;

protected:
  /** Check if \c rect_ is trivial. */
  void check() const;
};

/** A flat boundary whose normal vector is in the x direction. */
class xBoundary : public Boundary3d
{
public:
  xBoundary() {type = fd::Boundary::X;}
  xBoundary(double x, const fd::Rectangle &rect) :
      Boundary3d(x, rect) {type = fd::Boundary::X; check();}
  inline double y0 () const noexcept {return rect_.a;}
  inline double y1 () const noexcept {return rect_.b;}
  inline double z0 () const noexcept {return rect_.c;}
  inline double z1 () const noexcept {return rect_.d;}
};

/** A flat boundary whose normal vector is in the y direction. */
class yBoundary : public Boundary3d
{
public:
  yBoundary() {type = fd::Boundary::Y;}
  yBoundary(double y, const fd::Rectangle &rect) :
      Boundary3d(y, rect) {type = fd::Boundary::Y; check();}
  inline double x0 () const noexcept {return rect_.a;}
  inline double x1 () const noexcept {return rect_.b;}
  inline double z0 () const noexcept {return rect_.c;}
  inline double z1 () const noexcept {return rect_.d;}
};

/** A flat boundary whose normal vector is in the z direction. */
class zBoundary : public Boundary3d
{
public:
  zBoundary() {type = fd::Boundary::Z;}
  zBoundary(double z, const fd::Rectangle &rect) :
      Boundary3d(z, rect) {type = fd::Boundary::Z; check();}
  inline double x0 () const noexcept {return rect_.a;}
  inline double x1 () const noexcept {return rect_.b;}
  inline double y0 () const noexcept {return rect_.c;}
  inline double y1 () const noexcept {return rect_.d;}
};

} // namespace fd3

#endif // BC3D_HPP_INCLUDED
