#ifndef MESH_HPP_INCLUDED
#define MESH_HPP_INCLUDED

#include <memory>
#include "bc.hpp"

// =============================================================================
// MeshCommon
// =============================================================================

namespace fd
{

/**
 * \brief Common part of all Mesh classes.
 */
class MeshCommon
{

public:

#if 0
1d:
     [2] +-------------+ [0]
     (0)                 (1)

2d:
       3       [1]       2
         +-------------+
         |             |
     [2] |             | [0]
         |             |
         +-------------+
       0       [3]       1

3d:
        y^  7    z [6]
         |  +---/---------+ 6
         | /|  /          |
         |/ | /           |[5]
       3 +--|/   [4]   +2 |
         |  +-------------+ 5
      [2]| /4       [0]| /
         |/            |/
         +-------------+---------> x
       0       [3]       1

  1562 {0} --> R
  2673 {1} --> T
  0473 {2} --> L
  0154 {3} --> B
  4567 {4} --> U
  0123 {5} --> D

  04  { 0} --> BL // 2d and 3d
  15  { 1} --> BR // 2d and 3d
  26  { 2} --> TR // 2d and 3d
  37  { 3} --> TL // 2d and 3d
  45  { 4} --> UB
  56  { 5} --> UR
  67  { 6} --> UT
  74  { 7} --> UL
  01  { 8} --> DB
  12  { 9} --> DR
  23  {10} --> DT
  30  {11} --> DL
#endif

  /** \brief Side (face in 3d) identifiers. */
  enum {
    R = 0, ///< Right
    T = 1, ///< Top
    L = 2, ///< Left
    B = 3, ///< Bottom
    U = 4, ///< Up
    D = 5  ///< Down
  };

  /** \brief Vertex (edge in 3d) identifiers. */
  enum {
    BL =  0, ///< Bottom Left
    BR =  1, ///< Bottom Right
    TR =  2, ///< Top Right
    TL =  3, ///< Top Left
    UB =  4,
    UR =  5,
    UT =  6,
    UL =  7,
    DB =  8,
    DR =  9,
    DT = 10,
    DL = 11
  };

  /** \brief Vertex (3d only) identifiers. */
  enum {
    DBL =  0,
    DBR =  1,
    DTR =  2,
    DTL =  3,
    UBL =  4,
    UBR =  5,
    UTR =  6,
    UTL =  7
  };

protected:

  /**
   *  Vector of Boundaries (pointers NOT owned by MeshCommon).
   *
   *  The Boundary pointers contained in \c boundaries_ are NOT owned by
   *  \c MeshCommon. They are usually created locally in the \c main()
   *  function and can be reused.
   */
  std::vector<Boundary *> boundaries_;
  /**
   *  Global boundary instance.
   *
   *  This is the unique global boundary instance which may or may not be
   *  in the list of the mesh's \c boundaries_ depending on the mesh
   *  constructor.
   *  To check if a mesh has a global boundary use \sa has_global_boundary().
   */
  GlobalBoundary global_boundary_;
  /**
   *  Indicates that mesh is in finalized or not finalized state.
   */
  bool finalized_;

  /** MeshCommon constructor. */
  MeshCommon() : finalized_(false) {}

public:

  /** Check if \c MeshCommon is finalized. */
  inline bool finalized() const {return finalized_;}

  /**
   *  Get pointer to boundary.
   *
   *  \param bid The boundary's id. It is the boundary's index (offset)
   *  in \c boundaries_.
   *  \sa boundary.
   *  \sa get_bid.
   */
  Boundary *get_boundary(int bid) const;
  /**
   *  Get pointer to boundary without bounds checking.
   *
   *  \param bid The boundary' id.
   *  \sa get_boundary.
   *  \sa get_bid.
   */
  Boundary *boundary(int bid) const;
  /**
   *  Get boundary id ( \c bid).
   * 
   *  A boundary's id ( \c bid) is its index in the list of boundaries.
   *  \sa boundaries_.
   */
  int get_bid(Boundary *) const;
  /**
   * Number of boundaries.
   * 
   * This method returns the number of boundaries excluding a possible
   * global boundary. Use the \sa has_global_boundary method to get the
   * total number of boundaries.
   */
  int num_boundaries() const;
  /**
   *  Check if mesh has global boundary
   */
  bool has_global_boundary() const;
  /**
   *  Get reference of global boundary.
   */
  const Boundary& get_global_boundary() const;
  /**
   *  Get id of global boundary or -1 if not present.
   */
  int get_global_bid() const;

  /** Get BC for component \c cid on boundary with id \c bid. */
  BoundaryCondition *get_BC_for_component(const std::vector<BoundaryCondition*>&,
                                          int component, int bid) const;

  /** Print mesh boundaries for debugging and diagnostic purposes. */
  void print_boundaries() const;

protected:

  /**
   *  Get boundary id (index).
   *
   *  A boundary id (\c bid) is its index (offset) in the sorted and oriented
   *  (in 2- and 3-d) list of boundaries, \c boundaries_.
   *  \sa boundaries_.
   */
  inline int index_of(Boundary *p) const
  { return fd::index(boundaries_, p); }
};

} // namespace fd

#endif // MESH_HPP_INCLUDED
