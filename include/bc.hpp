#ifndef BC_HPP_INCLUDED
#define BC_HPP_INCLUDED

#include <cmath>
#include "basedefs.h"
#include "codefrag.h"
#include <memory>

//============================================================================//
//:::::::::::::  Global Boundary and BoundaryCondition classes  :::::::::::::://
//============================================================================//

namespace fd
{

/**
 * Base class of all Boundary classes.
 *
 * Used internally for the construction of 1-dimensional boundaries.
 */
class Boundary
{
private:
  double pos_; ///< Position of boundary.

public:
  enum {X = 0, Y = 1, Z = 2, G = 3}; ///< Boundary types.

  struct Ref
  {
    Boundary& self;
    Ref(Boundary& b) : self(b) {}
  };

  int type;	///< The type of Boundary: \c X \c Y \c Z or \c G

  /** Constructor: creates a 1-dimensional boundary. */
  Boundary(double x = 1) : pos_(x), type(-1) {}
  /** Destructor. */
  virtual ~Boundary() {}
  /** Position of boundary. */
  inline double pos	() const noexcept {return pos_; }
  /**
   * Dimension of space.
   *
   * Override when extending a lower space dimension class.
   */
  virtual int dimension() const noexcept {return 1;}
  /**
   * \brief Get edge extremity.
   *
   * The edge is defined by 2 sids in both 3d and 2d. In 2d the primary sid
   * plays no part in the calculation, hence \c sid0 is the secondary sid
   * and \c sid1 is -1; this is identical to \c vertex_extremity(sid0,-1).
   * In 3d \c sid0 is the primary and \c sid1 is the secondary sid.
   * 
   * \param sid0 primary sid (3d) or secondary sid (2d)
   * \param sid1 secondary sid (3d) or -1 (2d).
   */
  static int edge_extremity(int sid0, int sid1);
  /**
   * \brief Get vertex extremity.
   *
   * The vertex is defined by 3 sids in 3d, and 2 in 2d. The
   * primary sid plays no part in the calculation.
   */
  static int vertex_extremity(int secondary, int tertiary);
  /** Print boundary for debugging and diagnostic purposes. */
  virtual void print() const;
};

/**
 * Global boundary
 * 
 * The global boundary is all of the domain's boundary in 2- and 3-d.
 * Meshes in 1d have no global boundary.
 * A mesh contains at most one global boundary. The global boundary
 * is defined automatically by the mesh constructor, when appropriate.
 * It is never defined explicitly.
 */
class GlobalBoundary : public Boundary
{
public:
  GlobalBoundary() : Boundary(0)
  { type = fd::Boundary::G; }
};

/**
 * Base class of all Boundary Condition classes.
 *
 * For the construction of BCs use one of the specialized BC classes.
 */
class BoundaryCondition
{
public:
  /** \brief BC types */
  enum {UNKNOWN, DIRICHLET, NEUMANN, CUSTOM, USER_SPEC = CUSTOM};

  struct Ref
  {
    const BoundaryCondition& self;
    Ref(const BoundaryCondition& bc) : self(bc) {}
  };

  /**
   * The type of the BC
   * 
   * There are 4 BC types:
   * \c DIRICHLET \c NEUMANN \c CUSTOM = \c USER_SPEC
   */
  int type;

protected:
  /** The boundary associated which this BC */
  Boundary *boundary_;
  /**
   * Field component associated with this BC.
   *
   * This property is managed by the PDE object. Do not set it directly!
   */
  int component_;

  /** Constructor */
  BoundaryCondition(Boundary *b) :
    type        (UNKNOWN),
    boundary_   (b),
    component_  (-1) {}

  /** Constructor */
  inline BoundaryCondition(const Boundary& b) :
      BoundaryCondition(const_cast<Boundary*>(&b)) {}

public:
  /** Destructor */
  virtual ~BoundaryCondition() {}

  /** Get field component associated with this BC */
  inline int component() const noexcept {return component_;}
  /** Associate BC with field component. */
  inline void set_component(int index) noexcept {component_ = index;}
  /** Get Boundary associated with this BC */
  inline Boundary *boundary() const noexcept {return boundary_;}
  /** Dimension of space */
  inline int dimension() const noexcept {return boundary_->dimension();}
};

/**
 * Dirichlet boundary condition (BC)
 */
class DirichletBC : public BoundaryCondition
{
public:
  /** Dirichlet BC function name. */
  std::string fname;

  DirichletBC(Boundary *b, const std::string& f = _S"") :
    BoundaryCondition(b), fname(f)
  { type = DIRICHLET; }

  inline
  DirichletBC(const Boundary& b, const std::string& f = _S"") :
    DirichletBC(const_cast<Boundary*>(&b), f) {}

  DirichletBC(const DirichletBC& bc) :
    BoundaryCondition(bc.boundary()), fname(bc.fname)
  { type = DIRICHLET; }
};

/**
 * Neumann boundary condition (BC).
 *
 * Neumann BCs have the format
 * \f[ a\frac{\partial u}{\partial n}+bu=c\f]
 * or in 1d
 * \f[ a\frac{du}{dx}+bu=c\f]
 * where \c a, \c b, \c c are given functions and \b n is x, y or z.
 * The implementation of this class is in \c mesh2d.cpp.
 */
class NeumannBC : public BoundaryCondition
{
  /** Counter for constants that are used in Neumann BCs. */
  static int count_;

public:
  enum {ITEM_NOT_PRESENT, FUNC, CONST_VALUE, ZERO, NO_COEF, ONE = NO_COEF};
  /**
   * \brief Discretize this equation, if necessary.
   * 
   * Discretize this equation of the PDE system, if an additional equation
   * is required for this BC. For PDE systems only.
   */
  int discretize_equation;
  /** Coefficients of the BC.
   *
   * coef[0] = c, coef[1] = b, coef[2] = a. */
  std::string coef[3];
  /** Coefficient type.
   *
   * If type is \c FUNC, the corresponding coefficient is a function name.
   * If type is \c CONST_VALUE, the corresponding coefficient is a constant
   * value multiplier. In this case coef[] is in the format:
   * c_NeumannBC_<count_>=<value>
   */
  int coef_type[3];

  NeumannBC(Boundary *bd,
          const std::string a = std::string(""),
          const std::string b = std::string(""),
          const std::string c = std::string(""), int eq_to_discretize = -1) :
    BoundaryCondition		(bd),
    discretize_equation	(eq_to_discretize)
  {
    type = NEUMANN;
    coef[0] = coef[1] = coef[2] = "";
    build_coef(a, 2);
    build_coef(b, 1);
    build_coef(c, 0);
  }

  inline
  NeumannBC(const Boundary& bd,
            const std::string a = std::string(""),
            const std::string b = std::string(""),
            const std::string c = std::string(""), int eq_to_discretize = -1) :
    NeumannBC(const_cast<Boundary*>(&bd), a, b, c, eq_to_discretize) {}

  NeumannBC(const NeumannBC& bc) :
    BoundaryCondition   (bc.boundary()),
    discretize_equation	(bc.discretize_equation)
  {
    type = NEUMANN;
    coef[0] = bc.coef[0];
    coef[1] = bc.coef[1];
    coef[2] = bc.coef[2];
    coef_type[0] = bc.coef_type[0];
    coef_type[1] = bc.coef_type[1];
    coef_type[2] = bc.coef_type[2];
  }

  bool is_const(int w) const {return coef_type[w] == CONST_VALUE;}
  bool is_func (int w) const {return coef_type[w] == FUNC;}
  bool is_zero (int w) const {return coef_type[w] == ZERO;}
  bool no_coef (int w) const {return coef_type[w] == NO_COEF;}
  bool item_not_present(int w) const {return coef_type[w] == ITEM_NOT_PRESENT;}

  std::string get_varname(int which) const
  { const std::string& s = coef[which]; return s.substr(0, s.find('=')); }

private:
  void build_coef(const std::string& specval, int which);
};

class Expression;
class BoundaryDiscretizationData;

class CustomBC : public BoundaryCondition
{
public:

  // TODO useless, remove it!
  int discretize_equation;
  /** Expression associated with this BC. */
  std::shared_ptr<const Expression> expression;

  /** Constructor */
  CustomBC(Boundary *b) :
      BoundaryCondition   (b),
      expression          (nullptr) {type = CUSTOM;}

  /** Constructor */
  inline
  CustomBC(const Boundary& b) : CustomBC(const_cast<Boundary *>(&b)) {}

  /** Constructor */
  CustomBC(const CustomBC& bc) :
      BoundaryCondition   (bc.boundary()),
      discretize_equation (bc.discretize_equation),
      expression          (bc.expression) {type = CUSTOM;}

  /**
   * Generate code fragment.
   *
   * Generate this BC's code fragment according to the supplied
   * boundary discretization data.
   */ 
  CodeFragment generate_code_fragment(const BoundaryDiscretizationData &bdd) const;
  /**
   * Set the expression associated with this BC.
   * 
   * BCs have the form expression = 0. This method sets the BC's lhs.
   * The expression is subsequently used for code generation.
   * The code is not generated until the method
   * \sa PDECommon::get_code_for_boundary() is invoked.
   */
  CustomBC& operator=(const Expression& expr);
};

} // namespace fd

#endif // BC_HPP_INCLUDED
