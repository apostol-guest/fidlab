#ifndef CODEFRAG_H_INCLUDED
#define CODEFRAG_H_INCLUDED

#include "basedefs.h"

namespace fd {

// =============================================================================
// CodeFragment
// =============================================================================

struct CodeFragment
{
  enum Format {NO_BRACKETS, BRACKETS_IF_FIRST, BRACKETS_IF_LAST,
               BRACKETS_IF_NOT_FIRST, BRACKETS_IF_NOT_LAST, USE_BRACKETS};

  //! \brief Boundary identity.
  /*! When it is -1, the code refers to internal grid points. */
  int bid;
  //! \brief Simple expression code.
  /*! Indicates that the code was generated for a simple (terminal) expression,
   	  otherwise it was obtained by combining other simple or combined codes.
   	  It is used by the code generator to eliminate redundant parentheses. */
  bool terminal;
  //! \brief Linear item.
  /*! Indicates that this expression is linear. */
  bool linear;
  //! \brief Generated code.
  std::string text_y, text_j;
  //! \brief Hints on formatting generated code.
  Format hint_y, hint_j;
  /** \brief Code for 2d boundary extremities and 3d vertices.
   * 
   *  These members are used only by CUSTOM BCs. When they are present (not
   *  empty) they override the \c text_y, \c text_j entries at boundary
   *  extremities (2d: vertices; 3d: edges of the solution domain), "bx",
   *  according to the encoding of \sa PDECommon::boundary_vertex_extremity()
   *  for 2d and \sa PDECommon::boundary_edge_extremity() for 3d. */
  std::string text_bx_y[4], text_bx_j[4];
  /** \brief Hints on formatting generated code. */
  Format hint_bx_y[4], hint_bx_j[4];
  /** \brief Code for 3d boundary extremities (edges).
   * 
   *  These members are used only by CUSTOM BCs. When they are present (not
   *  empty) they override the \c text_y, \c text_j entries at vertex
   *  extremities (3d vertices of the solution domain), "vx", according to
   *  the encoding of \sa PDECommon::boundary_vertex_extremity(). */
  std::string text_vx_y[4], text_vx_j[4];
  /** \brief Hints on formatting generated code. */
  Format hint_vx_y[4], hint_vx_j[4];

  CodeFragment(const std::string& code_y = std::string(""),
               const std::string& code_j = std::string(""),
               Format h_y = NO_BRACKETS, Format h_j = NO_BRACKETS);

  static int sizeof_bx() noexcept;
  static int sizeof_vx() noexcept;

  inline int  refers_to_boundary() const noexcept {return bid;}
  inline void refers_to_boundary(int rbid) noexcept {bid = rbid;}
  inline bool boundary_code() const noexcept {return bid != -1;}
  bool no_bx_code() const;
  bool no_vx_code() const;
  bool bx_code(int i) const {return !text_bx_y[i].empty();}
  bool vx_code(int i) const {return !text_vx_y[i].empty();}
};

} // namespace fd

#endif // CODEFRAG_H_INCLUDED
