#ifndef MESH3D_HPP_INCLUDED
#define MESH3D_HPP_INCLUDED

#include <memory>
#include "mesh.hpp"
#include "bc3d.hpp"
#include <tuple>

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::::::::::  Mesh  ::::::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

namespace fd3
{

/**
 * \brief Three-dimensional flat mesh representation.
 * 
 * Inherited methods:
 * \li \sa int get_bid();
 */
class Mesh : public fd::MeshCommon
{
  using Boundary = fd::Boundary;
  using BoundaryCondition = fd::BoundaryCondition;

#if 0

3d:
        y^  7    z [6]
         |  +---/---------+ 6
         | /|  /          |
         |/ | /           |[5]
       3 +--|/   [4]   +2 |
         |  +-------------+ 5
      [2]| /4       [0]| /
         |/            |/
         +-------------+---------> x
       0       [3]       1

  1562 {0} --> R
  2673 {1} --> T
  0473 {2} --> L
  0154 {3} --> B
  4567 {4} --> U
  0123 {5} --> D

  0    {0} --> DBL
  1    {1} --> DBR
  2    {2} --> DTR
  3    {3} --> DTL
  4    {4} --> UBL
  5    {5} --> UBR
  6    {6} --> UTR
  7    {7} --> UTL
#endif

public:

  /** Number of knots in each direction */
  int nx, ny, nz;
  /** Solution domain */
  double a, b, c, d, e, f;
  /** Mesh spacing */
  double hx, hy, hz;
  /**
   * \brief Force disjoint boundaries
   * 
   * Check if the specified boundaries on each face are disjoint, and
   * issue an error if they are not. The default behavior is to allow
   * overlapping boundaries. In this case, boundaries defined earlier
   * take precedence when determining boundary conditions.
   */
  bool disjoint_boundaries{false};

  /**
   * \brief Mesh constructor
   *
   * The boundaries are subsequently added with the \sa set_boundaries()
   * method.
   */
  Mesh(int Nx, int Ny, int Nz) : nx(Nx), ny(Ny), nz(Nz) {}
  /**
   * \brief Mesh constructor with global boundary.
   * 
   * The initializer list can:
   * \li be empty. In this case the domain is [0,1]x[0,1]x[0,1].
   * \li have 3 elements b, d, f. In this case the domain is [0,b]x[0,d]x[0,f].
   * \li have 6 elements a, b, c, d, e, f. In this case the domain is
   *     [a,b]x[c,d]x[e,f].
   * 
   * The user can subsequently add further boundaries, provided that the flag
   * \c disjoint_boundaries is false, with the methods \sa set_boundary() or
   * \sa set_boundaries() without specifying any \c GlobalBoundary arguments.
   * In this process the supplied boundaries are arranged so that the unique
   * \c GlobalBoundary remains always last in the series.
   */
  Mesh(std::initializer_list<double> l, int Nx, int Ny, int Nz);
  /**
   * \brief Mesh constructor with global boundary.
   * 
   * Similar to \sa Mesh(std::initializer_list<double>, int, int, int) but
   * with the same number of nodes in each direction.
   */
  Mesh(std::initializer_list<double> l, int N) : Mesh(l, N, N, N) {}
  /** Access domain coordinates. */
  double& operator[](int i) noexcept;
  const double& operator[](int i) const noexcept;
  /** Access domain coordinates. */
  inline double& coord(int i) noexcept {return (*this)[i];}
  inline const double& coord(int i) const noexcept {return (*this)[i];}
  /** Access mesh spacings. */
  double spacing(fd::Axis i) const noexcept;
  /** Get rectangle of domain face normal to \c Axis \c n. */
  fd::Rectangle face_rectangle(fd::Axis n) const noexcept;
  /** Add boundary to mesh. */
  Mesh &set_boundary(Boundary &b);
  /** Add boundary to mesh. */
  inline Mesh &add_boundary(Boundary &b) {return set_boundary(b);}
  /** Add boundaries to mesh. */
  void set_boundaries(std::initializer_list<Boundary::Ref> l);

  /**
   * Get boundary ids (\c bid) by side.
   *
   * The returned vectors have the same order and orientation with
   * the original vector of boundaries.
   * \sa get_bid.
   */
  std::vector<std::vector<int>> get_bids_by_side() const;
  /**
   * Get boundary ids ( \c bid) on side \c sid.
   *
   * The returned vectors have the same order and orientation with
   * the original vector of boundaries.
   * \param sid Side identifier.
   * \sa get_bid.
   */
  std::vector<int> get_bids_on_side(int sid) const;

  /**
   * Finalize \c Mesh.
   * 
   * The Mesh is automatically finalized after calling \sa set_boundaries.
   * Usually, you should not have to call this method directly.
   * \sa set_boundaries. */
  void finalize();
  /**
   * Get the side id (\c sid) of the boundary.
   * 
   * A side id (\c sid) is one of Mesh::R, B etc. */
  int get_sid(Boundary *) const;
  /**
   * Get the side id (\c sid) of the boundary.
   * 
   * \sa get_bid(Boundary *). */
  int get_sid(int bid) const;
  /**
   * Check if boundary with id \c bid is in side \c sid.
   * 
   * This method offers a performance improvement over
   * \code{.cpp}
   * get_sid(bid) == sid
   * \endcode
   * \sa get_sid(int). */
  bool in_side(int bid, int sid) const;
  /** Get extremity of boundary (0 or 1). */
  int extremity(int bid) const;
  /** Get extremity of face (0 or 1). */
  static int face_extremity(int fid) noexcept;
  /**
   * Get boundary type.
   * 
   * \param bid Boundary id. */
  inline int get_btype(int bid) const {return boundaries_[bid]->type;}
  /**
   * Get boundary type of face id.
   * 
   * \param fid sid of domain face. */
  static int face_btype(int fid) noexcept;

  //--------------------------------------------------------------------------//
  //:::::::::::::::::::::::::::  PDE Utilities  :::::::::::::::::::::::::::::://
  //--------------------------------------------------------------------------//
  /** Get boundary's rectangle. */
  const fd::Rectangle& get_rectangle(const Boundary *p) const;
  /** Get boundary's rectangle. */
  const fd::Rectangle& get_rectangle(int bid) const;
  /** \brief Get domain edges of boundary. */
  std::vector<int> get_domain_edges(int bid) const;
  /** \brief Get domain vertices of boundary. */
  std::vector<int> get_domain_vertices(int bid) const;
  /**
   * \brief Get face side.
   *
   * \param \c fid The face identifier.
   * \param \c fid2 The face identifier of the side.
   *
   * TODO: remove after checking that it gives identical results with
   * edge_interval. 
   */
  std::tuple<fd::Interval,fd::Axis> get_face_side(int fid, int fid2) const;
  /** Get edge's interval. */
  std::tuple<fd::Interval,fd::Axis> edge_interval(int eid) const;
  /**
   * \brief Get boundary side.
   *
   * \param \c bid  The boundary identifier.
   * \param \c fid2 The face identifier of the side. It must have been
   *                obtained a call to \sa get_meeting_face(bid, eid), and
   *                \c eid in turn must have been obtained from the \c vector
   *                returned by \sa get_domain_edges(bid).
   * 
   * Example:
   * \code{.cpp}
   * const std::vector<int> edges = mesh.get_domain_edges(bid);
   * fd::Interval i{-1,0}; // empty
   * if (!edges.empty()) {
   *   const int eid = edges.front();
   *   const int fid2 = mesh.get_meeting_face(bid, eid);
   *   std::tie(i,std::ignore) = mesh.get_boundary_side(bid, fid2);
   * }
   * \endcode
   */
  std::tuple<fd::Interval,fd::Axis> get_boundary_side(int bid, int fid2) const;
  /**
   * \brief Get id of face meeting the boundary along the edge \c eid.
   *
   * \param \c bid The boundary identifier.
   * \param \c eid The edge identifier. It must have been obtained from the
   *               \c vector returned by \sa get_domain_edges(int).
   * \return The fid of the meeting face or -1 if none exists.
   */
  int get_meeting_face(int bid, int eid) const;
  /**
   * \brief Get id of boundary meeting this boundary along the edge \c eid.
   *
   * \param \c bid The boundary identifier.
   * \param \c eid The edge identifier.
   * \return A tuple containing the bid of the meeting boundary or -1
   *         if none exists, the axis of the meeting edge and the interval.
   * 
   * A boundary is considered meeting this boundary only if its edge along
   * the meeting line \em contains the meeting edge of this boundary.
   */
  std::tuple<int,fd::Axis,fd::Interval> get_meeting_bid(int bid, int eid) const;

  /** \brief Get face name. */
  static const char *face  (int fid);
  /** \brief Get edge name. */
  static const char *edge  (int eid);
  /** \brief Get vertex name. */
  static const char *vertex(int vid);

protected:

  /** Set solution domain (a, b, c, d, e, f) */
  void set_domain();
  /**
   * \brief Check boundaries
   *
   * Use this method only when a global boundary is there.
   */
  void check_boundaries(int begin_from) const;
  /** Get extremity of boundary (0 or 1). */
  int get_extremity(const Boundary *p) const;
#if 0
  /** Convert real number to mesh node. */
  int node(double x, fd::Axis i) const;
  /** Convert real point to mesh node. */
  std::array<int,3> node(const fd::Point &pt) const;
#endif // 0

};


class CubicMesh : public Mesh
{
  xBoundary xb_[2];
  yBoundary yb_[2];
  zBoundary zb_[2];

public:

  CubicMesh(int nx, int ny, int nz) : CubicMesh(nx, ny, nz, 1, 1, 1) {}
  CubicMesh(int n, double b, double d, double f) : CubicMesh(n, n, n, b, d, f){}
  CubicMesh(int nx, int ny, int nz, double b, double d, double f) :
      CubicMesh(nx, ny, nz, 0, b, 0, d, 0, f) {}
  CubicMesh(int nx, int ny, int nz,
            double a, double b, double c, double d, double e, double f);
};

} // namespace fd3

#endif // MESH3D_HPP_INCLUDED
