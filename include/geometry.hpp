#ifndef GEOMETRY_HPP_INCLUDED
#define GEOMETRY_HPP_INCLUDED

// #include <cmath>
#include <functional>
#include "basedefs.h"

//============================================================================//
//::::::::::::::::::::::::::::  Geometry classes  :::::::::::::::::::::::::::://
//============================================================================//

namespace fd
{

/** General geometry package methods. */
struct Geometry
{
  /** Set IO separator for geometry classes. */
  static const char set_io_separator(const char c);
};

/** The axes of an orthogonal coordinate system. */
enum Axis {X = 0, Y = 1, Z = 2};

/** Interval representation. */
struct Interval
{
  double a, b;
  /** Check if interval is a single point. */
  bool empty(Axis axis) const noexcept;
  /** Check if interval contains another interval. */
  bool contains(const Interval &r, Axis axis) const noexcept;
  /** Check if interval contains point. */
  bool contains(double x, Axis axis) const noexcept;
  /** Chenk if intervals coincide within specified resolution. */
  bool coincides(const Interval &r, Axis) const noexcept;
  /** Returns which extremity of the interval is the argument or -1 if it is not. */
  int extremity(double x, Axis) const noexcept;
  Interval intersect(const Interval &r, Axis i) const noexcept;
  /** Returns the smallest interval containing both intervals. */
  Interval hull(const Interval &r, Axis i) const;
  /** Replaces this interval with the hull of this and \c r. */
  inline void merge(const Interval &r, Axis i) {*this = hull(r, i);}
  /** Merges overlapping intervals. */
  static void merge(std::vector<Interval> &l, Axis i);
  /** Restore from input stream.
   * 
   * This method can restore character separated values. 
   */
  std::istream& restore(std::istream &is);
};

struct Point;

/** Two-dimensional point. */
struct Point2d
{
  double x, y;
  /** Access coordinates */
  double operator()(int i) const noexcept {return i == 0 ? x : y;}
  /** Check if points coincide within specified resolution.
   * 
   *  The points are assumed to be in the coordinate plane normal to axis \c n.
   *  \param n axis normal to coordinate plane containing the points.
   */
  bool coincides(const Point2d &pt, Axis n) const noexcept;
  /** Promote 2d-point in the coordinate plane perpendicular \c n to a 3d-point. */
  Point promote(const Axis n) const noexcept;
  /** Restore from input stream.
   * 
   * This method can restore character separated values. 
   */
  std::istream& restore(std::istream &is);
};

/** Point in 3d space. */
struct Point
{
  /** Coordinates of point. */
  double p[3];
  /** Project 3d-point along coordinate axis. */
  Point2d project(const Axis axis) const noexcept;
  /** Project 3d-point along coordinate axis. */
  Point2d project_oriented(const Axis axis) const noexcept;
  /** Access coordinates */
  double operator()(Axis i) const noexcept {return p[i];}
  /** Get position relative to a plane.
   * 
   *  The plane is normal to \c Axis \c n at \c pos.
   *  It returns -1 if the point is on the left of the plane, 1 if it is
   *  on the right, and 0 if it is on the plane.
   */
  int position_relative_to_plane_at(double pos, Axis n) const noexcept;
  /** Format point. */
  std::string format(const std::string &sep = _S",") const;
};

/** Representation of 2d rectangle. */
struct Rectangle
{
  using Ref = std::reference_wrapper<const Rectangle>;

  /** Coordinates of rectangle. */
  double a, b, c, d;

  /** Check if rectangle is a point or line.
   * 
   *  \param n axis normal to rectangle's plane.
   */
  bool empty(Axis n) const noexcept;
  /** Access coordinates */
  double operator()(int i) const;
  /** Check if rectangle coincides with another rectangle.
   * 
   *  \param n axis normal to rectangle planes.
   */
  bool coincides(const Rectangle &r, Axis n) const noexcept;
  /** Check if rectangle contains a point.
   * 
   *  \param pt point to check.
   *  \param n axis normal to rectangle's plane.
   */
  bool contains(const Point2d &pt, Axis n) const noexcept;
  /** Check if rectangle contains another rectangle.
   * 
   *  \param n axis normal to rectangle's plane.
   */
  bool contains(const Rectangle &r, Axis n) const noexcept;
  /** Check if rectangle normal to axis \c n at \c d contains point \c pt. */
  bool contains_at(double d, const Point &pt, Axis n) const noexcept;
  /** Get rectangle's vertices. */
  inline std::array<Point2d,4> get_vertices() const
  { return {Point2d{a,c}, Point2d{b,c}, Point2d{b,d}, Point2d{a,d}}; }
  /** Computes area of rectangle. */
  double area(Axis n) const noexcept {return empty(n) ? 0. : (b-a)*(d-c);}
  /** Returns the smallest rectangle containing both rectangles. */
  Rectangle rect_hull(const Rectangle &rect, Axis n) const;
  /** Returns the smallest rectangle containing all rectangles. */
  static Rectangle rect_hull(const std::vector<Ref> &l, Axis n);
  /** Returns the intersection of the rectangles.
   * 
   *  \param n axis normal to rectangle's plane.
   */
  Rectangle intersect(const Rectangle &rect, Axis n) const noexcept;
  /** Returns true if no two of the rectangles intersect. */
  static bool pairwise_disjoint(const std::vector<Ref> &l, Axis n) noexcept;
  /** Restore from input stream.
   * 
   * This method can restore character separated values. 
   */
  std::istream& restore(std::istream &is);
};

/** \brief Get plane coordinate axis.
 *
 * \param n Axis normal to plane.
 * \param which One of the two coordinate axes of the plane; 0 or 1.
 */
Axis plane_coordinate_axis(const Axis n, int which);

} // namespace fd

std::ostream& operator<<(std::ostream &os, const fd::Point2d &pt);
std::ostream& operator<<(std::ostream &os, const fd::Interval &i);
std::ostream& operator<<(std::ostream &os, const fd::Rectangle &r);
std::istream& operator>>(std::istream &is, fd::Point2d &pt);
std::istream& operator>>(std::istream &is, fd::Interval &i);
std::istream& operator>>(std::istream &is, fd::Rectangle &r);

#endif // GEOMETRY_HPP_INCLUDED
