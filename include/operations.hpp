#ifndef OPERATIONS_HPP_INCLUDED
#define OPERATIONS_HPP_INCLUDED

#include "basedefs.h"
#include "codefrag.h"

namespace fd
{

//  ============================================================================
//  Operations between operators
//  ============================================================================

template<char _OP>
struct OperationProcessor
{
  const fd::CodeFragment& a;
  const fd::CodeFragment& b;

  // f: first operand
  // s: second operand

  OperationProcessor(const fd::CodeFragment& f, const fd::CodeFragment& s) :
    a(f), b(s) {}

  static
  std::string apply(const std::string &f, bool f_usep, const std::string &s, bool s_usep)
  {
    const std::string lpa = f_usep ? "(" : "";
    const std::string rpa = f_usep ? ")" : "";
    const std::string lpb = s_usep ? "(" : "";
    const std::string rpb = s_usep ? ")" : "";
    return lpa + f + rpa + std::string(1, _OP) + lpb + s + rpb;
  }

  static
  std::string apply_dif(const std::string &df, const std::string &f, bool f_usep,
                        const std::string &ds, const std::string &s, bool s_usep)
  {
    if (_OP == '*') return apply_dif_mul(df, f, f_usep, ds, s, s_usep);
    if (_OP == '/') return apply_dif_div(df, f, f_usep, ds, s, s_usep);
    return apply(df, f_usep, ds, s_usep);
  }

  static
  std::string apply_dif_mul(const std::string &df, const std::string &f, bool f_usep,
                            const std::string &ds, const std::string &s, bool s_usep)
  {
    if (df == "0") return ds == "0" ? _S"0" : "(" + f + ")*(" + ds + ")";
    if (ds == "0") return "(" + s + ")*(" + df + ")";
    return "(" + s + ")*(" + df + ")+(" + f + ")*(" + ds + ")";
  }

  static
  std::string apply_dif_div(const std::string &df, const std::string &f, bool f_usep,
                            const std::string &ds, const std::string &s, bool s_usep)
  {
    if (df == "0") return ds == "0" ? _S"0" : "-(" + f + ")*(" + ds + ")/((" + s + ")*(" + s + "))";
    if (ds == "0") return "(" + df + ")/(" + s + ")";
    return "(" + df + "-(" + f + ")*(" + ds + ")/(" + s + "))/(" + s + ")";
  }
};

template<typename T> // T: AddProcessor, SubProcessor etc.
class Operation : protected T
{
public:
  Operation(const fd::CodeFragment& f, const fd::CodeFragment& s) : T(f, s) {}

  fd::CodeFragment apply() const
  {
    return (T::a.boundary_code() || T::b.boundary_code()) ?
        apply_with_bx() : apply_std();
  }

protected:
  fd::CodeFragment apply_std() const
  {
    const CodeFragment& a = T::a;
    const CodeFragment& b = T::b;
    const std::string code_y = T::apply    (a.text_y, T::useby1(),
                                            b.text_y, T::useby2());
    const std::string code_j = T::apply_dif(a.text_j, a.text_y, T::usebj1(),
                                            b.text_j, b.text_y, T::usebj2());
    CodeFragment cf(code_y, code_j);
    cf.terminal = false;
    return cf;
  }

  CodeFragment apply_with_bx() const
  {
    const CodeFragment& a = T::a;
    const CodeFragment& b = T::b;

    CodeFragment cf = apply_std();

    for (int i = 0; i < CodeFragment::sizeof_bx(); ++i)
    {
      if (!a.bx_code(i) && b.bx_code(i))
      {
        cf.text_bx_y[i] = T::apply    (a.text_y, T::useby1(),
                                       b.text_bx_y[i], T::usebxy2(i));
        cf.text_bx_j[i] = T::apply_dif(a.text_j, a.text_y, T::usebj1(),
                                       b.text_bx_j[i], b.text_bx_y[i], T::usebxj2(i));
      }
      else if (a.bx_code(i) && !b.bx_code(i))
      {
        cf.text_bx_y[i] = T::apply    (a.text_bx_y[i], T::usebxy1(i),
                                       b.text_y, T::useby2());
        cf.text_bx_j[i] = T::apply_dif(a.text_bx_j[i], a.text_bx_y[i], T::usebxj1(i),
                                       b.text_j, b.text_y, T::usebj2());
      }
      else if (a.bx_code(i) && b.bx_code(i))
      {
        cf.text_bx_y[i] = T::apply    (a.text_bx_y[i], T::usebxy1(i),
                                       b.text_bx_y[i], T::usebxy2(i));
        cf.text_bx_j[i] = T::apply_dif(a.text_bx_j[i], a.text_bx_y[i], T::usebxj1(i),
                                       b.text_bx_j[i], b.text_bx_y[i], T::usebxj2(i));
      }
    }

    apply_vx(cf);

    return cf;
  }

  void apply_vx(CodeFragment &cf) const
  {
    const CodeFragment& a = T::a;
    const CodeFragment& b = T::b;

    for (int i = 0; i < CodeFragment::sizeof_vx(); ++i)
    {
      if (!a.vx_code(i) && b.vx_code(i))
      {
        cf.text_vx_y[i] = T::apply    (a.text_y, T::useby1(),
                                       b.text_vx_y[i], T::usevxy2(i));
        cf.text_vx_j[i] = T::apply_dif(a.text_j, a.text_y, T::usebj1(),
                                       b.text_vx_j[i], b.text_vx_y[i], T::usevxj2(i));
      }
      else if (a.vx_code(i) && !b.vx_code(i))
      {
        cf.text_vx_y[i] = T::apply    (a.text_vx_y[i], T::usevxy1(i),
                                       b.text_y, T::useby2());
        cf.text_vx_j[i] = T::apply_dif(a.text_vx_j[i], a.text_vx_y[i], T::usevxj1(i),
                                       b.text_j, b.text_y, T::usebj2());
      }
      else if (a.vx_code(i) && b.vx_code(i))
      {
        cf.text_vx_y[i] = T::apply    (a.text_vx_y[i], T::usevxy1(i),
                                       b.text_vx_y[i], T::usevxy2(i));
        cf.text_vx_j[i] = T::apply_dif(a.text_vx_j[i], a.text_vx_y[i], T::usevxj1(i),
                                       b.text_vx_j[i], b.text_vx_y[i], T::usevxj2(i));
      }
    }
  }
};

struct AddProcessor : OperationProcessor<'+'>
{
  AddProcessor(const fd::CodeFragment& f, const fd::CodeFragment& s) :
    OperationProcessor<'+'>(f, s) {}

protected:
  bool usebj1  ()          const {return false;}
  bool useby1  ()          const {return false;}
  bool usebxy1 (int which) const {return false;}
  bool usebxj1 (int which) const {return false;}
  bool usevxy1 (int which) const {return false;}
  bool usevxj1 (int which) const {return false;}
  bool useby2  ()          const;
  bool usebj2  ()          const;
  bool usebxy2 (int which) const;
  bool usebxj2 (int which) const;
  bool usevxy2 (int which) const;
  bool usevxj2 (int which) const;
};

struct SubProcessor : OperationProcessor<'-'>
{
  SubProcessor(const fd::CodeFragment& f, const fd::CodeFragment& s) :
    OperationProcessor<'-'>(f, s) {}

protected:
  bool useby1  ()          const {return false;}
  bool usebj1  ()          const {return false;}
  bool usebxy1 (int which) const {return false;}
  bool usebxj1 (int which) const {return false;}
  bool usevxy1 (int which) const {return false;}
  bool usevxj1 (int which) const {return false;}
  bool useby2  ()          const;
  bool usebj2  ()          const;
  bool usebxy2 (int which) const;
  bool usebxj2 (int which) const;
  bool usevxy2 (int which) const;
  bool usevxj2 (int which) const;
};

struct MulProcessor : OperationProcessor<'*'>
{
  MulProcessor(const fd::CodeFragment& f, const fd::CodeFragment& s) :
    OperationProcessor<'*'>(f, s) {}

protected:
  bool useby1  ()          const;
  bool usebj1  ()          const;
  bool usebxy1 (int which) const;
  bool usebxj1 (int which) const;
  bool usevxy1 (int which) const;
  bool usevxj1 (int which) const;
  bool useby2  ()          const;
  bool usebj2  ()          const;
  bool usebxy2 (int which) const;
  bool usebxj2 (int which) const;
  bool usevxy2 (int which) const;
  bool usevxj2 (int which) const;
};

struct DivProcessor : OperationProcessor<'/'>
{
  DivProcessor(const fd::CodeFragment& f, const fd::CodeFragment& s) :
    OperationProcessor<'/'>(f, s) {}

protected:
  bool useby1  ()          const;
  bool usebj1  ()          const;
  bool usebxy1 (int which) const;
  bool usebxj1 (int which) const;
  bool usevxy1 (int which) const;
  bool usevxj1 (int which) const;
  bool useby2  ()          const;
  bool usebj2  ()          const;
  bool usebxy2 (int which) const;
  bool usebxj2 (int which) const;
  bool usevxy2 (int which) const;
  bool usevxj2 (int which) const;
};

typedef Operation<AddProcessor> PlusOp;
typedef Operation<SubProcessor> MinusOp;
typedef Operation<MulProcessor> MultOp;
typedef Operation<DivProcessor> DivOp;

void set_operations_options(const std::string& which, bool value);

} // namespace fd

#endif // OPERATIONS_HPP_INCLUDED
