#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info =
R"(/**
 *  Project: Laplace PDE with mixed Neumann && Dirichlet BCs, No. 2
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  BCs:
 *  b1: $-\frac{\partial u}{\partial x}+u=\sin(x+y)-\cos(x+y)$
 *  b3: $-\frac{\partial u}{\partial y}+u=\sin(x+y)-\cos(x+y)$
 *  b2: $\frac{\partial u}{\partial x}+u=\sin(x+y)+\cos(x+y)$
 *  b4: $u=u_exact$
 *
 *  PDE:
 *  $u_{xx}+u_{yy}=-2\sin(x+y)$
 *
 *  Exact solution:
 *  $u=\sin(x+y)$
 *
 *  Boundary discretization method: all that apply
 *  Linear solver: PETSC
 */
)";

int main()
{
  try
  {
    xBoundary b1(1., 0., 1.), b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.), b4(0., 0., 1.);

    Mesh mesh(50, 50);
    mesh.set_boundaries({b3, b2, b4, b1});

    DirichletBC bc4(b4, "bc");
    NeumannBC bc1(b1, "-1", "1", "nbc_c1");
    NeumannBC bc2(b2,  "1", "1", "nbc_c2");
    NeumannBC bc3(b3, "-1", "1", "nbc_c1");

    // Part 1: declaration of fields and functions
    Field u("u");
    Function f("f");
    Function uex("u_exact"), xs("exact_solution");
    Function bc("bc"), g1("nbc_c1"), g2("nbc_c2");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "Mixed";
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4/*SYM_DIFF2;*/);

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_BCs({bc1, bc2, bc3, bc4}, u);
    pde.set_functions({f, bc, g1, g2, uex, xs});

    f.set_implementation("-2*sin(x+y)");
    bc.set_implementation("u_exact(x,y)");
    g1.set_implementation("sin(x+y)-cos(x+y)");
    g2.set_implementation("sin(x+y)+cos(x+y)");
    uex.set_implementation("sin(x+y)");
    xs.set_implementation("u_exact(x,y)");

    // Part 4: set PDE equations
    pde = Dxx[u] + Dyy[u] - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc2d_mix_2";
    pde.project_info = project_info;
    pde.compiler = "clang++";
    pde.set_output("code/petsc2d_mix_2");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
