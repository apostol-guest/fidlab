#include <iostream>
#include <fidlab/pde3d.hpp>

using namespace fd;
using namespace fd3;

const char *project_info =
R"(/**
 *  Project: Nonlinear PDE-2d with Dirichlet BCs, No. 2
 *
 *  Features
 *  + Custom BCs.
 *  + Nonlocal BCs.
 *  + Custom initial estimate setting.
 *  + Custom Discretizer.
 *  + Nonlocal (localized) Operator/Discretizer.
 *
 *  PDE:
 *  $-\Delta u+e^u vD_y u D_x v+u D_y v+uv+v^2=f(x,y)$
 *  $-u\Delta v+(v^2+u)D_x v+e^v u^2=g(x,y)$
 *  $u_{xx}+u_{yy}+u_{zz}-uu_x=-(2+\cos(x+y))\sin(x+y)$
 *
 *  BCs:
 *  b0: $0$
 *  b1: $1$
 *
 *  Exact solution:
 *  $u(x)=x^2+y+z$
 *  $v(x)=x+y+z^2$
 *
 *  Boundary discretization method: INWARD_DIFF3
 *  Nonlinear solver: GSL Newton.
 */
)";

class PtEvalDiscretizer : public Discretizer
{
public:
	std::string point;
	PtEvalDiscretizer() : point("0") {}
	PtEvalDiscretizer(const std::string& pt) : point(pt) {}

	DECL_DISCRETIZER_DUP();
	virtual CodeFragment discretize(const DiscretizationData& d) const override;
	virtual CodeFragment discretize_on_boundary(
							const BoundaryDiscretizationData& bdd) const override;
  virtual CodeFragment discretize_scalar_field(
							const DiscretizationData& d) const override;

	SET_OPERATOR_NAME("ptev-operator");
};

// TODO: API replace code_elem_func("e", i, "$c$") --> element_function(i)

CodeFragment PtEvalDiscretizer::discretize(const DiscretizationData &d) const
{
	const std::string &i = point;
	const std::string &j = d.j;
	const std::string &k = d.k;
  const PDECommon &pde = *d.pde;
	const std::string code_y = "u["  + pde.code_elem_func(i, j, k) + "]";
	const std::string code_j = "du[" + pde.code_elem_func(i, j, k) + "]";
	return CodeFragment(code_y, code_j);
}

CodeFragment PtEvalDiscretizer::discretize_on_boundary(
		const BoundaryDiscretizationData &bdd) const
{
	CodeFragment cf = discretize({bdd.pde, bdd.i, bdd.j, bdd.k});
	cf.bid = bdd.bid;
	return cf;
}

// TODO: API replace code_func_call("$f$", point) --> function_call(point)

CodeFragment PtEvalDiscretizer::discretize_scalar_field(
		const DiscretizationData& d) const
{
	const std::string code_y = d.pde->code_func_call("$f$", point, d.j, d.k);
	const std::string code_j = "0";
	return CodeFragment(code_y, code_j);
}

IMPLEMENT_DISCRETIZER_DUP(PtEvalDiscretizer);

int main()
{
  try
  {
		// Create localized non-local operator
		PtEvalDiscretizer pteval_discretizer("0");
		BasicOperator PtEval(pteval_discretizer);

		// Create mesh
    xBoundary b0(1, {0, 1, 0, 1});
    xBoundary b1(0, {0, 1, 0, 1});

    Mesh mesh({}, 17, 17, 17);
    mesh.set_boundaries({b0, b1});

    DirichletBC bcu(mesh.get_global_boundary(), "u_ex");
    DirichletBC bcv(mesh.get_global_boundary(), "v_ex");
    CustomBC bc0u(b0), bc1u(b1);
    CustomBC bc0v(b0), bc1v(b1);

    // Part 1: declaration of fields and functions
    Field u("u"), v("v"), w;
    Function f("f"), g("g"), X("xc"), Y("yc"), Z("zc"), f1("f_1"), f2("f_2");
    Function xs("exact_solution"), uex("u_ex"), vex("v_ex");
    FieldFunction G("G", 2), Exp("f_exp",1), Uex("U_ex",2), Vex("V_ex",2);
    Operator L("L");

    // Set PDE BCs
    bc0u = u - PtEval[v] - f2; // Nonlocal
    bc1u = -v*Dx[u] + (u-Y-Z)*Exp(v);
    bc0v = -Dx[v] + v - Y - Z*Z;
    bc1v = v + .25*Exp(u) - f1;

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE3d";
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4);

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v});
    pde.set_BCs({bc0u, bc1u, bcu}, u);
    pde.set_BCs({bc0v, bc1v, bcv}, v);
    pde.set_functions({f, g, xs, X, Y, Z, f1, f2});
    pde.set_functions({uex, vex});
    pde.set_functions({G, Exp, Uex, Vex});
    pde.set_initial_estimate("c == 0 ? x+y : y+z");
//    pde.set_initial_estimate("c == 0 ? x*x+y+.9*z : .9*(x+y)+z*z");
//    pde.set_initial_estimate("c == 0 ? x : (x < .25 ? 1 : 2)");
//    pde.set_initial_estimate("c == 0 ? 1.1*x*x + .9*y : x+.9*y");
    pde.use_field_names = true;

    xs.set_implementation("c == 0 ? u_ex(x,y,z) : v_ex(x,y,z)");
    uex.set_implementation("x*x+y+z");
    vex.set_implementation("x+y+z*z");
    X.set_implementation("x");
    Y.set_implementation("y");
    Z.set_implementation("z");
    f1.set_implementation("y+z*z+.25*exp(y+z)");
    f2.set_implementation("1+z-z*z");
    f.set_implementation("U_ex(u_ex(x,y,z),v_ex(x,y,z))");
    g.set_implementation("V_ex(u_ex(x,y,z),v_ex(x,y,z))");

    G.set_implementation({"exp(u)*v",
      /* G_u */ "G(u,v)",
      /* G_v */ "exp(u)"});
    Uex.set_implementation({"-2+v*exp(u)+u+u*v+v*v","0","0"});
    Vex.set_implementation({"-2*u+v*v+u+exp(v)*u*u","0","0"});
    Exp.set_implementation({"exp(u)", /* derivative */ "exp(u)"});

    L(w) = Dxx[w] + Dyy[w] + Dzz[w];

    // Part 4: set PDE equations
    pde("u") = -L[u] + G(u,v)*Dy[u]*Dx[v] + u*Dx[v] + u*v + v*v - f;
    pde("v") = -u*L[v] + (v*v+u)*Dx[v] + G(v,u)*u - g;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_nonlinear_solver("newton");

    // Part 6: output
    pde.executable = "gsl3d_nl_2";
//    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl3d_nl_2");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    exit(1);
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    exit(1);
  }

  return 0;
}
