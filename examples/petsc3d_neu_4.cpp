#include <iostream>
#include <fidlab/pde3d.hpp>
#include "range.hpp"

using namespace fd;
using namespace fd3;

const char *project_info = R"(/**
 *  @brief Project: Laplace equation with Neumann BCs, No. 4
 *
 *  Features:
 *  \li system of 2 pdes
 *  \li operators
 *
 *  BCs:
 *  $$
 *  \begin{align*}
 *  & \frac{\partial u}{\partial x_i}-u=2x_i-u_{exact} && \text{all boundaries} \\
 *  & \frac{\partial u}{\partial x}-2u=2x-2u_{exact} && \text{b0} \\
 *  & \frac{\partial u}{\partial x}-4u=2x-4u_{exact} && \text{b1} \\
 *  & \frac{\partial u}{\partial z}-6u=2z-6u_{exact} && \text{b6} \\
 *  & && \\
 *  & \frac{\partial v}{\partial x_i}+v=(x+y+z+x_i)\frac{xyz}{x_i}+v_{exact} && \text{all boundaries} \\
 *  & \frac{\partial v}{\partial x}+2v=(2x+y+z)yz+2v_{exact} && \text{b0} \\
 *  & \frac{\partial v}{\partial x}+3v=(2x+y+z)yz+3v_{exact} && \text{b1} \\
 *  & \frac{\partial v}{\partial z}+4v=(x+y+2z)xy+4v_{exact} && \text{b6} \\
 *  \end{align*}
 *  $$
 *  PDE:
 * 	$\Delta u-2\Delta v=6-4(x*y+y*z+z*x)$
 * 	$\Delta u+2\Delta v=6+(x*y+y*z+z*x)$
 *
 * 	Exact solution:
 * 	$u=x^2+y^2+z^2$
 * 	$v=xyz(x+y+z)$
 *
 *  Boundary discretization method: inward differencing scheme (INWARD_DIFFx)
 *  Linear solver: PETSc
 */
)";

int main()
{
  try
  {
    xBoundary b0(6, {  0, .5, 2.8,   3});
    xBoundary b1(6, {-.8, .7,   2, 2.5});
    zBoundary b6(3, {  4,  6, -.5,  .3});

    Mesh mesh({1, 6, -1, 1, 2, 3}, 39, 17, 11);
    mesh.set_boundaries({b0, b1, b6});

    NeumannBC bcgu(mesh.get_global_boundary(), "", "-1", "gu");
    NeumannBC bc0u(b0, "", "-2", "g0u");
    NeumannBC bc1u(b1, "", "-4", "g1u");
    NeumannBC bc6u(b6, "", "-6", "g6u");
    NeumannBC bcgv(mesh.get_global_boundary(), "", "1", "gv");
    NeumannBC bc0v(b0, "", "2", "g0v");
    NeumannBC bc1v(b1, "", "3", "g1v");
    NeumannBC bc6v(b6, "", "4", "g6v");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f"), g("g"), xs("exact_solution");
    Function gu("gu"), g0u("g0u"), g1u("g1u"), g6u("g6u");
    Function gv("gv"), g0v("g0v"), g1v("g1v"), g6v("g6v");
    Operator Delta("Delta");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE3d";

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v});
    pde.set_functions({f, g, xs, gu, gv, g0u, g1u, g6u, g0v, g1v, g6v});
    pde.set_BCs({bcgu, bc0u, bc1u, bc6u}, u);
    pde.set_BCs({bcgv, bc0v, bc1v, bc6v}, v);

    xs.set_implementation("c == 0 ? x*x+y*y+z*z : (x+y+z)*x*y*z");
    f.set_implementation(" {return 6-4*(x*y+y*z+z*x);}");
    g.set_implementation("6+2*(x*y+y*z+z*x)");
    g0u.set_implementation("2*x-2*(x*x+y*y+z*z)");
    g1u.set_implementation("2*x-4*(x*x+y*y+z*z)");
    g6u.set_implementation("2*z-6*(x*x+y*y+z*z)");
    g0v.set_implementation("(2*x+y+z)*y*z+2*(x+y+z)*x*y*z");
    g1v.set_implementation("(2*x+y+z)*y*z+3*(x+y+z)*x*y*z");
    g6v.set_implementation("(x+y+2*z)*x*y+4*(x+y+z)*x*y*z");
    gu.set_implementation(R"(
	{
		double pd;
		if (fabs(x-a)<RESOL[0] || fabs(x-b)<RESOL[0]) pd = x;
		else if (fabs(y-c)<RESOL[1] || fabs(y-d)<RESOL[1]) pd = y;
		else pd = z;
		return 2*pd-(x*x+y*y+z*z);
	})");
    gv.set_implementation(R"(
	{
		double pd, pr;
		if (fabs(x-a)<RESOL[0] || fabs(x-b)<RESOL[0])
		{
			pd = x;
			pr = y*z;
		}
		else if (fabs(y-c)<RESOL[1] || fabs(y-d)<RESOL[1])
		{
			pd = y;
			pr = x*z;
		}
		else
		{
			pd = z;
			pr = x*y;
		}
		const double sum = x+y+z;
		return (pd + sum)*pr + x*y*z*sum;
	})");

    // Part 4: set PDE equations
    Delta(u) = Dxx[u] + Dyy[u] + Dzz[u];
    pde("u") = Delta[u] - 2*Delta[v] - f;
    pde("v") = Delta[u] +   Delta[v] - g;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");
    // pde.auto_system_config = true;

    // Part 6: output
    pde.executable = "petsc3d_neu_4";
    pde.project_info = project_info;
//		pde.compiler = "clang++";
    pde.set_output("code/petsc3d_neu_4");
    pde.generate_code();
    pde.generate_makefile();
	}
	catch (const std::string& msg)
	{
      std::cout << msg << std::endl;
      return 1;
	}
	catch (const char *msg)
	{
      std::cout << "Caught const char* exception!\n";
      std::cout << msg << std::endl;
      return 1;
	}

	return 0;
}
