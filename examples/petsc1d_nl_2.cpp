#include <iostream>
#include <fidlab/pde1d.hpp>

using namespace fd;
using namespace fd1;

const char *project_info =
R"(/**
 *  Project: Nonlinear PDE-1d with Dirichlet BCs, No. 2
 *  Features custom initial estimate setting.
 *
 *     --+----------+-----> x
 *       0          1
 *      [b0]       [b1]
 *
 *  PDE:
 *  $u_{xx}-2*u^2=0$
 *
 *  BCs:
 *  b0: $0$
 *  b1: $1$
 *
 *  Exact solutions:
 *  $u_1(x)=\frac{3}{(2+x)^2}$
 *  $u_2(x)=?$
 *
 *  Boundary discretization method: does not apply (INWARD_DIFF3)
 *  Linear solver: PETSc specified from command line.
 */
)";

int main()
{
  try
  {
    Boundary b0(0.);
    Boundary b1(1.);

    Mesh mesh(50);
    mesh.set_boundaries({b0, b1});

    DirichletBC bc0(b0, "0.75"), bc1(b1, "0.333333333333333333");

    // Part 1: declaration of fields and functions
    Field u("u");
    Function f("f");
    Function a("fa"), b("fb"), xs("exact_solution");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE1d";

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_BCs({bc1, bc0}, u);
    pde.set_functions({f, a, b, xs});
    pde.set_initial_estimate("1.-x");

    a.set_implementation("0");
    b.set_implementation("1./3.");
    f.set_implementation("0");
    xs.set_implementation("3/((2+x)*(2+x))");

    // Part 4: set PDE equations
    pde = Dxx[u] - 2.*u*u;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc1d_nl_2";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/petsc1d_nl_2");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
