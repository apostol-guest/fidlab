#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info =
R"(/**
 *  Project: Laplace equation with custom BCs, No. 1
 *
 *  Identical to Project Neumann 2 (Laplace PDE with Neumann BCs).
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  BCs:
 *  b1: $-\frac{\partial u}{\partial x}+2u=0$
 *  b3: $-\frac{\partial u}{\partial y}+u=-e^{x^2+1}$
 *  b2: $\frac{\partial u}{\partial x}=0$
 *  b4: $\frac{\partial u}{\partial y}=0$
 *
 *  PDE:
 *  $u_{xx}+u_{yy}=4(x^2+y^2+1)e^{x^2+y^2}$
 *
 *  Exact solution:
 *  $u=e^{x^2+y^2}$
 *
 *  Linear solver: GSL LU
 */
)";

int main()
{
  try
  {
    xBoundary b1(1., 0., 1.), b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.), b4(0., 0., 1.);

    Mesh mesh(50, 50);
    mesh.set_boundaries({b3, b2, b4, b1});

    // Part 0: declaration of fields and functions
    Field u("u");
    Function f("f");
    Function uex("exact_solution"), g("g");

    // Part 1: declaration and definition of BCs
    CustomBC bc1(b1), bc2(b2), bc3(b3), bc4(b4);

    bc1 = -Dx[u] + 2*u;
    bc3 = -Dy[u] + u - g;
    bc2 = Dx[u];
    bc4 = Dy[u];

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4); // SYM_DIFF2 creates an error
    pde.name = "PDE2d";

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_functions({f, g, uex});
    pde.set_BCs({bc1, bc2, bc3, bc4}, u);

    f.set_implementation("4*(x*x+y*y+1)*exp(x*x+y*y)");
    g.set_implementation("-exp(x*x+1)");
    uex.set_implementation("exp(x*x+y*y)");

    // Part 4: set PDE equations
    pde = Dxx[u] + Dyy[u] - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");

    // Part 6: output
    pde.executable = "gsl2d_cus_1";
    pde.project_info = project_info;
    pde.compiler = "clang++";
    pde.set_output("code/gsl2d_cus_1");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
