#include <iostream>
#include <fidlab/pde3d.hpp>

using namespace fd;
using namespace fd3;

const char *project_info =
R"(/**
 * Project: Nonlinear PDE-2d with Dirichlet BCs, No. 3
 * Features
 * + Field functions with space dependence.
 *
 * PDE:
 * $
 * \begin{matrix}
 * \Delta u_1-2D_x u_1-4u_1-\Delta u_3-5D_y u_3-6u_3+u_1(u_2-e^{2x+y})+u_3=e^{-3y}\\
 * \Delta u_2-3D_x u_2-D_y u_2+2u_2+2\Delta u_1-4D_x u_1-8u_1+3u_2u_3=3e^{2(x-y)}\\
 * \Delta u_3+5D_y u_3+6u_3+u_1^2u_2u_3=1\\
 * 6\Delta u_4-3D_x u_4+4D_y u_4-2u_4+3\Delta u_1-6D_x u_1-12u_1+u_1^2D_y u_2=e^{3y}
 * \end{matrix}
 * $
 *
 * BCs:
 * b0: $0$
 * b1: $1$
 *
 * Exact solution:
 * $u_1(x)=e^{-x+y}$
 * $u_2(x)=e^{2x+y}$
 * $u_3(x)=e^{-3y}$
 * $u_4(x)=e^{\frac{1}{2}x-y}$
 *
 * Boundary discretization method: INWARD_DIFFx
 * Linear solver: GSL Newton
 */
)";

int main()
{
  try
  {
    Mesh mesh({}, 13, 13, 13);

    // Part 1: declaration of fields and functions
    Field u("u"), v("v"), w("w"), z("z");
    Function f0("f0"), f1("f1"), f2("f2"), f3("f3");
    Function uex("u_ex"), vex("v_ex"), wex("w_ex"), zex("z_ex");
    FieldFunction G("G", 1, true);
    Function xs("exact_solution");
    Operator D("Laplace");

    // BCs
    DirichletBC bcu(mesh.get_global_boundary(), "u_ex");
    DirichletBC bcv(mesh.get_global_boundary(), "v_ex");
    DirichletBC bcw(mesh.get_global_boundary(), "w_ex");
    DirichletBC bcz(mesh.get_global_boundary(), "z_ex");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE3d";
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4);

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v, w, z});
    pde.set_BCs({bcu}, u);
    pde.set_BCs({bcv}, v);
    pde.set_BCs({bcw}, w);
    pde.set_BCs({bcz}, z);
    pde.set_functions({f0, f1, f2, f3, xs});
    pde.set_functions({uex, vex, wex, zex});
    pde.set_functions({G});
//    pde.set_initial_estimate("c == 0 ? x : 1");
//    pde.use_field_names = true;

    uex.set_implementation("exp(-x+y-z)");
    vex.set_implementation("exp(2*x+y-z)");
    wex.set_implementation("exp(-3*y)");
    zex.set_implementation("exp(.5*x-y-z)");
    xs.set_implementation(R"(
	{
		switch (c)
		{
			case 0: return u_ex(x,y,z);
			case 1: return v_ex(x,y,z);
			case 2: return w_ex(x,y,z);
			case 3: return z_ex(x,y,z);
		}
		throw std::string(__PRETTY_FUNCTION__)+": field index out of bounds.";
		return 0;
	})");
    f0.set_implementation("0");
    f1.set_implementation("3*exp(2*(x-y)-z)");
    f2.set_implementation("exp(-3*z)");
    f3.set_implementation("exp(3*(y-z))");

    G.set_implementation({"u-exp(2*x+y-z)", /* G_u */ "1"});

    D(w) = Dxx[w] + Dyy[w] + Dzz[w];

    // Part 4: set PDE equations
    pde("u") = D[u]-Dx[u]-4*u-D[w]-5*Dy[w]-6*w+u*G(v);//-f0;
    pde("v") = D[v]-3*Dx[v]-Dy[v]+v+2*D[u]-2*Dx[u]-8*u+3*v*w-f1;
    pde("w") = D[w]+5*Dy[w]+6*w+u*u*v*w-f2;
    pde("z") = 6*D[z]-3*Dx[z]+4*Dy[z]-8*z+3*D[u]+6*Dx[u]-3*u+u*u*Dy[v]-f3;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc3d_nl_3";
//    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/petsc3d_nl_3");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
