#include <iostream>
#include "pdexd3d.hpp"

using namespace fd;
using namespace extdom::d3;
using std::cout;

const char *project_info = R"(/**
 *  @brief Project: Laplace equation with Neumann BCs, No. 2
 *
 *  This project uses extended domains.
 *
 *  BCs:
 *  $u=u_exact$ on all boundaries
 *
 *  PDE:
 * 	$u_{xx}+u_{yy}+u_{zz}=-3sin(x+y+z)$
 *
 * 	Exact solution:
 * 	$u=sin(x+y+z)$
 *
 *  Boundary discretization method: extended domain (SYM_DIFF2)
 *  Linear solver: GSL LU
 */
)";

int main()
{
	try
	{
    // The number of nodes in each direction is critical
    // for the accuracy of the calculation.
    Mesh mesh({1,6,-1,1,2,3}, 49,18,11);
		NeumannBC bc(mesh.get_global_boundary(), "","2","g");

		// Part 1: declaration of fields and functions
		Field u("u");
		Function f("f"), xs("exact_solution"), g("g");

		// Part 2: declaration and customization of PDE
		PDE pde(mesh);
		pde.name = "PDE3d";

		// Part 3: set PDE fields, BCs and functions
		pde.set_field(u);
		pde.set_functions({f, g, xs});
		pde.set_BCs({bc});

		xs.set_implementation("sin(x+y+z)");
		f.set_implementation(" {return -3*sin(x+y+z);}");
		g.set_implementation("cos(x+y+z)+2*sin(x+y+z)");
/*		xs.set_implementation("x*x+y*y+z*z");
		f.set_implementation("6");
		g.set_implementation(R"(
  {
    double r;
    if (fabs(x-a) < RESOL[0] || fabs(x-b) < RESOL[0]) r = x;
    else if (fabs(y-c) < RESOL[1] || fabs(y-d) < RESOL[1]) r = y;
    else r = z;
    return 2*r+2*(x*x+y*y+z*z);
  })");*/

		// Part 4: set PDE equations
		pde("Neumann") = Dxx[u] + Dyy[u] + Dzz[u] - f;

		std::cout << "PDE";
		std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

		// Part 5: set methods
		pde.set_library("gsl");
		pde.set_matrix_method("LU");
//		pde.auto_system_config = true;

		// Part 6: output
		pde.executable = "gsl3d_nxd_2";
		pde.project_info = project_info;
//		pde.compiler = "clang++";
		pde.set_output("code/gsl3d_nxd_2");
		pde.generate_code();
		pde.generate_makefile();
	}
	catch (const std::string& msg)
	{
		std::cout << msg << std::endl;
		return 1;
	}
	catch (const char *msg)
	{
		std::cout << "Caught const char* exception!\n";
		std::cout << msg << std::endl;
		return 1;
	}

	return 0;
}
