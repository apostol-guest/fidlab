#include <iostream>
#include <fidlab/pde3d.hpp>
#include "range.hpp"

using namespace fd;
using namespace fd3;
using std::cout;

const char *project_info = R"(/**
 *	@brief Project: Laplace equation with Neumann BCs, No. 3
 *
 *  BCs:
 *  $\frac{\partial u}{\partial x_i}+u=\cos(x+y+z)+\sin(x+y+z)$ on all boundaries
 *  $\frac{\partial u}{\partial x}+2u=\cos(x+y+z)+2\sin(x+y+z)$ on b0
 *  $\frac{\partial u}{\partial x}+3u=\cos(x+y+z)+3\sin(x+y+z)$ on b1
 *  $\frac{\partial u}{\partial z}+4u=\cos(x+y+z)+4\sin(x+y+z)$ on b6
 *
 *  PDE:
 * 	$u_{xx}+u_{yy}+u_{zz}=-3\sin(x+y+z)$
 *
 * 	Exact solution:
 * 	$u=sin(x+y+z)$
 *
 *  Boundary discretization method: inward differencing scheme (INWARD_DIFFx)
 *  Linear solver: GSL LU
 */
)";

int main()
{
	try
	{
    xBoundary b0(6, {  0, .5, 2.8,   3});
    xBoundary b1(6, {-.8, .7,   2, 2.5});
    zBoundary b6(3, {  4,  6, -.5,  .3});

    Mesh mesh({1, 6, -1, 1, 2, 3}, 49, 19, 13);
    mesh.set_boundaries({b0, b1, b6});

    NeumannBC bcg(mesh.get_global_boundary(), "", "1", "g");
    NeumannBC bc0(b0, "", "2", "g0");
    NeumannBC bc1(b1, "", "3", "g1");
    NeumannBC bc6(b6, "", "4", "g6");
		// Part 1: declaration of fields and functions
		Field u("u");
		Function f("f"), xs("exact_solution"), g("g"), g0("g0"), g1("g1"), g6("g6");

		// Part 2: declaration and customization of PDE
		PDE pde(mesh);
		pde.name = "PDE3d";

		// Part 3: set PDE fields, BCs and functions
		pde.set_field(u);
		pde.set_functions({f, xs, g, g0, g1, g6});
		pde.set_BCs({bcg, bc0, bc1, bc6});

		xs.set_implementation("sin(x+y+z)");
		f.set_implementation(" {return -3*sin(x+y+z);}");
		g.set_implementation("cos(x+y+z)+sin(x+y+z)");
		g0.set_implementation("cos(x+y+z)+2*sin(x+y+z)");
		g1.set_implementation("cos(x+y+z)+3*sin(x+y+z)");
		g6.set_implementation("cos(x+y+z)+4*sin(x+y+z)");

		// Part 4: set PDE equations
		pde("Neumann") = Dxx[u] + Dyy[u] + Dzz[u] - f;

		std::cout << "PDE";
		std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

		// Part 5: set methods
		pde.set_library("gsl");
		pde.set_matrix_method("LU");
//		pde.auto_system_config = true;

		// Part 6: output
		pde.executable = "gsl3d_neu_3";
		pde.project_info = project_info;
//		pde.compiler = "clang++";
		pde.set_output("code/gsl3d_neu_3");
		pde.generate_code();
		pde.generate_makefile();
	}
	catch (const std::string& msg)
	{
		std::cout << msg << std::endl;
		return 1;
	}
	catch (const char *msg)
	{
		std::cout << "Caught const char* exception!\n";
		std::cout << msg << std::endl;
		return 1;
	}

	return 0;
}
