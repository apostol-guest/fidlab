#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info = R"(/**
 *  Project: Laplace equation with Neumann BCs, No. 0
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  BCs:
 *  b1: $-\frac{\partial u}{\partial x}+2u=2y^2$
 *  b3: $-\frac{\partial u}{\partial y}+u=x^2-1$
 *  b2: $\frac{\partial u}{\partial y}=0$
 *  b4: $\frac{\partial u}{\partial y}=0$
 *
 *  PDE:
 *  $u_{xx}+u_{yy}=4$
 *
 *  Exact solution:
 *  $u=x^2+y^2$
 *
 *  Linear solver: PETSC
 */
)";

int main()
{
  xBoundary b1(1., 0., 1.);
  xBoundary b2(0., 1., 0.);
  // The next two boundaries raise an exception because they
  // are impropeply oriented:
  #if 0
  yBoundary b3(1., 0., 1.);
  yBoundary b4(0., 1., 0.);
  #endif // 0
  yBoundary b3(1., 1., 0.);
  yBoundary b4(0., 0., 1.);

  try
  {
    Mesh mesh(5, 5);

    #if 0 // set boundaries one by one (deprecated)
    mesh.add_boundary(b3);
    mesh.add_boundary(b2);
    mesh.add_boundary(b4);
    mesh.add_boundary(b1);
    #endif // 0
    mesh.set_boundaries({b1,b2,b3,b4});

    NeumannBC bc1(b1, "-1", "2", "cx");
    NeumannBC bc2(b2, "1", "", "");
    NeumannBC bc3(b3, "-1", "1", "cy");
    NeumannBC bc4(b4, "1", "0", "");

    // Part 1: declaration of fields and functions
    Field u;
    Function cx("cx"), cy("cy"), xs("exact_solution");

    cx.set_implementation("2.*y*y");
    cy.set_implementation("x*x - 1.");
    xs.set_implementation("x*x+y*y");

    // Part 2: declaration of PDE
    PDE pde(&mesh);
    pde.name = "PDE2d";
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4/*SYM_DIFF2*/);

    // Part 3: set fields, BCs and functions
    pde.set_field(u);

    pde.set_BC(bc1);
    pde.set_BC(bc2, u);
    pde.set_BC(bc3);
    pde.set_BC(bc4, u);

    pde.set_functions({cx, cy, xs});

    // Part 4: PDE definition
    pde = Dxx[u] + Dyy[u] - 4.;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set code generation methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc2d_neu_0";
    pde.project_info = project_info;
    pde.compiler = "clang++";
    pde.set_output("code/petsc2d_neu_0");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
