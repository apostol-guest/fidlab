#include <iostream>
#include <fidlab/pde3d.hpp>

using namespace fd;
using namespace fd3;

const char *project_info =
R"(/**
 *  Project: Laplace equation with Custom BCs, No. 2
 *
 *  BCs:
 *  $\frac{\partial{u}}{\partial{x_i}}+u=\cos(x+y+z)+u_{exact}$
 *  on all boundaries
 *
 *  PDE:
 *  $u_{xx}+u_{yy}+u_{zz}=-3sin(x+y+z)$
 *
 *  Exact solution:
 *  $u=sin(x+y+z)$
 *
 *  Boundary discretization method: -
 *  Linear solver: GSL LU
 */
)";

int main()
{
  try
  {
    xBoundary b0(6, {  0, .5, 2.8,   3});
    xBoundary b1(6, {-.8, .7,   2, 2.5});
    zBoundary b6(3, {  4,  6, -.5,  .3});

    Mesh mesh({1, 6, -1, 1, 2, 3}, 29, 15, 13);
    mesh.set_boundaries({b0, b1, b6});

    // Part 1: declaration of fields and functions
    Field u("u");
    Function f("f"), xs("exact_solution"), g("g");
    Function g0("g0"), g1("g1"), g6("g6");
    CutoffFunction Hx("in_boundary_x"), Hy("in_boundary_y"), Hz("in_boundary_z");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE3d";

    // Part 3: set PDE fields, BCs and functions
    CustomBC bcg(mesh.get_global_boundary());
    CustomBC bc0(b0), bc1(b1), bc6(b6);
		bcg = Hx(Dx[u]) + Hy(Dy[u]) + Hz(Dz[u]) + u - g;
		bc0 = Dx[u] + 2*u - g0;
		bc1 = Dx[u] + 3*u - g1;
		bc6 = Dz[u] -   u - g6;

    pde.set_field(u);
    pde.set_functions({f, g, g0, g1, g6, xs});
    pde.set_functions({Hx, Hy, Hz});

    xs.set_implementation("sin(x+y+z)");
    f.set_implementation(" {return -3*sin(x+y+z);}");
    g.set_implementation("cos(x+y+z)+sin(x+y+z)");
    g0.set_implementation("cos(x+y+z)+2*sin(x+y+z)");
    g1.set_implementation("cos(x+y+z)+3*sin(x+y+z)");
    g6.set_implementation("cos(x+y+z)-sin(x+y+z)");
    Hx.set_implementation(R"(
  {
    return (fabs(x-a) < RESOL[0] || fabs(x-b) < RESOL[0]);
  })");
    Hy.set_implementation(R"(
  {
    return (fabs(y-c) < RESOL[1] || fabs(y-d) < RESOL[1]) && !in_boundary_x(x,y,z);
  })");
    Hz.set_implementation(R"(
  {
    return (fabs(z-p) < RESOL[2] || fabs(z-q) < RESOL[2]) && !in_boundary_x(x,y,z)
                                                          && !in_boundary_y(x,y,z);
  })");

    // Part 4: set PDE equations
    pde("Neumann") = Dxx[u] + Dyy[u] + Dzz[u] - f;
    pde.set_BC(bcg);
    pde.set_BCs({bc0, bc1, bc6});

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");
//    pde.auto_system_config = true;

    // Part 6: output
    pde.executable = "petsc3d_cus_2";
//    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/petsc3d_cus_2");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
