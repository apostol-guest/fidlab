#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info = R"(/**
 *  @brief Project: Laplace equation with Neumann BCs, No. 1
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  BCs:
 *  b1: $-\frac{\partial u}{\partial x}+2u=0$
 *  b3: $-\frac{\partial u}{\partial y}+u=-e^{x^2+1}$
 *  b2: $\frac{\partial u}{\partial x}=0$
 *  b4: $\frac{\partial u}{\partial y}=0$
 *
 *  PDE:
 *  $u_{xx}+u_{yy}=4(x^2+y^2+1)e^{x^2+y^2}$
 *
 *  Exact solution:
 *  $u=e^{x^2+y^2}$
 *
 *  Linear solver: GSL LU
 */
)";

int main()
{
  xBoundary b1(1., 0., 1.);
  xBoundary b2(0., 1., 0.);
  // The next two boundaries raise an exception because they
  // are impropeply oriented:
  #if 0
  yBoundary b3(1., 0., 1.);
  yBoundary b4(0., 1., 0.);
  #endif // 0
  yBoundary b3(1., 1., 0.);
  yBoundary b4(0., 0., 1.);

  try
  {
    Mesh mesh(50, 50);
    mesh.set_boundaries({b1,b2,b3,b4});

    NeumannBC bc1(b1, "-1", "2", "");
    NeumannBC bc2(b2, "1", "", "");
    NeumannBC bc3(b3, "-1", "1", "cy");
    NeumannBC bc4(b4, "1", "0", "");

    // Part 1: declaration of fields and functions
    Field u;
    Function cy("cy"), xs("exact_solution"), f("f");

    cy.set_implementation("-exp(x*x+1)");
    xs.set_implementation("exp(x*x+y*y)");
    f.set_implementation("4*(x*x+y*y+1)*exp(x*x+y*y)");

    // Part 2: declaration of PDE
    PDE pde(&mesh);
    pde.name = "PDE2d";
    pde.set_boundary_discretization_method(/*PDE::INWARD_DIFF3*/PDE::SYM_DIFF2);

    // Part 3: set fields, BCs and functions
    pde.set_field(u);

    pde.set_BC(bc1);
    pde.set_BC(bc2, u);
    pde.set_BC(bc3);
    pde.set_BC(bc4, u);

    pde.set_functions({cy, f, xs});

    // Part 4: PDE definition
    pde = Dxx[u] + Dyy[u] - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set code generation methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");

    // Part 6: output
    pde.executable = "gsl2d_neu_1";
    pde.project_info = project_info;
    pde.compiler = "clang++";
    pde.set_output("code/gsl2d_neu_1");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
