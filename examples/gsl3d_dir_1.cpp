#include <iostream>
#include <fidlab/pde3d.hpp>
#include "range.hpp"

using namespace fd;
using namespace fd3;
using std::cout;

const char *project_info =
R"(//!	Project: Laplace equation with Dirichlet BCs, No. 1
//!
//! BCs:
//! $u=u_exact$ on all boundaries
//!
//! PDE:
//!	$u_{xx}+u_{yy}+u_{zz}=-3sin(x+y+z)$
//!
//!	Exact solution:
//!	$u=sin(x+y+z)$
//!
//! Boundary discretization method: -
//! Linear solver: GSL LU
)";

int main()
{
	try
	{
    xBoundary b0(6, {  0, .5, 2.8,   3});
    xBoundary b1(6, {-.8, .7,   2, 2.5});
    xBoundary b2(6, { -1,  1,   2,   3});
    xBoundary b3(1, { -1,  1,   2,   3});
    yBoundary b4(1, {  1,  6,   2,   3});
    yBoundary b5(-1,{  1,  6,   2,   3});
    zBoundary b6(3, {  4,  6, -.5,  .3});
    zBoundary b7(3, {  1,  6,  -1,   1});
    zBoundary b8(2, {  1,  6,  -1,   1});

    Mesh mesh(13, 13, 13);
    mesh.set_boundaries({b0, b1, b2, b3, b4, b5, b6, b7, b8});

		DirichletBC bc0(b0, "g");
		DirichletBC bc1(b1, "g");
		DirichletBC bc2(b2, "g");
		DirichletBC bc3(b3, "g");
		DirichletBC bc4(b4, "g");
		DirichletBC bc5(b5, "g");
		DirichletBC bc6(b6, "g");
		DirichletBC bc7(b7, "g");
		DirichletBC bc8(b8, "g");

		// Part 1: declaration of fields and functions
		Field u("u");
		Function f("f"), uex("exact_solution"), g("g");

		// Part 2: declaration and customization of PDE
		PDE pde(mesh);
		pde.name = "PDE3d";

		// Part 3: set PDE fields, BCs and functions
		pde.set_field(u);
		pde.set_functions({f, g, uex});
		pde.set_BCs({bc0, bc1, bc2, bc3, bc4, bc5, bc6, bc7, bc8});

		f.set_implementation(" {return -3*sin(x+y+z);}");
		g.set_implementation("exact_solution(x,y,z)");
		uex.set_implementation("sin(x+y+z)");

		// Part 4: set PDE equations
		pde("Dirichlet") = Dxx[u] + Dyy[u] + Dzz[u] - f;

		std::cout << "PDE";
		std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

		// Part 5: set methods
		pde.set_library("gsl");
		pde.set_matrix_method("LU");
//		pde.auto_system_config = true;

		// Part 6: output
		pde.executable = "gsl3d_dir_1";
//		pde.compiler = "clang++";
		pde.project_info = project_info;
		pde.set_output("code/gsl3d_dir_1");
		pde.generate_code();
		pde.generate_makefile();
	}
	catch (const std::string& msg)
	{
		std::cout << msg << std::endl;
		return 1;
	}
	catch (const char *msg)
	{
		std::cout << "Caught const char* exception!\n";
		std::cout << msg << std::endl;
		return 1;
	}

	return 0;
}
