#include <iostream>
#include <fidlab/pde3d.hpp>
#include "range.hpp"

using namespace fd;
using namespace fd3;
using std::cout;

const char *project_info =
R"(/**
 *  Project: Laplace equation with Custom BCs, No. 0
 *
 *  BCs:
 *  $u=u_exact$ on all boundaries
 *
 *  PDE:
 *  $u_{xx}+u_{yy}+u_{zz}=-3sin(x+y+z)$
 *
 *  Exact solution:
 *  $u=sin(x+y+z)$
 *
 *  Boundary discretization method: -
 *  Linear solver: GSL LU
 */
)";

int main()
{
  try
  {
    xBoundary b0(6, {  0, .5, 2.8,   3});
    xBoundary b1(6, {-.8, .7,   2, 2.5});
    zBoundary b6(3, {  4,  6, -.5,  .3});

    Mesh mesh({1, 6, -1, 1, 2, 3}, 29, 15, 13);
    mesh.set_boundaries({b0, b1, b6});

    // Part 1: declaration of fields and functions
    Field u("u");
    Function f("f"), uex("exact_solution"), g("g"), h("h");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE3d";

    // Part 3: set PDE fields, BCs and functions
    CustomBC bcg(mesh.get_global_boundary());
    CustomBC bc0(b0), bc1(b1), bc6(b6);
		bcg = u - g;
		bc0 = u - h;
		bc1 = u - h;
		bc6 = u - h;

    pde.set_field(u);
    pde.set_functions({f, g, h, uex});
//    pde.set_BCs({bc0, bc1, bc2, bc3, bc4, bc5, bc6, bc7, bc8});
    pde.set_BCs({bc0, bc1, bcg, bc6});

    f.set_implementation(" {return -3*sin(x+y+z);}");
    g.set_implementation("exact_solution(x,y,z)");
    h.set_implementation("exact_solution(x,y,z)");
    uex.set_implementation("sin(x+y+z)");

    // Part 4: set PDE equations
    pde("Dirichlet") = Dxx[u] + Dyy[u] + Dzz[u] - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");
//    pde.auto_system_config = true;

    // Part 6: output
    pde.executable = "gsl3d_cus_0";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl3d_cus_0");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
