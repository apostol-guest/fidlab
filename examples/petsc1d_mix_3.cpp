#include <iostream>
#include <fidlab/pde1d.hpp>

using namespace fd;
using namespace fd1;

const char *project_info =
R"(/**
 *  Project: Linear PDE-1d with mixed Dirichlet-Neumann BCs, No. 3
 *
 *  Similar to no. 3 but without custom BasicOperator.
 *
 *     --+----------+-----> x
 *       0          1
 *      [b0]       [b1]
 *
 *  Custom BasicOperator
 *  $L[u]=\frac{d}{dx}\left(g(x)\frac{du}{dx}\right)$
 *
 *  PDE:
 *  $L[u}+a(x)u=f(x)$
 *  where:
 *  $a(x)=[2\lambda\cos\lambda x + b(x)]\sin\lambda x$
 *  $b(x)=e^x$
 *  $f(x)=b(x)\sin\lambda x$
 *  $g(x)=\cos\lambda x$
 *  $\lambda=\frac{\pi}{2}$
 *
 *  BCs:
 *  b0: $\frac{du}{dx}=u_x=\lambda$
 *  b1: $u=1$
 *
 *  Exact solution:
 *  $u(x)=\sin\lambda x$
 *
 *  Boundary discretization method: not relevant
 *  Linear solver: PETSc specified from command line.
 */
)";

int main()
{
  try
  {
    Boundary b0(0.);
    Boundary b1(1.);

    Mesh mesh(100);
    mesh.set_boundaries({b0, b1});

    NeumannBC bc0(b0, "1", "", "bc0");
    DirichletBC bc1(b1, "bc1");

    // Part 1: declaration of fields and functions
    Field u("u");
    Function f("f"), g("g"), gp("gp");
    Function a("fa"), b("fb"), xs("exact_solution"), f0("bc0"), f1("bc1");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE1d";
    pde.set_boundary_discretization_method(PDE::/*SYM_DIFF2*/INWARD_DIFF3);

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_BCs({bc1, bc0}, u);
    pde.set_functions({f, g, gp, a, b, xs, f0, f1});

    a.set_implementation("M_PI*M_PI_2*cos(M_PI_2*x)+fb(x)");
    b.set_implementation("exp(x)");
    f.set_implementation("fb(x)*sin(M_PI_2*x)");
    g.set_implementation("cos(M_PI_2*x)");
    gp.set_implementation("-M_PI_2*sin(M_PI_2*x)");
    xs.set_implementation("sin(M_PI_2*x)");
    f0.set_implementation("M_PI_2");
    f1.set_implementation("1");

    // Part 4: set PDE equations
    pde = g*Dxx[u] + /*Dx[g]*/gp*Dx[u] + a*u - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc1d_mix_3";
    // pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/petsc1d_mix_3");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
