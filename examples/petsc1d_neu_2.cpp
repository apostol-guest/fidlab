#include <iostream>
#include <fidlab/pde1d.hpp>

using namespace fd;
using namespace fd1;

const char *project_info =
R"(/**
 *  Project: Linear PDE-1d with Neumann BCs, No. 2
 *
 *  Features user specified equation replacement.
 *
 *     --+----------+-----> x
 *       0          1
 *      [b0]       [b1]
 *
 *  PDE:
 *  $u_{xx}+a(x)u_x=f(x)$
 *  where:
 *  $a(x)=-\pi$
 *  $f(x)=\lambda\left[ a\sin\lambda x + (a+2\lambda)\cos\lambda x \right]e^{\lambda x}$
 *  $\lambda=\frac{\pi}{2}$
 *
 *  BCs:
 *  b0: $\frac{du}{dx}=u_x=\lambda$
 *  b1: $\frac{du}{dx}=u_x=\frac{\pi}{2}e^{\frac{\pi}{2}}$
 *
 *  Exact solution:
 *  $u(x)=e^{\lambda x}\sin\lambda x$
 *
 *  Boundary discretization method: INWARD_DIFF3
 *  Linear solver: PETSc specified from command line.
 */
)";

int main()
{
  try
  {
    Boundary b0(0.);
    Boundary b1(1.);

    Mesh mesh(100);
    mesh.set_boundaries({b0, b1});

    NeumannBC bc0(b0, "1", "", "bc0");
    NeumannBC bc1(b1, "1", "", "bc1");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f");
    Function a("fa"), k("fk"), xs("exact_solution"), fbc0("bc0"), fbc1("bc1");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE1d";
    pde.set_boundary_discretization_method(PDE::SYM_DIFF2/*INWARD_DIFF3*/);

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_BCs({bc1, bc0}, u);
    pde.set_functions({f, a, xs, fbc0, fbc1, k});

    a.set_implementation("x*exp(2.*x)-M_PI"); // change as desired
    f.set_implementation("M_PI_2*(fa(x)*sin(M_PI_2*x)+(fa(x)+M_PI)*cos(M_PI_2*x))*exp(M_PI_2*x)");
    xs.set_implementation("exp(M_PI_2*x)*sin(M_PI_2*x)");
    k.set_implementation("-M_PI_2");
    fbc0.set_implementation("M_PI_2");
    fbc1.set_implementation("M_PI_2*exp(M_PI_2)");

    // Part 4: set PDE equations
    pde = Dxx[u] + a*Dx[u] - f;

    // If you replace 0 by n, the computed solution will differ from
    // the exact_solution by a constant.
    pde.replace_equation(u, 0/*"u[e(0)]", "du[e(0)]"*/);

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc1d_neu_2";
    // pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/petsc1d_neu_2");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
