#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info =
R"(/**
 *  Project: Laplace PDE with custom BCs, No. 0
 *  This project is identical to Project Dirichlet 1.
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  BCs:
 *  b1: $u=u_exact$
 *  b3: $u=u_exact$
 *  b2: $u=u_exact$
 *  b4: $u=u_exact$
 *
 *  PDE:
 *  $v_{xx}+v_{yy}=-2\sin(x+y)$
 *
 *  Exact solution:
 *  $v=\sin(x+y)$
 *
 *  Boundary discretization method: does not apply
 *  Linear solver: PETSC
 */
)";

int main()
{
  try
  {
    xBoundary b1(1., 0., 1.);
    xBoundary b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.);
    yBoundary b4(0., 0., 1.);

    Mesh mesh(50, 50);
    mesh.set_boundaries({b3, b2, b4, b1});

    // Part 0: declaration of fields and functions
    Field u("u");
    Function f("f");
    Function xs("exact_solution"), bc("bc");

    // Part 1: declaration and definition of BCs
    CustomBC bc1(b1), bc2(b2), bc3(b3), bc4(b4);
    bc1 = u - bc;
    bc2 = u - bc;
    bc3 = u - bc;
    bc4 = u - bc;

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE2d";

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_functions({f, bc, xs});
    pde.set_BCs({bc1, bc2, bc3, bc4}, u);

    f.set_implementation("-2*sin(x+y)");
    bc.set_implementation("exact_solution(x,y)");
    xs.set_implementation("sin(x+y)");

    // Part 4: set PDE equations
    pde = Dxx[u] + Dyy[u] - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc2d_cus_0";
    pde.project_info = project_info;
    pde.compiler = "clang++";
    pde.set_output("code/petsc2d_cus_0");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
