#include <iostream>
#include <fidlab/pde1d.hpp>

using namespace fd;
using namespace fd1;

const char *project_info =
R"(/**
 *  Project: Nonlinear PDE-1d with Dirichlet BCs, No. 5
 *
 *  Features
 *  + Field functions with space dependence.
 *
 *     --+----------+-----> x
 *       0          1
 *      [b0]       [b1]
 *
 *  PDE:
 *  $
 *  \begin{matrix}
 *  u_1^{\prime\prime}-2u_1^{\prime}-3u_1-u_3^{\prime\prime}-5u_3^{\prime}-6u_3+u_1(u_2-e^{2x})+u_3=e^{-3x}\\
 *  u_2^{\prime\prime}-3u_2^{\prime}+2u_2+2u_1^{\prime\prime}-4u_1^{\prime}-6u_1+3u_2u_3=3e^{-x}\\
 *  u_3^{\prime\prime}+5u_3^{\prime}+6u_3+u_1u_2^2u_3=1\\
 *  6u_4^{\prime\prime}+u_4^{\prime}-2u_4+3u_1^{\prime\prime}-6u_1^{\prime}-9u_1+u_1u_2^\prime=2e^{x}
 *  \end{matrix}
 *  $
 *
 *  BCs:
 *  b0: $0$
 *  b1: $1$
 *
 *  Exact solution:
 *  $u_1(x)=e^{-x}$
 *  $u_2(x)=e^{2x}$
 *  $u_3(x)=e^{-3x}$
 *  $u_4(x)=e^{\frac{1}{2}x}$
 *
 *  Boundary discretization method: does not apply (INWARD_DIFF3)
 *  Linear solver: PETSc specified from command line.
 *
 *  Run with: mpiexec -n 4 ./petsc1d_nl_4 -snes_max_it 500 -snes_stol 1e-22
 *    -snes_monitor -show_converged_reason
 *
)";

int main()
{
  try
  {
    // Create mesh
    Mesh mesh(50);
    Boundary b0(0.), b1(1.);
    mesh.set_boundaries({b0, b1});

    DirichletBC bc0[4]{DirichletBC(b0, "1"), DirichletBC(b0, "1"),
                       DirichletBC(b0, "1"), DirichletBC(b0, "1")};
    DirichletBC bc1[4]{DirichletBC(b1,"U1"), DirichletBC(b1,"V1"),
                       DirichletBC(b1,"W1"), DirichletBC(b1,"Z1")};

    // Part 1: declaration of fields and functions
    Field u("u"), v("v"), w("w"), z("z");
    Function f0("f0"), f1("f1"), f2("f2"), f3("f3"), g("g");
    Function U1("U1"), V1("V1"), W1("W1"), Z1("Z1");
    FieldFunction G("G", 1, true);
    Function xs("exact_solution");
//    Operator L("L");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE1d";
    pde.set_boundary_discretization_method(PDE::/*SYM_DIFF2*/INWARD_DIFF3);

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v, w, z});
    pde.set_BCs({bc0[0], bc1[0]}, u);
    pde.set_BCs({bc0[1], bc1[1]}, v);
    pde.set_BCs({bc0[2], bc1[2]}, w);
    pde.set_BCs({bc0[3], bc1[3]}, z);
    pde.set_functions({f0, f1, f2, f3, g, U1, V1, W1, Z1, xs});
    pde.set_functions({G});
//    pde.set_initial_estimate("c == 0 ? x : 1");
    pde.use_field_names = true;

    xs.set_implementation(R"(
	{
		switch (c)
		{
			case 0: return exp(-x);
			case 1: return exp(2*x);
			case 2: return exp(-3*x);
			case 3: return exp(.5*x);
		}
		throw std::string(__PRETTY_FUNCTION__)+": field index out of bounds.";
		return 0;
	})");
    f0.set_implementation("exp(-3*x)");
    f1.set_implementation("3*exp(-x)");
    f2.set_implementation("1");
    f3.set_implementation("2*exp(x)");
    U1.set_implementation("exp(-1)");
    V1.set_implementation("exp(2)");
    W1.set_implementation("exp(-3)");
    Z1.set_implementation("exp(.5)");
    g.set_implementation("exp(2*x)");

    G.set_implementation({"u-exp(2*x)", /* G_u */ "1"});

    // Part 4: set PDE equations
//    pde("u") = Dxx[u]-2*Dx[u]-3*u-Dxx[w]-5*Dx[w]-5*w+u*v-g*u-f0;
    pde("u") = Dxx[u]-2*Dx[u]-3*u-Dxx[w]-5*Dx[w]-5*w+u*G(v)-f0;
    pde("v") = Dxx[v]-3*Dx[v]+2*v+2*Dxx[u]-4*Dx[u]-6*u+3*v*w-f1;
    pde("w") = Dxx[w]+5*Dx[w]+6*w+u*v*v*w-1;
    pde("z") = 6*Dxx[z]+Dx[z]-2*z+3*Dxx[u]-6*Dx[u]-9*u+u*Dx[v]-f3;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");
    pde.set_nonlinear_solver("newton");

    // Part 6: output
    pde.executable = "gsl1d_nl_5";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl1d_nl_5");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
