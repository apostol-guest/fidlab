#ifndef DXFDX1D_HPP_INCLUDED
#define DXFDX1D_HPP_INCLUDED

#include <fidlab/discretizer.hpp>

namespace fd1
{

using fd::Discretizer; using fd::CodeFragment; using fd::DiscretizationData;


/**
 *  Discretizer for operator DxfDx = $\frac{d}{dx}\left(f\frac{d}{dx}\right)$
 *
 *  Extend Discretizer class in order to define the new discretization
 *  method.
 */
class DiscretizerDxfDx : public Discretizer
{
  public:
  std::string fname;

  DiscretizerDxfDx() {order_[0] = 2;}
  DiscretizerDxfDx(const std::string& name) : fname(name) {order_[0] = 2;}

  DECL_DISCRETIZER_DUP();
  virtual CodeFragment discretize(const DiscretizationData& d) const override;

  SET_OPERATOR_NAME("DxfDx");
};


} // namespace fd1

#endif // DXFDX1D_HPP_INCLUDED
