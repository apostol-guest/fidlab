#include <iostream>
#include "pdexd3d.hpp"

using namespace fd;
using namespace extdom::d3;
using std::cout;

const char *project_info = R"(/**
 *  @brief Project: Laplace equation with Neumann BCs, No. 0
 *
 *  This project uses extended domains.
 *
 *  BCs:
 *  b1: $-\frac{\partial u}{\partial x}+2u=2y^2$
 *  b3: $-\frac{\partial u}{\partial y}+u=x^2-1$
 *  b2: $\frac{\partial u}{\partial y}=0$
 *  b4: $\frac{\partial u}{\partial y}=0$
 *
 *  PDE:
 * 	$u_{xx}+u_{yy}+u_{zz}=6$
 *
 * 	Exact solution:
 * 	$u=x^2+y^2+z^2$
 *
 *  Boundary discretization method: extended domain (SYM_DIFF2)
 *  Linear solver: GSL LU
 */
)";

int main()
{
	try
	{
		fd3::xBoundary b1(.6, {-.1, .1, .2, .3});
		fd3::xBoundary b2(.1, {-.1, .1, .2, .3});
		fd3::yBoundary b3(-.1,{ .1, .6, .2, .3});
		fd3::yBoundary b4(.1, { .1, .6, .2, .3});
		fd3::zBoundary b5(.2, { .1, .6,-.1, .1});
		fd3::zBoundary b6(.3, { .1, .6,-.1, .1});

		fd3::Mesh mesh(15, 15, 15);
		mesh.set_boundaries({b3, b4, b2, b1, b5, b6});

		NeumannBC bc1(b1, "-1", "2", "cx");
		NeumannBC bc2(b2, "1", "", ".2");
		NeumannBC bc3(b3, "1", "0", "-.2");
		NeumannBC bc4(b4, "-1", "1", "cy");
		NeumannBC bc5(b5, "1", "0", ".4");
		NeumannBC bc6(b6, "az", "2", "cz");

		// Part 1: declaration of fields and functions
		Field u("u");
		Function cx("cx"), cy("cy"), cz("cz"), az("az"), xs("exact_solution");

		// Part 2: declaration and customization of PDE
		PDE pde(mesh);
		pde.name = "PDE3d";

		// Part 3: set PDE fields, BCs and functions
		pde.set_field(u);
		pde.set_functions({cx, cy, az, cz, xs});
		pde.set_BCs({bc1, bc2, bc3, bc4, bc5, bc6});

		xs.set_implementation("x*x+y*y+z*z");
		cx.set_implementation("-2*x+2*exact_solution(x,y,z)");
		cy.set_implementation("-2*y+exact_solution(x,y,z)");
		cz.set_implementation("2*(x*x+y*y)");
		az.set_implementation("-z");

		// Part 4: set PDE equations
		pde("Neumann") = Dxx[u] + Dyy[u] + Dzz[u] - 6;

		std::cout << "PDE";
		std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

		// Part 5: set code generation methods
		pde.set_library("gsl");
		pde.set_matrix_method("LU");
//		pde.auto_system_config = true;

		// Part 6: output
		pde.executable = "gsl3d_nxd_0";
		pde.project_info = project_info;
		// pde.compiler = "clang++";
		pde.set_output("code/gsl3d_nxd_0");
		pde.generate_code();
		pde.generate_makefile();
	}
	catch (const std::string& msg)
	{
		std::cout << msg << std::endl;
		return 1;
	}
	catch (const char *msg)
	{
		std::cout << "Caught const char* exception!\n";
		std::cout << msg << std::endl;
		return 1;
	}

	return 0;
}
