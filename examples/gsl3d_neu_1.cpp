#include <iostream>
#include <fidlab/pde3d.hpp>
#include "range.hpp"

using namespace fd;
using namespace fd3;
using std::cout;

const char *project_info = R"(/**
 *	@brief Project: Laplace equation with Neumann BCs, No. 1
 *
 *  BCs:
 *  $\frac{\partial u}{\partial x_i}+u=2x_i+u_{exact}(x,y,z)$
 *
 *  PDE:
 * 	$u_{xx}+u_{yy}+u_{zz}=6$
 *
 * 	Exact solution:
 * 	$u=x^2+y^2+z^2$
 *
 *  Boundary discretization method: inward differencing scheme (INWARD_DIFFx)
 *  Linear solver: GSL LU
 */
)";

int main()
{
	try
	{
		Mesh mesh({.1,.6,-.1,.1,.2,.3}, 15);
		NeumannBC bcg(mesh.get_global_boundary(), "1", "1", "cg");

		// Part 1: declaration of fields and functions
		Field u("u");
		Function cg("cg"), xs("exact_solution");

		// Part 2: declaration and customization of PDE
		PDE pde(mesh);
		pde.name = "PDE3d";

		// Part 3: set PDE fields, BCs and functions
		pde.set_field(u);
		pde.set_functions({cg, xs});
		pde.set_BCs({bcg});

		xs.set_implementation("x*x+y*y+z*z");
		cg.set_implementation(R"(
  {
    double r;
    if (fabs(x-a) < RESOL[0] || fabs(x-b) < RESOL[0]) r = x;
    else if (fabs(y-c) < RESOL[1] || fabs(y-d) < RESOL[1]) r = y;
    else r = z;
    return 2*r+exact_solution(x,y,z);
  })");

		// Part 4: set PDE equations
		pde("Neumann") = Dxx[u] + Dyy[u] + Dzz[u] - 6;

		std::cout << "PDE";
		std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

		// Part 5: set code generation methods
		pde.set_library("gsl");
		pde.set_matrix_method("LU");
//		pde.auto_system_config = true;

		// Part 6: output
		pde.executable = "gsl3d_neu_1";
		pde.project_info = project_info;
		// pde.compiler = "clang++";
		pde.set_output("code/gsl3d_neu_1");
		pde.generate_code();
		pde.generate_makefile();
	}
	catch (const std::string& msg)
	{
		std::cout << msg << std::endl;
		return 1;
	}
	catch (const char *msg)
	{
		std::cout << "Caught const char* exception!\n";
		std::cout << msg << std::endl;
		return 1;
	}

	return 0;
}
