#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info =
R"(/**
 * Project: Nonlinear PDE-2d with Dirichlet BCs, No. 3
 *
 * Features
 * + Field functions with space dependence.
 *
 *    y ^
 *      |
 *      |  [1] b3
 *    1 +----------+
 *      |          |
 *   [2]|          |[0]
 *   b2 |          | b1
 *      |          |
 *    0 +----------+-----> x
 *      0  [3] b4  1
 *
 * PDE:
 * $
 * \begin{matrix}
 * \Delta u_1-2D_x u_1-4u_1-\Delta u_3-5D_y u_3-6u_3+u_1(u_2-e^{2x+y})+u_3=e^{-3y}\\
 * \Delta u_2-3D_x u_2-D_y u_2+2u_2+2\Delta u_1-4D_x u_1-8u_1+3u_2u_3=3e^{2(x-y)}\\
 * \Delta u_3+5D_y u_3+6u_3+u_1^2u_2u_3=1\\
 * 6\Delta u_4-3D_x u_4+4D_y u_4-2u_4+3\Delta u_1-6D_x u_1-12u_1+u_1^2D_y u_2=e^{3y}
 * \end{matrix}
 * $
 *
 * BCs:
 * b0: $0$
 * b1: $1$
 *
 * Exact solution:
 * $u_1(x)=e^{-x+y}$
 * $u_2(x)=e^{2x+y}$
 * $u_3(x)=e^{-3y}$
 * $u_4(x)=e^{\frac{1}{2}x-y}$
 *
 * Boundary discretization method: INWARD_DIFFx
 * Linear solver: GSL Newton
 */
)";

int main()
{
  try
  {
    // Create mesh
    xBoundary b1(1., 0., 1.), b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.), b4(0., 0., 1.);
    Mesh mesh(30, 30);
    mesh.set_boundaries({b1, b2, b3, b4});

    DirichletBC bc1[4]{DirichletBC(b1,"U1"), DirichletBC(b1,"V1"),
                       DirichletBC(b1,"W1"), DirichletBC(b1,"Z1")};
    DirichletBC bc2[4]{DirichletBC(b2,"U2"), DirichletBC(b2,"V2"),
                       DirichletBC(b2,"W2"), DirichletBC(b2,"Z2")};
    DirichletBC bc3[4]{DirichletBC(b3,"U3"), DirichletBC(b3,"V3"),
                       DirichletBC(b3,"W3"), DirichletBC(b3,"Z3")};
    DirichletBC bc4[4]{DirichletBC(b4,"U4"), DirichletBC(b4,"V4"),
                       DirichletBC(b4,"W4"), DirichletBC(b4,"Z4")};

    // Part 1: declaration of fields and functions
    Field u("u"), v("v"), w("w"), z("z");
    Function f0("f0"), f1("f1"), f2("f2"), f3("f3"), g("g");
    Function U1("U1"), V1("V1"), W1("W1"), Z1("Z1");
    Function U2("U2"), V2("V2"), W2("W2"), Z2("Z2");
    Function U3("U3"), V3("V3"), W3("W3"), Z3("Z3");
    Function U4("U4"), V4("V4"), W4("W4"), Z4("Z4");
    FieldFunction G("G", 1, true);
    Function xs("exact_solution");
    Operator D("Laplace");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE2d";
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4);

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v, w, z});
    pde.set_BCs({bc1[0], bc2[0], bc3[0], bc4[0]}, u);
    pde.set_BCs({bc1[1], bc2[1], bc3[1], bc4[1]}, v);
    pde.set_BCs({bc1[2], bc2[2], bc3[2], bc4[2]}, w);
    pde.set_BCs({bc1[3], bc2[3], bc3[3], bc4[3]}, z);
    pde.set_functions({f0, f1, f2, f3, g, xs});
    pde.set_functions({U1, V1, W1, Z1, U2, V2, W2, Z2});
    pde.set_functions({U3, V3, W3, Z3, U4, V4, W4, Z4});
    pde.set_functions({G});
//    pde.set_initial_estimate("c == 0 ? x : 1");
    pde.use_field_names = true;

    xs.set_implementation(R"(
	{
		switch (c)
		{
			case 0: return exp(-x+y);
			case 1: return exp(2*x+y);
			case 2: return exp(-3*y);
			case 3: return exp(.5*x-y);
		}
		throw std::string(__PRETTY_FUNCTION__)+": field index out of bounds.";
		return 0;
	})");
    g.set_implementation("exp(2*x+y)");
    f0.set_implementation("exp(-3*y)");
    f1.set_implementation("3*exp(2*(x-y))");
    f2.set_implementation("1");
    f3.set_implementation("exp(3*y)");
    U1.set_implementation("exp(-1+y)");
    V1.set_implementation("exp(2+y)");
    W1.set_implementation("exp(-3*y)");
    Z1.set_implementation("exp(.5-y)");
    U2.set_implementation("exp(y)");
    V2.set_implementation("exp(y)");
    W2.set_implementation("exp(-3*y)");
    Z2.set_implementation("exp(-y)");
    U3.set_implementation("exp(-x+1)");
    V3.set_implementation("exp(2*x+1)");
    W3.set_implementation("exp(-3)");
    Z3.set_implementation("exp(.5*x-1)");
    U4.set_implementation("exp(-x)");
    V4.set_implementation("exp(2*x)");
    W4.set_implementation("1");
    Z4.set_implementation("exp(.5*x)");

    G.set_implementation({"u-exp(2*x+y)", /* G_u */ "1"});

    D(u) = Dxx[u] + Dyy[u];

    // Part 4: set PDE equations
//    pde("u") = D[u]-2*Dx[u]-4*u-D[w]-5*Dy[w]-6*w+u*v-g*u-f0;
    pde("u") = D[u]-2*Dx[u]-4*u-D[w]-5*Dy[w]-6*w+u*G(v)-f0;
    pde("v") = D[v]-3*Dx[v]-Dy[v]+2*v+2*D[u]-4*Dx[u]-8*u+3*v*w-f1;
    pde("w") = D[w]+5*Dy[w]+6*w+u*u*v*w-1;
    pde("z") = 6*D[z]-3*Dx[z]+4*Dy[z]-2*z+3*D[u]-6*Dx[u]-12*u+u*u*Dy[v]-f3;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");
    pde.set_nonlinear_solver("newton");

    // Part 6: output
    pde.executable = "gsl2d_nl_3";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl2d_nl_3");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
