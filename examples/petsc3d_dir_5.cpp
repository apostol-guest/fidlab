#include <iostream>
#include <fidlab/pde3d.hpp>

using namespace fd;
using namespace fd3;

const char *project_info =
R"(/**
 *  Project: Laplace 2-equation system with Dirichlet BCs, No. 5
 * 
 *  BC: $u=u_exact, v=v_exact$
 * 
 *  PDE:
 *  \begin{align*}
 *  & 2(u_{xx}+u_{yy}+u_{zz})-(v_{xx}+v_{yy}+v_{zz})=2[6\lambda+4\lambda^2(x^2+y^2+z^2)]e^{\lambda(x^2+y^2+z^2)}+3\sin(x+y+z) \\
 *  & u_{xx}+u_{yy}+u_{zz}+v_{xx}+v_{yy}+v_{zz}=[6\lambda+4\lambda^2(x^2+y^2+z^2)]e^{\lambda(x^2+y^2+z^2)}-3\sin(x+y+z)
 *  \end{align*}
 * 
 * 	Exact solution:
 * 	$u=e^{\lambda(x^2+y^2+z^2)}$
 * 	$v=\sin(x+y+z)$
 * 
 *  Boundary discretization method: does not apply.
 *  Linear solver: GSL LU
 */
)";

int main()
{
	try
	{
    xBoundary b0(6, {  0, .5, 2.8,   3});
    xBoundary b1(6, {-.8, .7,   2, 2.5});
//    xBoundary b2(6, { -1,  1,   2,   3});
//    xBoundary b3(1, { -1,  1,   2,   3});
//    yBoundary b4(1, {  1,  6,   2,   3});
//    yBoundary b5(-1,{  1,  6,   2,   3});
    zBoundary b6(3, {  4,  6, -.5,  .3});
//    zBoundary b7(3, {  1,  6,  -1,   1});
//    zBoundary b8(2, {  1,  6,  -1,   1});

		Mesh mesh({1, 6, -1, 1, 2, 3}, 15);
//    mesh.set_boundaries({b0, b1, b2, b3, b4, b5, b6, b7, b8});
    mesh.set_boundaries({b0, b1, b6});

		DirichletBC bcug(mesh.get_global_boundary(), "bcu");
		DirichletBC bcvg(mesh.get_global_boundary(), "bcv");
		DirichletBC bcu0(b0, "bcu"), bcv0(b0, "bcv");
		DirichletBC bcu1(b1, "bcu"), bcv1(b1, "bcv");
//		DirichletBC bc2(b2, "g");
//		DirichletBC bc3(b3, "g");
//		DirichletBC bc4(b4, "g");
//		DirichletBC bc5(b5, "g");
		DirichletBC bcu6(b6, "bcu"), bcv6(b6, "bcv");
//		DirichletBC bc7(b7, "g");
//		DirichletBC bc8(b8, "g");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f"), g("g"), bcu("bcu"), bcv("bcv");
    Function xs("exact_solution"), uex("u_exact"), vex("v_exact");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE3d";

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v});
    pde.set_BCs({bcu1, bcu0, bcu6, bcug}, u);
    pde.set_BCs({bcv1, bcv0, bcv6, bcvg}, v);
    pde.set_functions({f, g, bcu, bcv, uex, vex, xs});
    f.set_implementation("(1./6.+(x*x+y*y+z*z)/324)*exp((x*x+y*y+z*z)/36)");
    g.set_implementation("-3*sin(x+y+z)");
    bcu.set_implementation("u_exact(x,y,z)");
    bcv.set_implementation("v_exact(x,y,z)");
    uex.set_implementation("exp((x*x+y*y+z*z)/36)");
    vex.set_implementation("sin(x+y+z)");
    xs.set_implementation("c == 0 ? u_exact(x,y,z) : v_exact(x,y,z)");

    // Part 4: set PDE equations
    Operator Delta("Delta");
    Delta(v) = Dxx[v]+Dyy[v]+Dzz[v];
//    pde("D1") = 2*(Dxx[u] + Dyy[u] + Dzz[u])-(Dxx[v] + Dyy[v] + Dzz[v]) - (2*f-g);
//    pde("D2") = Dxx[u] + Dyy[u] + Dzz[u] + Dxx[v] + Dyy[v] + Dzz[v] - f - g;
    pde("D1") = 2*Delta[u] - Delta[v] - (2*f-g);
    pde("D2") = Delta[u] + Delta[v] - f - g;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc3d_dir_5";
    pde.project_info = project_info;
//    pde.compiler = "clang++";
    pde.set_output("code/petsc3d_dir_5");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
