#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info = R"(/**
 *  @brief Project: Laplace equation with Dirichlet BCs, No. 0
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  BCs:
 *  b1: $u=f1=u_exact$
 *  b3: $u=f3=u_exact$
 *  b2: $u=f2=u_exact$
 *  b4: $u=f4=u_exact$
 *
 *  PDE:
 *  $u_{xx}+u_{yy}=4(x^2+y^2+1)e^{x^2+y^2}$
 *
 *  Exact solution:
 *  $u=e^{x^2+y^2}$
 *
 *  Boundary discretization method: inward 4-point differencing-scheme (INWARD_DIFF4)
 *  Linear solver: GSL LU
 */
)";

int main()
{
  try
  {
    xBoundary b1(1., 0., 1.);
    xBoundary b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.);
    yBoundary b4(0., 0., 1.);

    Mesh mesh(50, 50);
    mesh.set_boundaries({b3, b2, b4 ,b1});

    DirichletBC bc1(b1, "f1");
    DirichletBC bc2(b2, "f2");
    DirichletBC bc3(b3, "f3");
    DirichletBC bc4(b4, "f4");

    // Part 1: declaration of fields and functions
    Field u("u");
    Function f("f"), uex("exact_solution"), f1("f1"), f2("f2"), f3("f3"), f4("f4");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE2d";

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
//    pde.set_BC(bc1);
    pde.set_BC(bc2);
//    pde.set_BC(bc3, u);
//    pde.set_BC(bc4, u);
    pde.set_BCs({bc1, /*bc2, */bc3, bc4});
    pde.set_functions({f, f1, f2, f3, f4, uex});
    #if 0
    // This is the old way
    pde.get_function("f").implementation = " {return 4*(x*x+y*y+1)*exp(x*x+y*y);}";
    pde.get_function("f1").implementation = " {return exact_solution(x,y);}";
    pde.get_function("f2").implementation = " {return exact_solution(x,y);}";
    pde.get_function("f3").implementation = " {return exact_solution(x,y);}";
    pde.get_function("f4").implementation = " {return exact_solution(x,y);}";
    pde.get_function("exact_solution").implementation = " {return exp(x*x+y*y);}";
    #else
    f.set_implementation(" {return 4*(x*x+y*y+1)*exp(x*x+y*y);}");
    f1.set_implementation("exact_solution(x,y)");
    f2.set_implementation("exact_solution(x,y)");
    f3.set_implementation("exact_solution(x,y)");
    f4.set_implementation("exact_solution(x,y)");
    uex.set_implementation("exp(x*x+y*y)");
    #endif // 0

    // Part 4: set PDE equations
    pde("Dirichlet") = Dxx[u] + Dyy[u] - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");
//    pde.auto_system_config = true;

    // Part 6: output
    pde.executable = "gsl2d_dir_0";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl2d_dir_0");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
