#include <iostream>
#include <fidlab/pde3d.hpp>
#include "range.hpp"

using namespace fd;
using namespace fd3;
using std::cout;

const char *project_info =
R"(//!	Project: Laplace equation with Dirichlet BCs, No. 0
//!
//!    y ^
//!      |
//!      |  [1] b3
//!    1 +----------+
//!      |          |
//!   [2]|          |[0]
//!   b2 |          | b1
//!      |          |
//!    0 +----------+-----> x
//!      0  [3] b4  1
//!
//! BCs:
//! b1: $u=f1=u_exact$
//! b3: $u=f3=u_exact$
//! b2: $u=f2=u_exact$
//! b4: $u=f4=u_exact$
//!
//! PDE:
//!	$u_{xx}+u_{yy}=4(x^2+y^2+1)e^{x^2+y^2}$
//!
//!	Exact solution:
//!	$u=e^{x^2+y^2}$
//!
//! Boundary discretization method: inward 4-point differencing-scheme (INWARD_DIFF4)
//! Linear solver: GSL LU
)";

int main()
{
	try
	{
		Mesh mesh({.1, .6, -.1, .1, .2, .3}, 13, 13, 13);
		DirichletBC bc(mesh.get_global_boundary(), "g");

		// Part 1: declaration of fields and functions
		Field u("u");
		Function f("f"), uex("exact_solution"), g("g");

		// Part 2: declaration and customization of PDE
		PDE pde(mesh);
		pde.name = "PDE3d";

		// Part 3: set PDE fields, BCs and functions
		pde.set_field(u);
		pde.set_functions({f, g, uex});
		pde.set_BCs({bc});

		f.set_implementation(" {return (4*(x*x+y*y+z*z)+6)*exp(x*x+y*y+z*z);}");
		g.set_implementation("exact_solution(x,y,z)");
		uex.set_implementation("exp(x*x+y*y+z*z)");

		// Part 4: set PDE equations
		pde("Dirichlet") = Dxx[u] + Dyy[u] + Dzz[u] - f;

		std::cout << "PDE";
		std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

		// Part 5: set methods
		pde.set_library("gsl");
		pde.set_matrix_method("LU");
//		pde.auto_system_config = true;

		// Part 6: output
		pde.executable = "gsl3d_dir_2";
//		pde.compiler = "clang++";
		pde.project_info = project_info;
		pde.set_output("code/gsl3d_dir_2");
		pde.generate_code();
		pde.generate_makefile();
	}
	catch (const std::string& msg)
	{
		std::cout << msg << std::endl;
		return 1;
	}
	catch (const char *msg)
	{
		std::cout << "Caught const char* exception!\n";
		std::cout << msg << std::endl;
		return 1;
	}

	return 0;
}
