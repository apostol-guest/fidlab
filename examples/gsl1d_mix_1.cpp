#include <iostream>
#include <fidlab/pde1d.hpp>

using namespace fd;
using namespace fd1;

const char *project_info =
R"(/**
 *  Project: Linear PDE-1d with mixed Dirichlet-Neumann BCs, No. 1
 *
 *     --+----------+-----> x
 *       0          1
 *      [b0]       [b1]
 *
 *  PDE:
 *  $u_{xx}+a(x)u_x+b(x)u=f(x)$
 *  where:
 *  $a(x)=\frac{1}{\lambda}(\cos\lambda x + e^x)$
 *  $b(x)=\sin\lambda x + \lambda^2$
 *  $f(x)=1+e^x \cos\lambda x$
 *  $\lambda=\frac{\pi}{2}$
 *
 *  BCs:
 *  b0: $\frac{du}{dx}=u_x=\lambda$
 *  b1: $u=1$
 *
 *  Exact solution:
 *  $u(x)=\sin\lambda x$
 *
 *  Boundary discretization method: SYM_DIFF2, INWARD_DIFF3
 *  Linear solver: GSL all but GMRES (it does not work).
 */
)";

int main()
{
  try
  {
    Boundary b0(0.);
    Boundary b1(1.);

    Mesh mesh(100);
    mesh.set_boundaries({b0, b1});

    NeumannBC bc0(b0, "1", "", "bc0");
    DirichletBC bc1(b1, "bc1");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f");
    Function a("fa"), b("fb"), xs("exact_solution"), f0("bc0"), f1("bc1");

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE1d";
    pde.set_boundary_discretization_method(PDE::SYM_DIFF2/*INWARD_DIFF3*/);

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_BCs({bc1, bc0}, u);
    pde.set_functions({f, a, b, xs, f0, f1});

    a.set_implementation("(cos(M_PI_2*x)+exp(x))/M_PI_2");
    b.set_implementation("sin(M_PI_2*x)+M_PI_2*M_PI_2");
    f.set_implementation("1+exp(x)*cos(M_PI_2*x)");
    xs.set_implementation("sin(M_PI_2*x)");
    f0.set_implementation("M_PI_2");
    f1.set_implementation("1");

    // Part 4: set PDE equations
    pde = Dxx[u] + a*Dx[u] + b*u - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");
//    pde.set_matrix_method("QRP");

    // Part 6: output
    pde.executable = "gsl1d_mix_1";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl1d_mix_1");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
