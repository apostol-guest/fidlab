#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info =
R"(/**
 *  Project: Nonlinear PDE-2d with Dirichlet BCs, No. 2
 *
 *  Features
 *  + Custom BCs.
 *  + Nonlocal BCs.
 *  + Custom initial estimate setting.
 *  + Custom Discretizer.
 *  + Nonlocal (localized) Operator/Discretizer.
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  PDE:
 *  $-\Delta u+e^u vD_y u D_x v+u D_y v+uv+v^2=f(x,y)$
 *  $-u\Delta v+(v^2+u)D_x v+e^v u^2=g(x,y)$
 *
 *  BCs:
 *  b0: $0$
 *  b1: $1$
 *
 *  Exact solution:
 *  $u(x)=x^2+y$
 *  $v(x)=x+y$
 *
 *  Boundary discretization method: INWARD_DIFF3
 *  Linear solver: PETSc specified from command line.
 *
 *  Run with -pc_type lu
 */
)";

//namespace fd2 {
//std::string code_func_call(const std::string& f, const std::string& i,
//                                                 const std::string& j);
//}

class PtEvalDiscretizer : public Discretizer
{
public:
  std::string point;
  PtEvalDiscretizer() : point("0") {}
  PtEvalDiscretizer(const std::string& pt) : point(pt) {}

  DECL_DISCRETIZER_DUP();
  virtual CodeFragment discretize(const DiscretizationData& d) const override;
  virtual CodeFragment discretize_on_boundary(
              const BoundaryDiscretizationData& bdd) const override;
  virtual CodeFragment discretize_scalar_field(
              const DiscretizationData& d) const override;

  SET_OPERATOR_NAME("ptev-operator");
};

// TODO: API replace code_elem_func("e", i, "$c$") --> element_function(i)

CodeFragment PtEvalDiscretizer::discretize(const DiscretizationData &d) const
{
  const std::string &i = point;
  const std::string &j = d.j;
  const std::string code_y = "u["   + d.pde->code_elem_func(i, j, "") + "]";
  const std::string code_j = "du["  + d.pde->code_elem_func(i, j, "") + "]";
  return CodeFragment(code_y, code_j);
}

CodeFragment PtEvalDiscretizer::discretize_on_boundary(
    const BoundaryDiscretizationData &bdd) const
{
  CodeFragment cf = discretize({bdd.pde, bdd.i, bdd.j, bdd.k});
  cf.refers_to_boundary(bdd.bid);
  return cf;
}

// TODO: API replace code_func_call("$f$", point) --> function_call(point)

CodeFragment PtEvalDiscretizer::discretize_scalar_field(
    const DiscretizationData& d) const
{
  const std::string code_y = d.pde->code_func_call("$f$", point, d.j, "");
  const std::string code_j = "0";
  return CodeFragment(code_y, code_j);
}

IMPLEMENT_DISCRETIZER_DUP(PtEvalDiscretizer);

int main()
{
  try
  {
    // Create localized non-local operator
    PtEvalDiscretizer pteval_discretizer("0");
    BasicOperator PtEval(pteval_discretizer);

    // Create mesh
    xBoundary b1(1., 0., 1.), b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.), b4(0., 0., 1.);
    Mesh mesh(50, 50);
    mesh.set_boundaries({b1, b2, b3, b4});

    CustomBC bc1u(b1), bc2u(b2), bc3u(b3), bc4u(b4);
    CustomBC bc1v(b1), bc2v(b2), bc3v(b3), bc4v(b4);

    // Part 1: declaration of fields and functions
    Field u("u"), v("v"), w;
    Function f("f"), g("g"), X("xc"), Y("yc"), f1("f_1");
    Function xs("exact_solution"), uex("u_ex"), vex("v_ex");
    FieldFunction G("G", 2), Exp("f_exp",1), Uex("U_ex",2), Vex("V_ex",2);
    Operator L("L");

    // Set PDE BCs
    bc1u = u - PtEval[v] - 1; // Nonlocal
    bc2u = v*Dx[u] + (u-Y)*Exp(v);//u - uex;
    bc3u = u - uex;
    bc4u = u - uex;
    bc1v = -Dx[v] + v - Y;
    bc2v = v + Exp(u) - f1;
    bc3v = v - vex;
    bc4v = v - vex;

    // Part 2: declaration and customization of PDE
    PDE pde(&mesh);
    pde.name = "PDE2d";
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4/*SYM_DIFF2*/);

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v});
    pde.set_BCs({bc1u, bc2u, bc3u, bc4u}, u);
    pde.set_BCs({bc1v, bc2v, bc3v, bc4v}, v);
    pde.set_functions({f, g, xs, X, Y, f1});
    pde.set_functions({uex, vex});
    pde.set_functions({G, Exp, Uex, Vex});
//    pde.set_initial_estimate("c == 0 ? x+y : 1+y");
//    pde.set_initial_estimate("c == 0 ? x : (x < .25 ? 1 : 2)");
    pde.set_initial_estimate("c == 0 ? 1.1*x*x + .9*y : x+.9*y");
    pde.use_field_names = true;

    xs.set_implementation("c == 0 ? u_ex(x,y) : v_ex(x,y)");
    uex.set_implementation("x*x+y");
    vex.set_implementation("x+y");
    X.set_implementation("x");
    Y.set_implementation("y");
    f1.set_implementation("y+exp(y)");
    #if 0
    f.set_implementation("-2+exp(x*x)*(x+1)*2*x+x*x*(x+2)+(x+1)*(x+1)");
    g.set_implementation("(x+1)*(x+1)+x*x*(1+exp(x+1)*x*x)");
    #endif // 0
    f.set_implementation("U_ex(u_ex(x,y),v_ex(x,y))");
    g.set_implementation("V_ex(u_ex(x,y),v_ex(x,y))");

    G.set_implementation({"exp(u)*v",
      /* G_u */ "G(u,v)",
      /* G_v */ "exp(u)"});
    Uex.set_implementation({"-2+v*exp(u)+u+u*v+v*v","0","0"});
    Vex.set_implementation({"v*v+u+exp(v)*u*u","0","0"});
    Exp.set_implementation({"exp(u)",
      /* fBC0v_u */ "exp(u)"});

    L(w) = Dxx[w] + Dyy[w];

    // Part 4: set PDE equations
    pde("u") = -L[u] + G(u,v)*Dy[u]*Dx[v] + u*Dx[v] + u*v + v*v - f;
    pde("v") = -u*L[v] + (v*v+u)*Dx[v] + G(v,u)*u - g;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("Petsc");

    // Part 6: output
    pde.executable = "petsc2d_nl_2";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/petsc2d_nl_2");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
