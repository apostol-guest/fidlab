#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info = R"(/**
 *  @brief Project: Laplace equation with Neumann BCs, No. 2
 *
 *  Features:
 *  \li system of 2 pdes
 *  \li operators
 *
 *     y ^
 *       |
 *       |  [1] b3
 *     1 +----------+
 *       |          |
 *    [2]|          |[0]
 *    b2 |          | b1
 *       |          |
 *     0 +----------+-----> x
 *       0  [3] b4  1
 *
 *  BCs for u:
 *  b1: $-\frac{\partial u}{\partial x}+2u=0$
 *  b3: $-\frac{\partial u}{\partial y}+u=-e^{x^2+1}$
 *  b2: $\frac{\partial u}{\partial x}=0$
 *  b4: $\frac{\partial u}{\partial y}=0$
 *  BCs for v:
 *  b1: $v=v_exact$
 *  b3: $v=v_exact$
 *  b2: $v=v_exact$
 *  b4: $v=v_exact$
 *
 *
 *  PDE:
 *  $u_{xx}+u_{yy}=4(x^2+y^2+1)e^{x^2+y^2}$
 *  $v_{xx}+v_{yy}=-2\sin(x+y)$
 *
 *  Exact solution:
 *  $u=e^{x^2+y^2}$
 *  $v=\sin(x+y)$
 *
 *  Linear solver: GSL LU
 */
)";

int main()
{
  try
  {
    xBoundary b1(1., 0., 1.), b2(0., 1., 0.);
    yBoundary b3(1., 1., 0.), b4(0., 0., 1.);

    Mesh mesh(50, 50);
    mesh.set_boundaries({b1,b2,b3,b4});

    NeumannBC bcu1(b1, "-1", "2", "");
    NeumannBC bcu2(b2, "1", "", "");
    NeumannBC bcu3(b3, "-1", "1", "cy");
    NeumannBC bcu4(b4, "1", "0", "");
    DirichletBC bcv1(b1, "bcv"), bcv2(b2, "bcv"), bcv3(b3, "bcv"), bcv4(b4, "bcv");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v"), w;
    Function cy("cy"), bc("bcv"), f("f"), g("g"), fu("fu"), fv("fv");
    Function xs("exact_solution"), uex("u_ex"), vex("v_ex");
    Operator Delta("Delta");

    cy.set_implementation("-exp(x*x+1)");
    bc.set_implementation("v_ex(x,y)");
    fu.set_implementation("4*(x*x+y*y+1)*exp(x*x+y*y)");
    fv.set_implementation("-2*sin(x+y)");
    f.set_implementation("fu(x,y)-2*fv(x,y)");
    g.set_implementation("fu(x,y)+fv(x,y)");
    uex.set_implementation("exp(x*x+y*y)");
    vex.set_implementation("sin(x+y)");
    xs.set_implementation("c == 0 ? u_ex(x,y) : v_ex(x,y)");

    Delta(w) = Dxx[w] + Dyy[w];

    // Part 2: declaration of PDE
    PDE pde(&mesh);
    pde.name = "PDE2d";
    pde.set_boundary_discretization_method(PDE::INWARD_DIFF4);// PDE::SYM_DIFF2 creates an error

    // Part 3: set fields, BCs and functions
    pde.set_fields({u,v});

    pde.set_BCs({bcu1, bcu2, bcu3, bcu4},u);
    pde.set_BCs({bcv1, bcv2, bcv3, bcv4},v);

    pde.set_functions({cy, bc, f, g, fu, fv});
    pde.set_functions({xs, uex, vex});

    // Part 4: PDE definition
    pde("u") = Delta[u] - 2*Delta[v] - f;
    pde("v") = Delta[u] +   Delta[v] - g;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set code generation methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");

    // Part 6: output
    pde.executable = "gsl2d_neu_2";
    pde.project_info = project_info;
    pde.compiler = "clang++";
    pde.set_output("code/gsl2d_neu_2");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
