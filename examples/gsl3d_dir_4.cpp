#include <iostream>
#include <fidlab/pde3d.hpp>

using namespace fd;
using namespace fd3;

const char *project_info =
R"(/**
 *  Project: Laplace 2-equation system with Dirichlet BCs, No. 4
 * 
 *  BC: $u=u_exact, v=v_exact$
 * 
 *  PDE:
 *  \begin{align*}
 *  & 2(u_{xx}+u_{yy}+u_{zz})-(v_{xx}+v_{yy}+v_{zz})=2[6\lambda+4\lambda^2(x^2+y^2+z^2)]e^{\lambda(x^2+y^2+z^2)}+3\sin(x+y+z) \\
 *  & u_{xx}+u_{yy}+u_{zz}+v_{xx}+v_{yy}+v_{zz}=[6\lambda+4\lambda^2(x^2+y^2+z^2)]e^{\lambda(x^2+y^2+z^2)}-3\sin(x+y+z)
 *  \end{align*}
 * 
 * 	Exact solution:
 * 	$u=e^{\lambda(x^2+y^2+z^2)}$
 * 	$v=\sin(x+y+z)$
 * 
 *  Boundary discretization method: does not apply.
 *  Linear solver: GSL LU
 */
)";

int main()
{
	try
	{
    Mesh mesh({},13);

		DirichletBC bcgu(mesh.get_global_boundary(), "bcu");
		DirichletBC bcgv(mesh.get_global_boundary(), "bcv");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f"), g("g"), bcu("bcu"), bcv("bcv");
    Function xs("exact_solution"), uex("u_exact"), vex("v_exact");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE3d";

    // Part 3: set PDE fields, BCs and functions
    pde.set_fields({u, v});
    pde.set_BCs({bcgu}, u);
    pde.set_BCs({bcgv}, v);
    pde.set_functions({f, g, bcu, bcv, uex, vex, xs});
    f.set_implementation("(6+4*(x*x+y*y+z*z))*exp(x*x+y*y+z*z)");
    g.set_implementation("-3*sin(x+y+z)");
    bcu.set_implementation("u_exact(x,y,z)");
    bcv.set_implementation("v_exact(x,y,z)");
    uex.set_implementation("exp(x*x+y*y+z*z)");
    vex.set_implementation("sin(x+y+z)");
    xs.set_implementation("c == 0 ? u_exact(x,y,z) : v_exact(x,y,z)");

    // Part 4: set PDE equations
    pde("D1") = 2*(Dxx[u] + Dyy[u] + Dzz[u])-(Dxx[v] + Dyy[v] + Dzz[v]) - (2*f-g);
    pde("D2") = Dxx[u] + Dyy[u] + Dzz[u] + Dxx[v] + Dyy[v] + Dzz[v] - f - g;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");

    // Part 6: output
    pde.executable = "gsl3d_dir_4";
    pde.project_info = project_info;
//    pde.compiler = "clang++";
    pde.set_output("code/gsl3d_dir_4");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    exit(1);
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    exit(1);
  }

  return 0;
}
