#include <iostream>
#include <fidlab/pde2d.hpp>

using namespace fd;
using namespace fd2;

const char *project_info =
R"(//!	Project: Laplace PDE with multiple mixed Dirichlet-Neumann BCs, No. 0
//!	Features operators (linear), multiple BCs per side
//!
//!    y ^
//!      |
//!    1 |  [2]      [1]
//!      +--------|-------+
//!   [3]|                |
//!      -                |
//!   [4]|                |
//!      |                |
//!      -                |[0]
//!   [5]|                |
//!      -                |
//!   [6]|                |
//!    0 +----------------+-----> x
//!      0      [7]        1
//!
//! BCs:
//! [0]: $u=u_exact$
//! [1]: $u=u_exact$
//! [2]: $u_y=cos(x+1)$
//! [3]: $u=u_exact$
//! [4]: $u_x=cos(y)$
//! [5]: $u=u_exact$
//! [6]: $u_x=cos(y)$
//! [7]: $u=u_exact$
//!
//! PDE:
//!	$v_{xx}+v_{yy}=-2\sin(x+y)$
//!
//!	Exact solution:
//!	$v=\sin(x+y)$
//!
//! Boundary discretization method: does not apply
//! Linear solver: GSL LU
)";

int main()
{
	try
	{
		// 0. Mesh definition
		Mesh mesh(50, 50);

		xBoundary b0(1,   0,  1);
		xBoundary b3(0, .75,  1),
							b4(0, .5,  .75),
							b5(0, .25, .5),
							b6(0,   0, .25);
		yBoundary b1(1,  .5,  1),
							b2(1,   0, .5);
		yBoundary b7(0,   0,  1);
		mesh.set_boundaries({b3, b0, b2, b4, b1, b5, b6, b7});
		mesh.print_boundaries();

		// 1. Declaration of fields and functions
		Field u("u"), w("w");
		Function f("f"), uex("exact_solution"), neu("gn");
		Function dir0("g0"), dir1("g1"), dir3("g3"), dir5("g5"), dir7("g7");

		// 2. Declaration and customization of PDE
		PDE pde(&mesh);
		pde.name = "PDE2d";

		// 3. Set PDE fields and functions
		pde.set_field(u);
		pde.set_functions({f, dir0, dir1, dir3, dir5, dir7, neu, uex});
		f.set_implementation("2*sin(x+y)");
		dir0.set_implementation("sin(x+y)");
		dir1.set_implementation("sin(x+y)");
		dir3.set_implementation("sin(x+y)");
		dir5.set_implementation("sin(x+y)");
		dir7.set_implementation("sin(x+y)");
		neu.set_implementation("cos(x+y)");
		uex.set_implementation("sin(x+y)");

		// 4. Declaration of operators
		Operator Delta("Laplace");
		Delta(w) = Dxx[w] + Dyy[w];

		// 5. Set PDE equations
		pde("D") = Delta[u] + f;

		// 6. Set BCs
		DirichletBC bc0(b0, "g0"), bc1(b1, "g1"), bc3(b3, "g3"),
								bc5(b5, "g5"), bc7(b7, "g7");
		NeumannBC 	bc2(b2, "1", "", "gn"), bc4(b4, "1", "", "gn"),
								bc6(b6, "1", "", "gn");
		pde.set_BCs({bc1, bc2, bc3, bc4, bc0, bc5, bc6, bc7});

		std::cout << "PDE";
		std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
		std::cout << "PDE";
		std::cout << (pde.linear() ? " is linear" : " is nonlinear") << '\n';

		// 7. Set methods
		pde.set_library("Petsc");

		// 8. Output
		pde.executable = "petsc2d_mb_0";
		pde.project_info = project_info;
		pde.compiler = "clang++";
		pde.set_output("code/petsc2d_mb_0");
		pde.generate_code();
		pde.generate_makefile();
	}
	catch (const std::string& msg)
	{
		std::cout << msg << std::endl;
		return 1;
	}
	catch (const char *msg)
	{
		std::cout << "Caught const char* exception!\n";
		std::cout << msg << std::endl;
		return 1;
	}

	return 0;
}
