#include <iostream>
#include <fidlab/pde1d.hpp>

using namespace fd;
using namespace fd1;

const char *project_info =
R"(/**
 *  Project: Nonlinear PDE-1d with Dirichlet BCs, No. 3
 *
 *  Features
 *  + Custom initial estimate setting.
 *  + Nonlinear operator.
 *
 *     --+----------+-----> x
 *       0          1
 *      [b0]       [b1]
 *
 *  PDE:
 *  $-\frac{d}{dx}\left(\left|u\right|^\alpha\frac{du}{dx}\right)+bu^2=f(x)$
 *
 *  BCs:
 *  b0: $0$
 *  b1: $1$
 *
 *  Exact solution:
 *  $u(x)=\sin(\lambda x)$
 *
 *  Boundary discretization method: does not apply (INWARD_DIFF3)
 *  Nonlinear solver: GSL all.
 *
 *  NOTE: For LAMBDA > (0.5*M_PI) this is an example of a non-converging
 *  system.
 */
)";

#define ALPHA "1.5"
#define LAMBDA "(0.5*M_PI)"

int main()
{
  try
  {
    Boundary b0(0.), b1(1.);

    Mesh mesh(50);
    mesh.set_boundaries({b0, b1});

    DirichletBC bc0(b0, "0"), bc1(b1, "1");

    // Part 1: declaration of fields and functions
    Field u("u"), v("v");
    Function f("f");
    Function xs("exact_solution"), sig("sig");
    FieldFunction G("G", 1), Gp("Gp", 1);
    Operator L("L");

    // Part 2: declaration and customization of PDE
    PDE pde(mesh);
    pde.name = "PDE1d";
    pde.set_boundary_discretization_method(PDE::/*SYM_DIFF2*/INWARD_DIFF3);

    // Part 3: set PDE fields, BCs and functions
    pde.set_field(u);
    pde.set_BCs({bc1, bc0}, u);
    pde.set_functions({f, xs, sig});
    pde.set_functions({G, Gp});
    pde.set_initial_estimate("x");

    xs.set_implementation("sin(" LAMBDA "*x)");
    sig.set_implementation("x > 0 ? x : (x < 0 ? -x : 0)");
    f.set_implementation(R"(
	{
		const double u = sin()" LAMBDA R"(*x);
		const double p = cos()" LAMBDA R"(*x);
		return G(u)*)" LAMBDA "*" LAMBDA "*u-Gp(u)*" LAMBDA R"(*p+2*u*u;
	})");

    G.set_implementation({"pow(fabs(u), " ALPHA ")",
      /* F^\prime */ "Gp(u)"});
    Gp.set_implementation({ALPHA "*pow(fabs(u)," ALPHA "-1)*sig(u)",
    /* Fp^\prime */ ALPHA "*(" ALPHA "-1)*pow(fabs(u),"  ALPHA "-2)*sig(u)"});

    L(v) = G(v)*Dxx[v] + Gp(v)*Dx[v];

    // Part 4: set PDE equations
    pde = -L[u] + 2*u*u - f;

    std::cout << "PDE";
    std::cout << (pde.linear_BCs() ? " has linear BCs" : " has nonlinear BCs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear_eqs() ? " has linear eqs" : " has nonlinear eqs") << '\n';
    std::cout << "PDE";
    std::cout << (pde.linear() ? " is linear." : " is nonlinear.") << '\n';

    // Part 5: set methods
    pde.set_library("gsl");
    pde.set_matrix_method("LU");
    pde.set_nonlinear_solver("newton");

    // Part 6: output
    pde.executable = "gsl1d_nl_3";
    pde.compiler = "clang++";
    pde.project_info = project_info;
    pde.set_output("code/gsl1d_nl_3");
    pde.generate_code();
    pde.generate_makefile();
  }
  catch (const std::string& msg)
  {
    std::cout << msg << std::endl;
    return 1;
  }
  catch (const char *msg)
  {
    std::cout << "Caught const char* exception!\n";
    std::cout << msg << std::endl;
    return 1;
  }

  return 0;
}
